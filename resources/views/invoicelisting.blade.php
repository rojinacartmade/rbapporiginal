@extends('shopify-app::layouts.default')

@section('content')


 <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.26.1/styles.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
  <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet">
  <!-- links for rb portal -->
  <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/ordercreation.css') }}">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="//cdn.shopify.com/s/files/1/0115/7490/2850/t/3/assets/jquery.csv.min.js?23815" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <!-- <script src="{{ asset('public/js/spectrum.js') }}"></script> -->
   
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
  ShopifyApp.init({
      apiKey: '427ad31ed9c45b59b4227a4187f91e7e',
      shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}'
  });
</script>
<div class="customer-tab-first first-wrapper">    
      <div class="Polaris-Page">        
        <div class="Polaris-Page-Header heading-margin  Polaris-Page-Header--mobileView">
             <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                  <div class="Polaris-Header-Title header-option-style">
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge ">Invoices</h1>                  
                  </div>                
              </div>           
        </div>

         <?php 
         if(isset($searchdata)){
             $data = json_decode($searchdata);             
              if (array_key_exists('no_of_order_list', $data)) {
                $no_of_order_list = $data->no_of_order_list;
            }
             if (array_key_exists('invoice_number', $data)) {
                $invoice_number = $data->invoice_number;
            }
            if (array_key_exists('customer_name', $data)) {
                $customer_name = $data->customer_name;
            }
             if (array_key_exists('customer_last_name', $data)) {
                $customer_last_name = $data->customer_last_name;
            }
            
         }
           
        ?> 


          <div class="Polaris-Page__Content">
            <div class="Polaris-Card">
              <div>
                <ul role="tablist" class="Polaris-Tabs">
                  <li class="Polaris-Tabs__TabContainer step-tab step-tab-1"><button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers"><span class="Polaris-Tabs__Title">1. Invoice Lists</span></button></li>
                  <li class="Polaris-Tabs__TabContainer step-tab step-tab-2"><button id="accepts-marketing" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="accepts-marketing-content"><span class="Polaris-Tabs__Title">2. Invoice Detail</span></button></li>
                </ul>
              <div class="Polaris-Tabs__Panel step-panel step-panel-1" id="all-customers-content" role="tabpanel" aria-labelledby="all-customers" tabindex="-1">
              <div class="order_filter" class="wrappable__item" >
                <div class="formwrap">
                <form action="/invoices" method="post" class="order_filter_form">
                  <div class="fieldswrapper">
                    <div class="qty-filter-wrapper fieldwrap">
                     <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Show </label></div>
                    <div class="custom-option custom-option-Size">  
                      <div class="ariant-height-input-wrap">
                        <select class=" variant-closure-input selectvalue" aria-invalid="false" name="no_of_order_list">
                          
                           <option value="20" <?php if(isset($no_of_order_list)){ if($no_of_order_list=="20"){ echo 'selected="selected"';}} ?> >20</option>
                           <option value="50" <?php if(isset($no_of_order_list)){ if($no_of_order_list=="50"){ echo 'selected="selected"';}} ?>>50</option>
                           <option value="100" <?php if(isset($no_of_order_list)){ if($no_of_order_list=="100"){ echo 'selected="selected"';}} ?>>100</option>
                        </select>                       
                       </div>  
                     </div>
                  </div>

                  <div class="ordernumber-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Invoice Number</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="{{ $invoice_number ?? '' }}" name="invoice_number">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                  <div class="firstname-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">First Name</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="{{ $customer_name ?? '' }}" name="customer_name">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                  <div class="firstname-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Last Name</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="{{ $customer_last_name ?? '' }}" name="customer_last_name">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                  
                </div>

                <div class="buttonwrapper">
                  <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                    <div class="ui-page-actions__button-group">
                      <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading filterorder" type="submit" name="commit" >Filter</button>
                    </div>
                  </div>
                
                <div >
                
                
                </div>
                </div>
                </form>
                <form method="post" action="/invoices" class="resersearch">

                <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                  <div class="ui-page-actions__button-group">
                    <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading resetfilter" type="submit" name="commit" value="search" >Reset</button>
                  </div>
                </div>
                </form>
              </div>
              </div>
              <div class="sales-by-product-wrapper">
                <div class="Polaris-DataTable__Navigation"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly" disabled="" aria-label="Scroll table left one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Scroll table right one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button></div>
                <div class="Polaris-DataTable">
                  <div class="Polaris-DataTable__ScrollContainer">
                    <table class="Polaris-DataTable__Table orderlisting">
                      <thead>
                        <tr>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Invoice Number</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Customer Name</th>
                          
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Total</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Order Date</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Invoice Date</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Reference</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Deposit</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Status</th>
                        </tr>

                      </thead>
                      <tbody>
                        
                       @php
                        foreach ($valuesnode as $key => $order) {                       

                          $orderid=$order->orderid;
                          $ordername = $order->invoice_number;
                          $ordername = str_replace("#50000","",$ordername);
                          $customer_first_name = $order->customer_first_name;
                          $customer_last_name = $order->customer_last_name;
                          $customername = $customer_first_name." ".$customer_last_name;
                          $created_at = date("m/d/Y",strtotime($order->order_date));
                          $deliver_at = date("m/d/Y",strtotime($order->invoice_date));
                          $reference = $order->school;                        
                          $fulfillments = $order->status;
                          $Deposit = $order->Deposit;                      
                          $total = $order->amount;
                          $amttobepaid = $order->amttobepaid;
                      @endphp
                            <tr class="Polaris-DataTable__TableRow  individual_order " data-toggle="modal" data-orderid="{{ $orderid }}" deposit_paid="{{ $Deposit }}" amttobepaid="{{ $amttobepaid }}" invoicestatus="{{ $fulfillments }}" data-target="#orderModal" >
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">INV{{ $ordername }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $customername }}</td>
                            
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">${{ $total }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $created_at }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $deliver_at }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $reference }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $Deposit ?? '' }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $fulfillments }}</td>

                            </tr>                              

                            @php
                                } 
                             @endphp 
                              </tbody>
                            </table>

                            
                          </div>
                        </div>
                      <div class="button-for-pagination">
                         {{ $valuesnode->appends(request()->query())->links() }}
                      </div>
                  </div>
              </div>
 
                    <!-- second tab for order detail -->
              <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden step-panel step-panel-2" id="orderdetailwrapper" role="tabpanel" aria-labelledby="accepts-marketing" tabindex="-1" style="display: none;">
                  <div class="order-detail-wrapper">
                    <div class="Polaris-Card__Section">
                       <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;" class="">
                        <div class="Polaris-Layout">
                          <div class="Polaris-Layout__Section">
                            
                            <div class="Polaris-Card">
                              <div class="Polaris-Card__Header invoice_header" style="padding: 15px;">
                                <div class="invoice_number" style="width: 50%;">
                                <h2 class=" ordername" style="padding: 10px;">Invoice Number : <span class="order_name" style="padding-top: 77px !important;">#500001066</span></h2>
                                </div>
                                <div class="order_number" style="width: 50%; text-align: right;">
                                <h2 class=" ordername" style="padding: 10px;">Order Number : <span class="order_num" style="padding-top: 77px !important;">#500001066</span></h2>
                                </div>
                            </div>
                             <div class="Polaris-Layout">

                                    <div class="Polaris-Layout__Section">
                                      <div class="Polaris-Card">
                                        <section class="ui-card__section customer-top-wrap">
                                          <div class="Polaris-Card__Header">
                                            <h2 class="Polaris-Heading">Customer</h2>
                                          </div>
                                          
                                          <div class="ui-card__section">
                                            <div class="ui-type-container ui-type-container--spacing-tight">

                                              <div data-protected-personal-information="">
                                                <a href="">
                                                 <span id="first_name">first_name</span>
                                                 <span id="last_name">last_name</span>
                                               </a>          
                                             </div>

                                             <div class="ui-stack ui-stack--alignment-baseline ui-stack--spacing-none">
                                              <div class="ui-stack-item ui-stack-item--fill type--truncated">
                                                <a href="#" id="email">email</a>
                                              </div>
                                            </div>                                       
                                          </div>
                                        </div>
                                      </section>

                                      <section class="ui-card__section customer-top-wrap">
                                        <div class="ui-type-container">
                                          <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                                            <div class="ui-card__section-header"><h3 class="ui-subheading">Shipping address</h3></div>
                                          </div>   
                                          <div class="ui-type-container text-emphasis-subdued" data-protected-personal-information="true">
                                            <span id="sfirstname">sandeep</span>
                                            <span id="slastname">pangeni</span>
                                            <br><span id="company">Cartmade</span>
                                            <br><span id="address1">ktm</span>
                                            <br><span id="address2">fns</span><br>
                                            <span id="city">Kathmandu</span><span id="province_code"></span> , <span id="zip">44600</span><br>
                                            <span id="country_name"></span><br>
                                          </div>
                                        </div>
                                      </section>

                                      <section class="ui-card__section customer-top-wrap">
                                        <div class="ui-type-container">
                                          <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                                            <div class="ui-card__section-header"><h3 class="ui-subheading">Billing address</h3></div>
                                          </div>    
                                          <p class="text-emphasis-subdued" data-protected-personal-information="">
                                            Same as shipping address 
                                          </p>
                                        </div>
                                      </section>
                                    </div>
                                  </div>

                                  <div class="Polaris-Layout__Section customer-top-wrap invoice-detail-lineitem--wrap">
                                    <div class="Polaris-Card">                       

                                      <div class="next-card__section next-card__section--no-vertical-spacing result-row">
                                        <table class="next-table--line-items">
                                          <tbody id="line_item_rows" context="draftBuilder">                    
                                          <!-- append the order list -->
                                        

                                        </tbody>
                                        </table>
                                      </div>

                                      <!-- div ends here -->

                                      <div class="next-card__section calculation-div">
                                        <div class="wrappable" context="draftBuilder">
                                          <div class="wrappable__item" style="display: none;">
                                            <div class="date-picker-wrapper">
                                              <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text"><b>Need by Date :</b></label> <span class="pickup-date "></span></div>
                                                
                                           </div>
                                           <div class="po-number-wrapper">
                                            <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text"><b>Reference :</b></label><span class="reference"></span></div>
                                            
                                          </div>
                                          <div class="next-input-wrapper order-note-wrapper"><label class="next-label" for="note"><b>Notes :</b></label>
                                            <span class="order_note"></span>
                                            <div class="only-when-printing"></div>
                                          </div>
                                          <div class="fileinput-wrapper" filepath="false">
                                            <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text"><b>Attach File</b></label></div>
                                            <div class="Polaris-TextField">
                                              <a href="#filelink" id="attachment_link" download>Attachment</a>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="wrappable__item">
                                          <div>
                                            <table class="table--no-border">
                                              <tbody>
                                                <tr>
                                                  <td class="type--right" width="80%">        
                                                    <div class="ui-popover__container discount-popover-wrapper" style="position: relative;">
                                                      <!-- <button type="button" class="btn btn--link" bind-disabled="calculating">Add discount</button> -->
                                                      <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                                                       <div class="discount-label-wrap">
                                                         <div>
                                                          <button type="button" class="add-discount-cta Polaris-Button btn btn--link" data-discount-percent="" data-discount-reason="" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper">Discount Amount</span></span></button>
                                                         <p class="type--subdued sel-discount-name"></p>
                                                       </div>
                                                       </div>
                                                     </div>
                                                    
                                                    </div>
                                                  </td>
                                                  <td class="type--right discount-value">
                                                    <span class="type--subdued" id="discount-amt">0.00</span>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td class="type--right type--subdued">Subtotal</td>
                                                  <td class="type--right sub-total">25.68</td>
                                                </tr>
                                                <tr>
                                                  <td class="type--right" style="max-width: 100px;position: relative;">
                                                  <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                                                    <div class="shipping-label-wrap">
                                                     <div><button type="button" class="add-shipping-cta Polaris-Button btn btn--link" data-shipping-type="Two Day shipping" data-custom-shipping-name="Two Day shipping" data-custom-shipping-amt="1" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper"> Shipping Method</span></span></button>
                                                     <p class="type--subdued sel-shipping-name">Two Day shipping</p>
                                                   </div>
                                                   </div>
                                                    </div>
                                                    
                                                    </td>
                                                    <td class="type--right">
                                                      <span class="type--subdued" id="shipping-amt">—</span>
                                                    </td>
                                                  </tr>
                                                </tbody>

                                                <tbody class="next-table--row-group-no-spacing">
                                                  <tr class="tax_applied">
                                                    <td class="type--right">          
                                                      <button type="button" class="btn btn--link ">Taxes</button>                
                                                    </td>
                                                    <td class="type--right taxes-applied">
                                                      0.00
                                                    </td>
                                                  </tr>
                                                </tbody>
                                                <tbody>
                                                  <tr>
                                                    <td class="type--right" style="width: 50%;">
                                                      <strong>Deposit</strong>
                                                    </td>
                                                    <td class="type--right amttobepaid" style="width: 50%;">No deposit paid</td>
                                                  </tr>
                                                  <tr style="display:none;">
                                                    <td class="type--right" style="width: 50%;">
                                                      <strong>Payment Status</strong>
                                                    </td>
                                                    <td class="type--right  deposit_paid" style="width: 50%;">Not Paid</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="type--right" style="width: 50%;">
                                                      <strong>Due Amount</strong>
                                                    </td>
                                                    <td class="type--right due_amount" style="width: 50%;">25.68</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="type--right" style="width: 50%;">
                                                      <strong>Total</strong>
                                                    </td>
                                                    <td class="type--right grand-total" style="width: 50%;">25.68</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>                  
                                  </div>
                                </div>
                                <!-- end here -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
           
            </div>
          </div>


          <style type="text/css">
            .fieldwrap{
                width: 190px !important;
                margin-bottom: 15px!important;
                margin-left: 10px;
              }
            .order_filter form{
              /*display: inline-flex;*/
              margin-top: 15px;
            }
            .fieldswrapper{
              display: inline-flex;
            }
            .buttonwrapper {
                display: inline-flex;
                margin-bottom: 15px;
            }
            .resersearch{
              margin-top: 0px !important;
            }
            ul.pagination {
                display: flex;
            }
            #customer_id{
              display: none;
            }
            .Polaris-DataTable__ScrollContainer
            {margin-left:0px;}
            table.Polaris-DataTable__Table th, table.Polaris-DataTable__Table td {
                text-align: left;
            }
            .first-wrapper
            {
              margin: 0px 20px 20px 20px;
            }
            .heading-margin
            {
              margin-top: 0px;
              padding-top: 2.1rem;
            }
            .header-option-style
            {
              padding: 7px 0px 0px 0px;
            }
            form.customer-csv-upload {
                text-align: left;
                margin-left: 20px;
                padding-top: 15px;
            }
            .customerSel{
              background-color: #e1e5f2;
            }

          </style>
    <script type="text/javascript">
      
        $(document).ready(function(){         
          //Step 1 : order selection
          $('table.orderlisting tr').on('click',function(){
            var orderid = $(this).attr('data-orderid');
            var deposit_paid = $(this).attr('deposit_paid');
            var amttobepaid = $(this).attr('amttobepaid'); 
            var invoicestatus = $(this).attr('invoicestatus');           
            var orderid = {'orderid':orderid};
            $.ajax({
                url: '/getorderdetail',
                dataType: 'json',
                type: 'post',
                data: orderid,
                beforeSend: function() {
                    // setting a timeout
                    // $('.response').html("Uploading ......");
                },
                success: function(orderdetail)
                {
                    
                  console.log(orderdetail);
                  var email = orderdetail.customer.email;
                  var cust_id = orderdetail.customer.id;                  
                  var first_name = orderdetail.customer.first_name;
                  var last_name = orderdetail.customer.last_name;
                  var addresses = orderdetail.customer.default_address;
                  var company = addresses.company;
                  var address1 = addresses.address1;
                  var address2 = addresses.address2;
                  var phone = orderdetail.customer.phone;
                  var city = addresses.city;
                  var province_code = addresses.provinceCode;
                  var zip = addresses.zip;
                  var country_name = addresses.country;
                  $("#first_name , #sfirstname").text(first_name);
                  $("#last_name , #slastname").text(last_name);
                  $("#email").html(email);
                  $("#company").html(company);
                  $("#address1").html(address1);
                  $("#address2").html(address2);
                  $("#city").html(city);
                  $("#province_code").html(province_code);
                  $("#zip").html(zip);
                  $("#country_name").html(country_name); 
                   var discount_detail = orderdetail.discount_codes;

                  $(discount_detail).each(function(i, discountdetail){
                    $(discount_detail).each(function(i, attr){
                      console.log(attr);
                      var name= attr.code;
                      var value= attr.amount;
                      $('#discount-amt').html("$"+value);
                      $('.sel-discount-name').html(name);                    
                    });
                  });
                  var line_items = orderdetail.line_items;
                  console.log(line_items);
                  var notes = orderdetail.note;
                  if (notes) {
                    $('.order_note').html(notes);
                  }
                  $('.order_name').html("INV" + orderdetail.order_number);                  
                  $('.order_num').html(orderdetail.name);
                  $(".deposit_paid").html(deposit_paid);
                  // $(".amttobepaid").html(amttobepaid);
                  var paidpercentage = 0.00;
                  var remaining = 100.00; 
                  if (amttobepaid == "30% Deposit" && deposit_paid=="Paid") {
                    paidpercentage = 30;
                    remaining = 100-paidpercentage;
                    $(".amttobepaid").html(amttobepaid + " Paid");
                  }
                  if (amttobepaid == "70% remaining balance" && deposit_paid=="Paid") {
                    paidpercentage = 30;
                    remaining = 100-paidpercentage;
                    amttobepaid = "30% Deposit";
                    $(".amttobepaid").html(amttobepaid + " Paid");
                  }
                  if (amttobepaid == "50% Deposit"  && deposit_paid=="Paid") {
                    paidpercentage = 50;
                    remaining = 100-paidpercentage;
                    $(".amttobepaid").html(amttobepaid + " Paid");
                  }
                  if (amttobepaid == "50% remaining balance"  && deposit_paid=="Paid") {
                    paidpercentage = 50;
                    remaining = 100-paidpercentage;
                    amttobepaid = "50% Deposit";
                    $(".amttobepaid").html(amttobepaid + " Paid");
                  }
                  if (amttobepaid == "Paid") {
                    paidpercentage = 100;
                    remaining = 100-paidpercentage;
                  }

                  console.log(remaining);
                  var grand_total =orderdetail.total_price;
                  var due_amount = (parseFloat(grand_total)*parseFloat(remaining))/100;
                  if (invoicestatus == "Paid") {
                    due_amount = parseFloat(0);
                    $(".amttobepaid").html("Paid");
                  }
                  $(".due_amount").html("$"+due_amount);

                  var grand_total =orderdetail.total_price;
                  $('.grand-total').html('$'+grand_total);
                  var total_discounts =orderdetail.total_discounts;
                  // $('#discount-amt').html('$'+total_discounts);
                  var subtotal_price =orderdetail.subtotal_price;
                  $('.sub-total').html('$'+subtotal_price);

                  var shipping_lines = orderdetail.shipping_lines;
                  $(shipping_lines).each(function(i, shipval){
                    $('.sel-shipping-name').html(shipval.title);
                    $('#shipping-amt').html('$'+shipval.price);
                    
                  });
                  var attribute_field = [];
                  var note_attributes = orderdetail.note_attributes;
                    console.log(note_attributes);
                    $(note_attributes).each(function(i, attr){
                      attribute_field.push(attr.name);
                      var name= attr.name;
                      var value= attr.value;
                      if (name == "Date") {
                        $('.pickup-date').html(value);
                      }
                      if (name == "Reference") {
                        $('.reference').html(value);
                      }
                      if (name == "filepath") {
                        $('#attachment_link').attr("href",value);
                      }
                    });
                    var hasfile = jQuery.inArray("filepath", attribute_field);
                    console.log(hasfile);
                    if(jQuery.inArray("filepath", attribute_field) == -1){
                      $('.fileinput-wrapper').hide();
                    }
                    var filepath = 'https://rbmembersportal.cartbrain.net/public/csvfiles/'+cust_id+'.csv';
                    var csvcontents = '';
                    // var url_path = '/rbmembersportal.cartbrain.net/public/csvfiles/'+sel_customerid+'.csv';
                    console.log(filepath);
                    $.ajax({
                      url : filepath,
                      dataType: "text",
                      async: false,
                      success : function (data) {
                       csvcontents = $.csv.toObjects(data);
                       console.log(csvcontents);
                     }
                    });
                    //product sku vs price array
                    var csv_product = [];      
                    if (csvcontents != '') {       
                    $.each(csvcontents, function(key,val){
                      var csv_sku = val.sku;
                      var csv_price = val.Price;
                      csv_product[csv_sku]=csv_price;
                    });
                    }

                  $(line_items).each(function(i, field){
                    var productid = field.id;
                    var properties = field.properties;
                    console.log(properties);
                    var properties_all= '';
                    $(properties).each(function(i, label){
                      console.log(label);
                      var properties_individual ='<span class="property"><span class="name">'+label.name+' :</span><span class="value">'+label.value+'</span></span>'
                      properties_all += properties_individual;
                    });
                    var title = field.name;
                    var product_id = field.product_id;                    
                    var product_id = {'product_id':product_id};
                    var title = field.title;
                    $.ajax({
                        async: false,
                        url: '/getproductimage',
                        dataType: 'json',
                        type: 'post',
                        data: product_id,
                        success: getproductimage,
                        error: function(data)
                        {
                            console.log(data);
                        }

                      });
                    function getproductimage(res)
                    {
                      var hasimage = res.length;
                      if (hasimage > 0) {
                        var iprod_mage = res['0'].src;   
                        image =  iprod_mage;                      
                      }else{
                        image = field.title;
                      }
                      console.log(image);
                    }
                    var price =field.price;
                    var discount_amt = field.total_discount;
                    var original_price = field.price;
                    var qty= field.quantity;
                    discount_amt = (parseFloat(discount_amt)/ parseFloat(qty)).toFixed(2); 
                    var price = (parseFloat(original_price)-parseFloat(discount_amt)).toFixed(2);
                    var itemtotal = ((parseFloat(original_price)-parseFloat(discount_amt))*parseFloat(qty)).toFixed(2);
                    var sku = field.sku;
                    // if (csvcontents != '') {
                    //   if(typeof csv_product[sku] != "undefined"){
                    //     console.log("inside csv");
                    //     var csv_price = csv_product[sku];
                    //     console.log(csv_price); 
                    //     var price = (csv_price);
                    //     var itemtotal = (parseFloat(price)*parseFloat(qty)).toFixed(2);
                    //   }
                    // }                   

                    var productrow = '<tr class="indiv-row-tr" data-attr-id="'+productid+'"><td class="next-table__image-cell hide-when-printing" width="1"><div class="aspect-ratio aspect-ratio--square aspect-ratio--square--50">\
                      <img title="" class="productimage block aspect-ratio__content" src="'+image+'"></div>\
                  </td>\
                  <td class="next-table__cell--item-name" width="45%">\
                    <a href="#" class="handle">'+title+'</a>\
                    <p class="type--subdued">SKU: <span class="sku price">'+sku+'</span></p>\
                    <p class="properties">'+properties_all+'</p>\
                  </td>\
                  <td class="type--left next-table__cell--grow-when-condensed" \
                  width="1">               \
                    <div class="ui-popover__container">\
                        <span class="price unit-price" data-custom-price="" data-unit-price="'+price+'" data-unit-discount="'+discount_amt+'" data-attr-org-price="'+original_price+'">$'+price+'</span>\
                        \
                    </div>\
                    <div class="item-custom-pricing">\
                    </div>\
                  </td>\
                  <td class="type--subdued">✖</td>\
                  <td style="min-width: 75px; width: 75px;">\
                    <div class="next-input-wrapper" data-unit-price="'+price+'" data-custom-price=""><input min="1" max="1000000" class="next-input next-input--number qty-input next_input_wrapper" size="30" type="number" value="'+qty+'" name="" disabled>\
                    </div>\
                  </td>\
                  <td class="type--right individual-total" class="total_sub_right">$'+itemtotal+'</td>\
                  </tr> ';
                  $('#line_item_rows').append(productrow);
                  });

                  
                  console.log(email);
                  $('li.step-tab').each(function(){
                    $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
                  });
                  $('.step-tab-2').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
                  //2. respective content area
                  $('.step-panel').each(function(){
                    $(this).hide();
                  });
                  $('.step-panel-2').show();
                    
                },
                error: function(data)
                {
                    console.log(data);
                }
            });
                   
          });


          // selecting order show

          $('.step-tab-1').on('click',function(){             
            $('li.step-tab').each(function(){
              $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
            });
            $('.step-tab-1').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
            $('.step-panel-2').hide();
            $('.step-panel-1').show();
            $('#line_item_rows').empty();
            $(".due_amount").html("$0.00");
            $(".amttobepaid").html("No deposit paid");
          });

        });

    </script>

@endsection