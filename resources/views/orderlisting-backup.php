@extends('shopify-app::layouts.default')

@section('content')


 <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.26.1/styles.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
  <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet">
  <!-- links for rb portal -->
  <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/ordercreation.css') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="//cdn.shopify.com/s/files/1/0115/7490/2850/t/3/assets/jquery.csv.min.js?23815" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <!-- <script src="{{ asset('public/js/spectrum.js') }}"></script> -->
   
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
  ShopifyApp.init({
      apiKey: '427ad31ed9c45b59b4227a4187f91e7e',
      shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}'
  });
</script>
<div class="customer-tab-first first-wrapper">    
      <div class="Polaris-Page">        
        <div class="Polaris-Page-Header heading-margin  Polaris-Page-Header--mobileView">
             <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                  <div class="Polaris-Header-Title header-option-style">
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge ">Orders</h1>                  
                  </div>                
              </div>           
        </div>


        <div class="Polaris-Page__Content">
          <div class="Polaris-Card">
            <div class="sales-by-product-wrapper">
              <div class="Polaris-DataTable__Navigation"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly" disabled="" aria-label="Scroll table left one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path>
              </svg></span></span></span></button><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Scroll table right one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path>
              </svg></span></span></span></button></div>
              <div class="Polaris-DataTable">
                <div class="Polaris-DataTable__ScrollContainer">
                  <table class="Polaris-DataTable__Table">
                    <thead>
                      <tr>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Order Number</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Customer Name</th>
                        
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Total</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Date</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Delivery Date</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Reference</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Description</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Fulfillment</th>
                      </tr>

                    </thead>
                    <tbody>
                      
                     @php
                      foreach ($valuesnode as $key => $order) {                       

                        $orderid=$order->order_id;
                        $ordername = $order->order_name;
                        $customer_first_name = $order->customer_first_name;
                        $customer_last_name = $order->customer_last_name;
                        $customername = $customer_first_name." ".$customer_last_name;
                        $created_at = $order->order_created_at;
                        $deliver_at = $order->order_delivery_date;
                        $reference = $order->reference;                        
                        $fulfillments = $order->fulfillment_status;
                        $tag = $order->order_tags;
                                                
                        $total = $order->total_amount;
                        
                        $description = $order->notes;
                    @endphp
                          <tr class="Polaris-DataTable__TableRow  toggle-addcsv individual-customer" data-toggle="modal" data-id="{{ $orderid }}" data-target="#orderModal" >
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{ $ordername }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $customername }}</td>
                          
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $total }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $created_at }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $deliver_at }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $reference }}</td>
                             <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $description }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $fulfillment ?? '' }}</td>

                          </tr>                              

                          @php
                              } 
                           @endphp 
                            </tbody>
                          </table>


                        </div>
                      </div>
                           @php
                           $allcursors = array("ajsdama");
                            $fornext=$allcursors[count($allcursors) - 1];
                            $forprev=$allcursors['0'];
                            $hasNextPageProd = $hasNextPageCust;
                            $hasPreviousPageProd=$hasPreviousPageCust;

                          @endphp
                          <div class="button-for-pagination">
                            @if($hasPreviousPageProd)
                           <form class="productbackform" method="post" action="/ordersnext">  
                              <input  name="hasforback" value="{{$forprev}}" type="hidden">
                              <input  name="hasbackprod" value="{{$hasPreviousPageProd}}" type="hidden">
                              <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Back</span></span></button></div>
                          </form>
                          @else
                              <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button" disabled="disabled"><span class="Polaris-Button__Content"><span>Back</span></span></button></div>

                          @endif
                          @if($hasNextPageProd)
                           <form class="productnextform" method="post" action="/ordersnext">  
                              <input  name="hasfornext" value="{{$fornext}}" type="hidden">
                              <input  name="hasnextprod" value="{{$hasNextPageProd}}" type="hidden">
                              <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Next</span></span></button></div>
                              </form>
                          @else
                            <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button" disabled="disabled"><span class="Polaris-Button__Content"><span>Next</span></span></button></div>
                          @endif
                          </div>
                    </div>
                </div>
         
          </div>
        </div>


          <style type="text/css">
            #customer_id{
              display: none;
            }
            .Polaris-DataTable__ScrollContainer
            {margin-left:0px;}
            table.Polaris-DataTable__Table th, table.Polaris-DataTable__Table td {
                text-align: left;
            }
            .first-wrapper
            {
              margin: 0px 20px 20px 20px;
            }
            .heading-margin
            {
              margin-top: 0px;
              padding-top: 2.1rem;
            }
            .header-option-style
            {
              padding: 7px 0px 0px 0px;
            }
            form.customer-csv-upload {
                text-align: left;
                margin-left: 20px;
                padding-top: 15px;
            }
            .customerSel{
              background-color: #e1e5f2;
            }

          </style>
    

@endsection