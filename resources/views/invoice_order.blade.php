@extends('shopify-app::layouts.default')

@section('content')
 @include('custom-popup') 
 @include('shipping-popup') 

<link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.26.1/styles.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
  <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet">
  <!-- links for rb portal -->
  <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/ordercreation.css') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="//cdn.shopify.com/s/files/1/0115/7490/2850/t/3/assets/jquery.csv.min.js?23815" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <!-- <script src="{{ asset('public/js/spectrum.js') }}"></script> -->
   
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
  ShopifyApp.init({
      apiKey: '427ad31ed9c45b59b4227a4187f91e7e',
      shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}'
  });
</script>
<div class="customer-tab-first first-wrapper">    
      <div class="Polaris-Page">        
        <div class="Polaris-Page-Header heading-margin  Polaris-Page-Header--mobileView">
             <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                  <div class="Polaris-Header-Title header-option-style">
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge ">Invoice Creation</h1>                  
                  </div>                
              </div>           
        </div>
        <?php 
         if(isset($searchdata)){
             $data = json_decode($searchdata);
             
              if (array_key_exists('no_of_order_list', $data)) {
                $no_of_order_list = $data->no_of_order_list;
            }
             if (array_key_exists('ordernumber', $data)) {
                $search_data_ordernumber = $data->ordernumber;
            }
            
            if (array_key_exists('deliverystatus', $data)) {
                $search_data_deliverystatus = $data->deliverystatus;
            }
            if (array_key_exists('customerid', $data)) {
                $customerid = $data->customerid;
            }


    
             // $search_data_firstname = $data->firstname;
             // $search_data_ordernumber = $data->ordernumber;
             // $search_data_lastname = $data->lastname;
             // $search_data_deliverystatus = $data->deliverystatus;
             // $search_data_schoolname = $data->schoolname;
            
         }
           
        ?>

          <div class="Polaris-Page__Content">
            <div class="Polaris-Card">
              <div>
              <ul role="tablist" class="Polaris-Tabs">
                <li class="Polaris-Tabs__TabContainer step-tab step-tab-1"><button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers"><span class="Polaris-Tabs__Title">1. Select Orders</span></button></li>
                <li class="Polaris-Tabs__TabContainer step-tab step-tab-2"><button id="accepts-marketing" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="accepts-marketing-content"><span class="Polaris-Tabs__Title">2. Invoice Creation </li>
              </ul>
              <div class="Polaris-Tabs__Panel step-panel step-panel-1" id="all-customers-content" role="tabpanel" aria-labelledby="all-customers" tabindex="-1">
                <div class="order_filter" class="wrappable__item" >
                <div class="formwrap">
                <form action="/invoicesearch" method="post" class="order_filter_form">
                  <div class="fieldswrapper">
                    <div class="qty-filter-wrapper fieldwrap">
                     <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Show </label></div>
                    <div class="custom-option custom-option-Size">  
                      <div class="ariant-height-input-wrap">
                        <select class=" variant-closure-input selectvalue" aria-invalid="false" name="no_of_order_list">
                          
                           <option value="20" <?php if(isset($no_of_order_list)){ if($no_of_order_list=="20"){ echo 'selected="selected"';}} ?> >20</option>
                           <option value="50" <?php if(isset($no_of_order_list)){ if($no_of_order_list=="50"){ echo 'selected="selected"';}} ?>>50</option>
                           <option value="100" <?php if(isset($no_of_order_list)){ if($no_of_order_list=="100"){ echo 'selected="selected"';}} ?>>100</option>
                        </select>                       
                       </div>  
                     </div>
                  </div>
                  <?php 

                     if ((isset($_GET['customerid']))) {
                      $customerid = $_GET['customerid'];
                    }
                    else{
                      if(isset($customerid)){
                       $customerid = $customerid;
                      }
                    }
                  ?>
                  <input type="hidden" name="customerid" value="{{ $customerid ?? '' }}">
                  <div class="ordernumber-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Invoice Number</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="{{ $search_data_ordernumber ?? '' }}" name="ordernumber">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>

                  <div class="fulfillment-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Status</label></div>
                    <div class="custom-option custom-option-Size">      
                      <div class="ariant-height-input-wrap">
                        <select class=" variant-closure-input selectvalue" aria-invalid="false" name="deliverystatus">
                          <option value="">Select Status</option>
                           <option value="Settled" <?php if(isset($search_data_deliverystatus)){ if($search_data_deliverystatus=="Settled"){ echo 'selected="selected"';}} ?>>Settled</option>
                           <option value="Unsettled" <?php if(isset($search_data_deliverystatus)){ if($search_data_deliverystatus=="Unsettled"){ echo 'selected="selected"';}} ?>>Unsettled</option>    
                        </select>                       
                       </div>  
                     </div>
                  </div>
                 
                  
                </div>

                <div class="buttonwrapper">
                  <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                    <div class="ui-page-actions__button-group">
                      <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading filterorder" type="submit" name="commit" >Filter</button>
                    </div>
                  </div>
                
                <div >
                
                
                </div>
                </div>
                </form>
                <form method="post" action="/invoiceorders" class="resetsearch" customerid="/invoiceorders?customerid={{ $customerid ?? '' }}">

                <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                  <div class="ui-page-actions__button-group">
                    <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading resetfilter" type="submit" name="commit" value="search" >Reset</button>
                  </div>
                </div>
                </form>
              </div>
              </div>
              <div class="sales-by-product-wrapper invoice-creation-wrapper">
                <div class="Polaris-DataTable__Navigation"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly" disabled="" aria-label="Scroll table left one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Scroll table right one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button></div>
                 @php
                  if(count($valuesnode) > 1){
                  @endphp  
                <div class="Polaris-DataTable">
                  <div class="Polaris-DataTable__ScrollContainer">
                    <table class="Polaris-DataTable__Table orderlisting">
                      <thead>
                        <tr>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Invoice #</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Order Number</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Order Date</th>
                        
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Total</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Deposite</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Status</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Fulfillment</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">
                          
                        </th>
                        </tr>

                      </thead>
                      <tbody>
                        
                        @php
                        $count = 1;
                        foreach ($valuesnode as $key => $order) {                       

                        $orderid=$order->order_id;
                        $orderid = str_replace("gid://shopify/Order/","",$orderid);
                        $ordername = $order->order_name;
                        $invoiceid = str_replace("#50000","",$ordername);
                        $invoiceid = "INV".$invoiceid;
                        $createdAt = date("m/d/Y",strtotime($order->order_created_at));
                        $customerid = $order->customer_id;
                        $fulfillments = $order->fulfillment_status;
                        $deposite = $order->deposite;
                        $status = $order->status;
                        $comment =$order->comment;                      
                        $totalamt = $order->total_amount;
                        $count = $count+1;
                    @endphp
                          <tr class="Polaris-DataTable__TableRow  toggle-addcsv individual-customer" data-toggle="modal" data-id="{{ $orderid }}" data-target="#orderModal" >
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{ $invoiceid }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{ $ordername }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $createdAt }}</td>
                          
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">${{ $totalamt ?? '' }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $deposite ?? 'Not Paid' }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $status ?? '' }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $fulfillments }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                              @php
                                if($status == "Settled"){

                              }
                              else{
                              @endphp
                              <label class="Polaris-Choice" for="PolarisCheckbox<?php echo  $ordername  ?>">
                              <span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="PolarisCheckbox<?php echo  $ordername  ?>" type="checkbox" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" value="true" name="selectforinvoice"><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></svg></span></span></span></span>
                            </label>
                            @php
                              }
                            @endphp
                            </td>


                          </tr>                              

                          @php
                              } 
                           @endphp 
                              </tbody>
                            </table>

                            
                          </div>                         
                        </div>
                         <div class="ui-page-actions">
                            <div class="ui-page-actions__container">
                              <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                                <form method="post" class="proceedforinvoice" id="proceedforinvoice">
                                  <input type="hidden" name="orderdata" value="" id="orderdata">
                                <div class="ui-page-actions__button-group">
                                  <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading createinvoice" type="submit" name="commit" value="Proceed" >Proceed</button>
                                </div>
                                </form>
                              </div>
                            </div>
                          </div>
                           @php
                          }else{
                          echo "No Invoice Available !";
                        }
                          @endphp  
                        <div class="button-for-pagination">
                          {{ $valuesnode->appends(request()->query())->links() }}
                      </div>
                  </div>
              </div>
                <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden step-panel step-panel-2" id="invoicedetailwrapper" role="tabpanel" aria-labelledby="accepts-marketing" tabindex="-1" style="display: none;">
                  <div class="order-detail-wrapper">
                    <div class="Polaris-Card__Section">
                       <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;" class="">
                        <div class="Polaris-Layout">
                          <div class="Polaris-Layout__Section">
                            
                            <div class="Polaris-Card">
                              
                             <div class="Polaris-Layout">

                                    <div class="Polaris-Layout__Section">
                                      <div class="Polaris-Card invoice-creation-detail-wrapper">
                                        <section class="ui-card__section customer-top-wrap invoice-detail-lineitem--wrap">
                                          <div class="Polaris-Card__Header">
                                            <h2 class="Polaris-Heading">Customer</h2>
                                          </div>
                                          
                                          <div class="ui-card__section">
                                            <div class="ui-type-container ui-type-container--spacing-tight">

                                              <div data-protected-personal-information="">
                                                <a href="">
                                                 <span id="first_name">first_name</span>
                                                 <span id="last_name">last_name</span>
                                               </a>          
                                             </div>

                                             <div class="ui-stack ui-stack--alignment-baseline ui-stack--spacing-none">
                                              <div class="ui-stack-item ui-stack-item--fill type--truncated">
                                                <a href="#" id="email">email</a>
                                              </div>
                                            </div>                                       
                                          </div>
                                        </div>
                                      </section>

                                      <section class="ui-card__section customer-top-wrap">
                                        <div class="ui-type-container">
                                          <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                                            <div class="ui-card__section-header"><h3 class="ui-subheading">Shipping address</h3></div>
                                          </div>   
                                          <div class="ui-type-container text-emphasis-subdued" data-protected-personal-information="true">
                                            <span id="sfirstname">sandeep</span>
                                            <span id="slastname">pangeni</span>
                                            <br><span id="company">Cartmade</span>
                                            <br><span id="address1">ktm</span>
                                            <br><span id="address2">fns</span><br>
                                            <span id="city">Kathmandu</span><span id="province_code"></span> , <span id="zip">44600</span><br>
                                            <span id="country_name"></span><br>
                                          </div>
                                        </div>
                                      </section>

                                      <section class="ui-card__section customer-top-wrap">
                                        <div class="ui-type-container">
                                          <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                                            <div class="ui-card__section-header"><h3 class="ui-subheading">Billing address</h3></div>
                                          </div>    
                                          <p class="text-emphasis-subdued" data-protected-personal-information="">
                                            Same as shipping address 
                                          </p>
                                        </div>
                                      </section>
                                    </div>
                                  </div>

                                  <div class="Polaris-Layout__Section invoicewrapper">
                                  
                                  </div>

                                </div>
                                <!-- end here -->
                            </div>
                            <div class="ui-page-actions Invoice_wrapper">
                            <div class="ui-page-actions__container">
                              <div class="date-picker-wrapper">
                                <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text" style="visibility: hidden;">Suggested Payment Date: </label></div>
                                  <div class="Polaris-TextField">
                                    <input id="PolarisTextField3" class="pickupdate Polaris-TextField__Input" value="">
                                    <div class="Polaris-TextField__Backdrop"></div>
                                  </div>
                                </div> 
                                <div class="invoiceresponse">
                                </div>                             
                              <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                                <div class="ui-page-actions__button-group">
                                  <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading sendinvoice" type="submit" name="Send Invoice" >Send Invoice</button>
                                </div>
                              </div>
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
           
            </div>
          </div>


          <style type="text/css">
            ul.pagination {
                display: flex;
            }
            #customer_id{
              display: none;
            }
            .Polaris-DataTable__ScrollContainer
            {margin-left:0px;}
            table.Polaris-DataTable__Table th, table.Polaris-DataTable__Table td {
                text-align: left;
            }
            .first-wrapper
            {
              margin: 0px 20px 20px 20px;
            }
            .heading-margin
            {
              margin-top: 0px;
              padding-top: 2.1rem;
            }
            .header-option-style
            {
              padding: 7px 0px 0px 0px;
            }
            form.customer-csv-upload {
                text-align: left;
                margin-left: 20px;
                padding-top: 15px;
            }
            .customerSel{
              background-color: #e1e5f2;
            }
            .custom-option select {
                width: 58% !important;
            }
          </style>
           <script src="{{ asset('public/js/_order.js') }}"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css"/>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.pickupdate').datepicker({
         dateFormat: "dd/mm/yy",
         minDate:3
      });
      $('.days li').click(function(){
        $('.datepicker-container').hide();
      });
      $('.pickupdate').click(function(){
        $('.datepicker-container').show();
      });

      
      $(document).on('click','button.resetfilter',function(e){
        e.preventDefault();
        var url = $('.resetsearch').attr("customerid");
        window.location.href = url;

      });

      $(document).on('click','button.createinvoice',function(e){
        e.preventDefault();
        var orderidarray = [];
        $('.orderlisting').find('tr').each(function(){         
          var selectforinvoice = $(this).find("input[name=selectforinvoice]").prop('checked');
          if(selectforinvoice){
              var orderid = $(this).attr("data-id");
              orderidarray.push(orderid);
          }
        });
        var orderdata = JSON.stringify(Object.assign({}, orderidarray));
        var data = {"orderdata":orderdata};
        // console.log(data);
        $.ajax({
          url: '/getorderinvoicedetail',
          dataType: 'json',
          type: 'post',
          data:data,
          beforeSend: function() {
              // setting a timeout
              // $('.response').html("Uploading ......");
          },
          success: function(orderdata)
          {              
          // console.log(orderdata);
          var count = 1;
          
          $.each( orderdata, function( key, value ) {
            orderdetail = value;
            console.log(orderdetail);
            if (count == 1) {
              var cust_id= orderdetail.customer.id;
              var email = orderdetail.customer.email;
              var first_name = orderdetail.customer.first_name;
              var last_name = orderdetail.customer.last_name;
              var addresses = orderdetail.customer.default_address;
              var company = addresses.company;
              var address1 = addresses.address1;
              var address2 = addresses.address2;
              var phone = orderdetail.customer.phone;
              var city = addresses.city;
              var province_code = addresses.provinceCode;
              var zip = addresses.zip;
              var country_name = addresses.country;
              $("#first_name , #sfirstname").text(first_name);
              $("#last_name , #slastname").text(last_name);
              $("#email").html(email);
              $("#company").html(company);
              $("#address1").html(address1);
              $("#address2").html(address2);
              $("#city").html(city);
              $("#province_code").html(province_code);
              $("#zip").html(zip);
              $("#country_name").html(country_name); 
            }
            count = count+1;
            var line_items = orderdetail.line_items;
            // console.log(line_items);
            var notes = orderdetail.note;
            if (notes) {
              $('.order_note').html(notes);
            }
            // $('.order_name').html(orderdetail.name);
            
            var ordername = orderdetail.name;
            var orderid =orderdetail.id;
            var grand_total =orderdetail.total_price;
            var due_amount = grand_total;
            var amtto_bepaid= "No deposit paid";
            var deposit_paid_status = "Not Paid";
            // $('.grand-total').html(grand_total);
            var total_discounts =orderdetail.total_discounts;
            if (!total_discounts) {
              var total_discounts = 0.00;
            }
            // $('#discount-amt').html(total_discounts);
            var subtotal_price =orderdetail.subtotal_price;
            // $('.sub-total').html(subtotal_price);

            var shipping_lines = orderdetail.shipping_lines;
            var shippingtitle = '';
              var shippingprice = '-';  
            
            // console.log(shippingtitle +" -  "+shippingprice);
            var cust_id= orderdetail.customer.id;
            var filepath = 'https://rbmembersportal.cartbrain.net/public/csvfiles/'+cust_id+'.csv';
            // var csvcontents = '';
            // var url_path = '/rbmembersportal.cartbrain.net/public/csvfiles/'+sel_customerid+'.csv';
            // console.log(filepath);
            
            //product sku vs price array
            // var csv_product = [];      
            // if (csvcontents != '') {       
            // $.each(csvcontents, function(key,val){
            //   var csv_sku = val.sku;
            //   var csv_price = val.Price;
            //   csv_product[csv_sku]=csv_price;
            // });
            // }

            var orderstatus ;
            var payment_html;
            var alreadypaid;
            var paymenthtml = '<select id="payment_value" class="" aria-invalid="false" name="Size">\
                    <option value="30">30% Deposit</option><option value="50" price="0">50% Deposit</option> \
                    <option value="100" price="0">Full Payment</option>\
                    </select>';
            var statusdata = {'orderid':orderid};            
            $.ajax({
              async: false,
               url: '/getorderinvoicestatusdetail',
                dataType: 'json',
                type: 'post',
                data:statusdata,
                beforeSend: function() {
                    // setting a timeout
                    // $('.response').html("Uploading ......");
                },
                success: getorderdata,

                error: function(data)   
                {
                    console.log(data);
                }
            });

            function getorderdata(orderdata)
            { 
              console.log(orderdata)
              if ((orderdata.length) > 0) {
              var order_status =orderdata[0].status;
              var amttobepaid = orderdata[0].amttobepaid;              
              // amtto_bepaid = amttobepaid;
              deposit_paid_status = orderdata[0].Deposit;
              if (amttobepaid == "30% Deposit" && deposit_paid_status=="Paid") {
                var paidpercentage = 30;
                alreadypaid = paidpercentage;
                amtto_bepaid = amttobepaid + " Paid";
              }
              if (amttobepaid == "50% Deposit" && deposit_paid_status=="Paid") {
                var paidpercentage = 50;
                alreadypaid = paidpercentage;
                amtto_bepaid = amttobepaid+ " Paid";
              }
              if (amttobepaid == "Full Payment" && deposit_paid_status=="Paid") {
                var paidpercentage = 100;
                alreadypaid = paidpercentage;
                amtto_bepaid = amttobepaid;
              }
              if (order_status == "Partially Paid"  || order_status == "Deposit paid") {
                var alreadypaid= parseInt(alreadypaid);
                var remaining = 100-alreadypaid;
                due_amount = (parseFloat(remaining)*parseFloat(grand_total))/100;
                console.log(due_amount);
                var payment_html = '<select id="payment_value" class="" aria-invalid="false" name="Size">\
                <option value="'+remaining+'">Remaining Balance</option>\
                </select>';
                paymenthtml = payment_html;
                orderstatus = order_status;
              }

            }
            }

            // console.log(orderstatus);
            var invoicehtml = '<div class="individual-invoice-wrapper" orderid="'+orderid+'" id="'+orderid+'">\
                                    <div class="Polaris-Card__Header" style="padding: 15px;">\
                                      <h2 class=" ordername" style="padding: 10px;">Order No : <span class="order_name" style="padding-top: 77px !important;">'+ordername+'</span></h2>\
                                    </div>\
                                    <div class="Polaris-Card">                     \
                                      <div class="next-card__section next-card__section--no-vertical-spacing result-row">\
                                        <table class="next-table--line-items">\
                                          <tbody id="line_item_rows_'+orderid+'" context="draftBuilder"> \
                                          </tbody>\
                                        </table>\
                                      </div>\
                                      <div class="next-card__section calculation-div">\
                                        <div class="wrappable" context="draftBuilder">\
                                          <div class="wrappable__item"></div>\
                                        <div class="wrappable__item">\
                                          <div>\
                                            <table class="table--no-border">\
                                              <tbody>\
                                                <tr>\
                                                  <td class="type--right">        \
                                                    <div class="ui-popover__container discount-popover-wrapper" style="position: relative;">\
                                                      <!-- <button type="button" class="btn btn--link" bind-disabled="calculating">Add discount</button> -->\
                                                      <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">\
                                                       <div class="discount-label-wrap">\
                                                         <div>\
                                                          <button type="button" class="add-discount-cta Polaris-Button btn btn--link" data-discount-percent="" data-discount-reason="" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper">Discount Amount</span></span></button>\
                                                         <p class="type--subdued sel-discount-name"></p>\
                                                       </div>\
                                                       </div>\
                                                     </div>\
                                                    \
                                                    </div>\
                                                  </td>\
                                                  <td class="type--right discount-value">\
                                                    <span class="type--subdued" id="discount-amt">$'+total_discounts+'</span>\
                                                  </td>\
                                                </tr>\
                                                <tr>\
                                                  <td class="type--right type--subdued">Subtotal</td>\
                                                  <td class="type--right sub-total">$'+subtotal_price+'</td>\
                                                </tr>\
                                                <tr>\
                                                  <td class="type--right" style="max-width: 100px;position: relative;">\
                                                  <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">\
                                                    <div class="shipping-label-wrap">\
                                                     <div><button type="button" class="add-shipping-cta Polaris-Button btn btn--link" data-shipping-type="'+value+'" data-custom-shipping-name="Two Day shipping" data-custom-shipping-amt="1" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper"> Shipping Method</span></span></button>\
                                                     <p class="type--subdued sel-shipping-name">'+shippingtitle+'</p>\
                                                   </div>\
                                                   </div>\
                                                    </div>\
                                                    \
                                                    </td>\
                                                    <td class="type--right">\
                                                      <span class="type--subdued" id="shipping-amt">$'+shippingprice+'</span>\
                                                    </td>\
                                                  </tr>\
                                                </tbody>\
                                                <tbody class="next-table--row-group-no-spacing">\
                                                  <tr class="tax_applied">\
                                                    <td class="type--right">          \
                                                      <button type="button" class="btn btn--link ">Taxes</button>  \
                                                    </td>\
                                                    <td class="type--right taxes-applied">\
                                                      0.00\
                                                    </td>\
                                                  </tr>\
                                                </tbody>\
                                                <tbody>\
                                                <tr>\
                                                    <td class="type--right" style="width: 50%;">\
                                                      <strong>Deposit</strong>\
                                                    </td>\
                                                    <td class="type--right deposite_paid" style="width: 50%;" totalamt="$'+amtto_bepaid+'">'+amtto_bepaid+'</td>\
                                                  </tr>\
                                                <tr style="display:none;">\
                                                    <td class="type--right" style="width: 50%;">\
                                                      <strong>Payment Status</strong>\
                                                    </td>\
                                                    <td class="type--right deposit_paid_status" style="width: 50%;" totalamt="$'+deposit_paid_status+'">'+deposit_paid_status+'</td>\
                                                  </tr>\
                                                  <tr>\
                                                    <td class="type--right" style="width: 50%;">\
                                                      <strong>Due Amount</strong>\
                                                    </td>\
                                                    <td class="type--right due_amount" style="width: 50%;" totalamt="$'+due_amount+'">$'+due_amount+'</td>\
                                                  </tr>\
                                                  <tr>\
                                                    <td class="type--right" style="width: 50%;">\
                                                      <strong>Total</strong>\
                                                    </td>\
                                                    <td class="type--right grand-total" style="width: 50%;" totalamt="$'+grand_total+'">$'+grand_total+'</td>\
                                                  </tr>\
                                                  <tr><td>\
                                                  <div class="custom-option">    \
                                                    <p><label><span class="label_index">Amount to be Paid</span></label></p>                        \
                                                    <div class="paymentwrapper">'+ paymenthtml +'                                   \
                                                    </div>                      \
                                                  </div>\
                                                  </d></tr>\
                                                </tbody>\
                                              </table>\
                                            </div>\
                                          </div>\
                                        </div>\
                                      </div>\
                                    </div> \
                                  </div>';
            $('.invoicewrapper').append(invoicehtml); 
            var ele= "#"+orderid;
            $(shipping_lines).each(function(i, shipval){
              var shippingtitle = shipval.title;
              var shippingprice = shipval.price; 
                   $(ele).find('.sel-shipping-name').html(shipval.title);
                    $(ele).find('#shipping-amt').html("$"+shipval.price);
              // console.log(shippingtitle +" -  "+shippingprice); 
            });
            var discount_detail = orderdetail.discount_codes;
            $(discount_detail).each(function(i, discountdetail){
              $(discount_detail).each(function(i, attr){
                console.log(attr);
                var name= attr.code;
                var value= attr.amount;
                $(ele).find('#discount-amt').html("$"+value);
                $(ele).find('.sel-discount-name').html(name);                    
              });
            });
            var csvcontents = '';
            $.ajax({
              url : filepath,
              dataType: "text",
              async: false,
              success : function (data) {
               csvcontents = $.csv.toObjects(data);
               // console.log(csvcontents);
             }
            });
            var csv_product = [];      
            if (csvcontents != '') {       
            $.each(csvcontents, function(key,val){
              var csv_sku = val.sku;
              var csv_price = val.Price;
              csv_product[csv_sku]=csv_price;
            });
            }
            // console.log(csvcontents);
            console.log(line_items);
            $(line_items).each(function(i, field){
              var productid = field.id;
              var properties = field.properties;
              // console.log(properties);
              var properties_all= '';
              $(properties).each(function(i, label){
                // console.log(label);
                var properties_individual ='<span class="property"><span class="name">'+label.name+' :</span><span class="value">'+label.value+'</span></span>'
                properties_all += properties_individual;
              });
              var image;
              var product_id = field.product_id;                    
              var product_id = {'product_id':product_id};
              var title = field.title;
              $.ajax({
                  async: false,
                  url: '/getproductimage',
                  dataType: 'json',
                  type: 'post',
                  data: product_id,
                  success: getproductimage,
                  error: function(data)
                  {
                      console.log(data);
                  }

                });
              function getproductimage(res)
              {
                var hasimage = res.length;
                if (hasimage > 0) {
                  var iprod_mage = res['0'].src;   
                  image =  iprod_mage;                      
                }else{
                  image = field.title;
                }
                console.log(image);
              }
              var price =field.price;
              var discount_amt = field.total_discount;
              var original_price = field.price;
              var qty= field.quantity;
              discount_amt = (parseFloat(discount_amt)/ parseFloat(qty)).toFixed(2); 
              var price = (parseFloat(original_price)-parseFloat(discount_amt)).toFixed(2);
              var itemtotal = ((parseFloat(original_price)-parseFloat(discount_amt))*parseFloat(qty)).toFixed(2);
              var sku = field.sku; 
              // if (csvcontents != '') {
              //   // console.log(csv_product[sku]);     
              //   if(typeof(csv_product[sku]) != "undefined"){
              //     console.log("inside csv");
              //     var csv_price = csv_product[sku];
              //     // console.log(csv_price); 
              //     var price = parseFloat(csv_price).toFixed(2);
              //     var itemtotal = (parseFloat(price)*parseFloat(qty)).toFixed(2);
              //   }
              // }
              console.log(price);                  

              var productrow = '<tr class="indiv-row-tr" data-attr-id="'+productid+'"><td class="next-table__image-cell hide-when-printing" width="1"><div class="aspect-ratio aspect-ratio--square aspect-ratio--square--50">\
                <img title="" class="productimage block aspect-ratio__content" src="'+image+'"></div>\
            </td>\
            <td class="next-table__cell--item-name" width="45%">\
              <a href="#" class="handle">'+title+'</a>\
              <p class="type--subdued">SKU: <span class="sku price">'+sku+'</span></p>\
              <p class="properties">'+properties_all+'</p>\
            </td>\
            <td class="type--left next-table__cell--grow-when-condensed" \
            width="1">               \
              <div class="ui-popover__container">\
                  <span class="price unit-price" data-custom-price="" data-unit-price="'+price+'" data-unit-discount="'+discount_amt+'" data-attr-org-price="'+original_price+'">$'+price+'</span>\
                  \
              </div>\
              <div class="item-custom-pricing">\
              </div>\
            </td>\
            <td class="type--subdued">✖</td>\
            <td style="min-width: 75px; width: 75px;">\
              <div class="next-input-wrapper" data-unit-price="'+price+'" data-custom-price=""><input min="1" max="1000000" class="next-input next-input--number qty-input next_input_wrapper" size="30" type="number" value="'+qty+'" name="" disabled>\
              </div>\
            </td>\
            <td class="type--right individual-total" class="total_sub_right">$'+itemtotal+'</td>\
            </tr> ';
            $('#line_item_rows_'+orderid+'').append(productrow);
            });            

          });
          // hide show tab
          $('li.step-tab').each(function(){
            $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
          });
          $('.step-tab-2').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
          //2. respective content area
          $('.step-panel').each(function(){
            $(this).hide();
          });
          $('.step-panel-2').show();
            
          },
          error: function(data)
          {
              console.log(data);
          }
      });

       // selecting order show

          $('.step-tab-1').on('click',function(){             
            $('li.step-tab').each(function(){
              $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
            });
            $('.step-tab-1').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
            $('.step-panel-2').hide();
            $('.step-panel-1').show();
            $('.invoicewrapper').empty();
            $('.invoiceresponse').empty();
          });

      });
     });

    $(document).on('click','button.sendinvoice',function(e){
      var totalamount = 0;
      $('.invoicewrapper').find(".individual-invoice-wrapper").each(function(){
        var totalamt = $(this).find('.grand-total').attr("totalamt");
        totalamount = totalamount + parseFloat(totalamt);        
      });

      var orderidarray = [];
      $('.invoicewrapper').find(".individual-invoice-wrapper").each(function(){
        var orderid = $(this).attr("orderid");
        var paymentpercent = $(this).find('#payment_value').val(); 
        orderidarray.push(orderid+":"+paymentpercent);     
      });  

      var orderdata = JSON.stringify(Object.assign({}, orderidarray));
      var paymentamt = totalamount;
      var paymentdate = $('.pickupdate').val();     
      var data = {"orderdata":orderdata,'paymentdate':paymentdate,'paymentamt':paymentamt}; 
      // console.log(data);
      $.ajax({
          url: 'https://cartbrain.net/rbinvoice/rbinvoice/invoice.php',
          dataType: 'json',
          crossDomain: true,
          type: 'post',
          data:data,
          beforeSend: function() {
              // setting a timeout
              $('.sendinvoice').html("Sending ...");
          },
          success: function(orderdata)
          { 
            console.log(orderdata);
            $('.sendinvoice').html("Send Invoice");
            $('.invoiceresponse').html("<span>Invoice Email Sent.</span>");
              setTimeout(function() {                 
                window.location.href = "/invoicecustomerlist";
              }, 2000);
          },
          error: function(data)
          {
              console.log(data);
              $('.invoiceresponse').html("<span>Failed.</span>");
          }
      });

    });
    </script>

@endsection

