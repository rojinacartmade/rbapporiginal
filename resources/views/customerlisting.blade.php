@extends('shopify-app::layouts.default')

@section('content')
 @include('custom-popup') 
 @include('shipping-popup') 


 <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.26.1/styles.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
  <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet">
  <!-- links for rb portal -->
  <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/ordercreation.css') }}">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="//cdn.shopify.com/s/files/1/0115/7490/2850/t/3/assets/jquery.csv.min.js?23815" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <!-- <script src="{{ asset('public/js/spectrum.js') }}"></script> -->
   
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
  ShopifyApp.init({
      apiKey: '427ad31ed9c45b59b4227a4187f91e7e',
      shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}'
  });
</script>
<div class="customer-tab-first first-wrapper">    
      <div class="Polaris-Page">        
        <div class="Polaris-Page-Header heading-margin  Polaris-Page-Header--mobileView">
             <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                  <div class="Polaris-Header-Title header-option-style">
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge ">Customers</h1>                  
                  </div>                
              </div>           
        </div>


        <div class="Polaris-Page__Content">
          <div class="Polaris-Card">
            <div class="sales-by-product-wrapper cm-customer-detail-wrapper">
              <div class="Polaris-DataTable__Navigation"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly" disabled="" aria-label="Scroll table left one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path>
              </svg></span></span></span></button><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Scroll table right one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path>
              </svg></span></span></span></button></div>
              <div class="Polaris-DataTable">
                <div class="Polaris-DataTable__ScrollContainer">
                  <table class="Polaris-DataTable__Table cm_polaris_table">
                    <thead>
                      <tr>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Name</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Email</th>
                        
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Telephone</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Zip</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Country</th>
                        <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">State/Province</th>
                        
                      </tr>

                    </thead>
                    <tbody>
                      
                     @php

                      foreach ($valuesnode as $key => $customer) {

                        $customer_id=$customer->id;
                        $customer_id=str_replace("gid://shopify/Customer/","",$customer_id);
                        $firstName = $customer->firstName;
                        $email = $customer->email;
                        $phone = $customer->phone;
                        $addresses=$customer->addresses[0];
                        $provinceCode=$addresses->provinceCode;
                        $country = $addresses->country;
                        $province = $addresses->province;
                        $createdAt = $customer->createdAt;
                        $customerforattr = json_encode($customer);
                        $zip=$addresses->zip;
                    @endphp
                        <tr class="Polaris-DataTable__TableRow  toggle-addcsv individual-customer" data-toggle="modal" data-id="{{ $customer_id }}" data-target="#orderModal" >
                          <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{$firstName}}</td>
                          <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $email }}</td>
                        
                          <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $phone }}</td>
                            <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $zip }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $country }}</td>
                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $province }}</td>
                                 

                                </tr>

                                <tr class="customer-row Polaris-DataTable__TableRow" style="display:none;" id="customer-{{$customer_id}}">
                                  <td colspan="8" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                                    <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                                      <form method="POST" action="/rbfileupload" class="customer-csv-upload" enctype="multipart/form-data">
                                        @csrf
                                        <div class="Polaris-FormLayout">
                                          <div class="Polaris-Page-Header__TitleActionMenuWrapper-FormLayout__Item fileuploaderwrapper">
                                              <input type="hidden" name="rb_customer_id" value="{{ $customer_id }}">
                                              
                                              <input type="file" class="btn Polaris-Button--plain choosefile" name="csvfile" accept="csv">
                                              <div class="upload-wrapper">
                                              <input type="submit" class="btn Polaris-Button--plain Polaris-Button Polaris-Button--seconday rbuploadfile" name="submit" value="upload">
                                               @php
                                                   $filename = $customer_id.".csv";
                                                    $filepath = "https://rbmembersportal.cartbrain.net/public/csvfiles/".$filename;
                                                    @endphp 
                                                    @if(file_exists(public_path('/csvfiles/'.$filename.'')) )     
                                                                                       
                                                 <!-- file already exists error -->
                                               <div class="download-link">

                                                 <a href="{{ $filepath }}" class="btn downloadfile">Download file </a>
                                                 <span><?php echo $filename ?></span>
                                               </div>
                                             @endif
                                           </div>
                                          </div>
                                        </div>
                                      </form>
                                    </div>

                                  </td>
                                </tr>

                                @php
                                    } 
                                 @endphp 
                            </tbody>
                          </table>


                        </div>
                      </div>
                           @php
  $fornext=$allcursors[count($allcursors) - 1];
  $forprev=$allcursors['0'];
  $hasNextPageProd = $hasNextPageCust;
  $hasPreviousPageProd=$hasPreviousPageCust;

@endphp
<div class="button-for-pagination">
  @if($hasPreviousPageProd)
 <form class="productbackform" method="post" action="/customertab">  
    <input  name="hasforback" value="{{$forprev}}" type="hidden">
    <input  name="hasbackprod" value="{{$hasPreviousPageProd}}" type="hidden">
    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Back</span></span></button></div>
</form>
@else
    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button" disabled="disabled"><span class="Polaris-Button__Content"><span>Back</span></span></button></div>

@endif
@if($hasNextPageProd)
 <form class="productnextform" method="post" action="/customertab">  
    <input  name="hasfornext" value="{{$fornext}}" type="hidden">
    <input  name="hasnextprod" value="{{$hasNextPageProd}}" type="hidden">
    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Next</span></span></button></div>
    </form>
@else
  <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button" disabled="disabled"><span class="Polaris-Button__Content"><span>Next</span></span></button></div>
@endif
                    </div>
              </div>
          </div>
   
    </div>
  </div>


          <style type="text/css">
            #customer_id{
              display: none;
            }
            .Polaris-DataTable__ScrollContainer
            {margin-left:0px;}
            table.Polaris-DataTable__Table th, table.Polaris-DataTable__Table td {
                text-align: left;
            }
            .first-wrapper
            {
              margin: 0px 20px 20px 20px;
            }
            .heading-margin
            {
              margin-top: 0px;
              padding-top: 2.1rem;
            }
            .header-option-style
            {
              padding: 7px 0px 0px 0px;
            }
            form.customer-csv-upload {
                text-align: left;
                margin-left: 20px;
                padding-top: 15px;
            }
            /*.customerSel{
              background-color: #e1e5f2;
            }*/
            .upload-wrapper .Polaris-Button--plain:hover{
              border: 1px solid;
            }
          </style>
          <script type="text/javascript">
            $(document).ready(function(){
              $('.individual-customer').click(function(){
                // $( this ).addClass( "customerSel" );

                // $('.cm_polaris_table').find( ".individual-customer" ).each(function() {
                //   var issel = $( this ).hasClass( "customerSel" );
                //   if(issel){
                //     $( this ).removeClass("customerSel");
                //     $( this ).removeClass("active");
                //   }
                  
                // });
                var issel = $(this).hasClass("customerSel");
                 console.log(issel);
                  if(issel){
                    $(this).removeClass("customerSel");
                    $(this).removeClass("active");
                  }else{
                    $( this ).addClass( "customerSel" );
                    $( this ).addClass( "active" );
                  }
                
                var customerid = $(this).attr('data-id');
                var targetele = '#customer-'+customerid;
                $(targetele).toggle();
                $(targetele).addClass("active");
              });
            });

               // $(document).on("click",".rbuploadfile",function() {
               //    var rb_customer_id=$("input[name=rb_customer_id]").val();

               //    event.preventDefault();
               //      $.ajax
               //      ({
               //        url: '/rbfileupload',
               //        headers: {
               //            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               //        },
               //        data: {
               //            'rb_customer_id' : rb_customer_id                          
               //        },
               //        type: 'POST',
               //         beforeSend:function(){                        
                          
               //        },
               //        success: function(result)
               //        {

               //        },
               //        error: function(err)
               //        {
               //            // console.log(err);
               //            alert("opps, something went wrong");
               //        }
               //    });

               // });
            

          </script>

@endsection