
		Hi {{ $shop_name_for_email }}, <br> <br>Thank you for installing the cartmade handpick related app!. I'm sandesh, the support specialist of the app.<br><br>
		We are excited that you have downloaded the app and hope you find it useful!<br><br>

		If you have any questions or need support to get up and running, contact us at sandesh.paudyal@cartmade.com .

		<br><br>
		Best regards,
		<br><br>
		Sandesh Paudyal.
		<br><br>
		Cartmade Handpick Related
		<br>
		