@extends('shopify-app::layouts.default')

@section('content')
 @include('custom-popup') 
 @include('shipping-popup') 

<link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.26.1/styles.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
  <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet">
  <!-- links for rb portal -->
  <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/ordercreation.css') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="//cdn.shopify.com/s/files/1/0115/7490/2850/t/3/assets/jquery.csv.min.js?23815" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <!-- <script src="{{ asset('public/js/spectrum.js') }}"></script> -->
   
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
  ShopifyApp.init({
      apiKey: '427ad31ed9c45b59b4227a4187f91e7e',
      shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}'
  });
</script>
<style type="text/css">
  a#navigation_tab_1 span {
    border-bottom: .3rem solid #5c6ac4 !important;
}
  input.item-to-custom-price {
    width: 15px;
    height: 15px;
    border: 1px solid #212b36;
    position: relative;
}
input.item-to-custom-price {
    width: 15px;
    height: 15px;
    border: 1px solid #212b36;
    position: relative;
}
input.item-to-custom-price:checked:after {
    content: '✔';
    color: #fff;
    width: 16px;
    height: 16px;
    display: flex;
    justify-content: center;
    border: 1px solid #5A68C3;
    border-radius: 2px;
    background: #5A68C3;
    position: absolute;
    left: -2px;
    top: -2px;
}
.Polaris-TextField {
    z-index: 1 !important;
}
textarea.order_note {
    margin-bottom: 5px !important;
}
.isempty {
    border: 1px solid red !important;
}
</style>
  <div class="customer-tab-first first-wrapper"> 
 <div class="Polaris-Page">     
      <div class="Polaris-Page-Header heading-margin  Polaris-Page-Header--mobileView">
       <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
        <div class="Polaris-Header-Title header-option-style">
          <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge ">Create Order</h1>                  
        </div>                
      </div>           
    </div>
    <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;" class = "Polaris-Page__Content">
      <div class="Polaris-Card">
        <div>
          <ul role="tablist" class="Polaris-Tabs">
            <li class="Polaris-Tabs__TabContainer step-tab step-tab-1"><button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers"><span class="Polaris-Tabs__Title">1. Select Customer</span></button></li>
            <li class="Polaris-Tabs__TabContainer step-tab step-tab-2"><button id="accepts-marketing" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="accepts-marketing-content"><span class="Polaris-Tabs__Title">2. Choose products</span></button></li>
            <!-- <li class="Polaris-Tabs__TabContainer step-tab step-tab-3"><button id="repeat-customers" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="repeat-customers-content"><span class="Polaris-Tabs__Title">3. Order confirmation</span></button></li> -->
          </ul>
          <div class="Polaris-Tabs__Panel step-panel step-panel-1" id="all-customers-content" role="tabpanel" aria-labelledby="all-customers" tabindex="-1">

            <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">

              <div class="Polaris-Page__Content table-head-margin">
                <div class="Polaris-Card">
                  <div class="">
                    <div class="Polaris-DataTable">
                      <div class="Polaris-DataTable__ScrollContainer">
                        <table class="Polaris-DataTable__Table sel-customer-table">
                          <thead>
                            <tr>
                              <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header" scope="col">Name</th>
                              <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header " scope="col">Email</th>
                              <!-- <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header " scope="col">Group</th> -->
                              <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header " scope="col">Telephone</th>
                              <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header " scope="col">Zip</th>
                              <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header " scope="col">Country</th>
                              <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header " scope="col">State/Province</th>
                              <!-- <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header " scope="col">Created-at</th> -->
                            </tr>

                          </thead>
                              <tbody>

                            @php
                            
                           
                           
                            foreach ($valuesnode as $key => $customer) {

                              $customer_id=$customer->id;
                              $firstName = $customer->firstName;
                              $email = $customer->email;
                              $phone = $customer->phone;
                              $addresses=$customer->addresses[0];
                              $provinceCode=$addresses->provinceCode;
                              $country = $addresses->country;
                              $province = $addresses->province;
                              $createdAt = $customer->createdAt;
                              $customerforattr = json_encode($customer);
                              $zip=$addresses->zip;
                           
                          @endphp
                              <tr class="Polaris-DataTable__TableRow selectedcustomer" data-sel-customerid='{{ $customerforattr }}' >
                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop">{{ $firstName }}</td>
                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop ">{{ $email }}</td>
                               <!--  <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop ">curr</td>
 -->
                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop ">{{ $phone }}</td>
                                 <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop ">{{ $zip }}</td>
                                  <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop ">{{ $country }}</td>
                                   <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop ">{{ $province }}</td>
                                   <!--  <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop ">{{ $createdAt }}</td> -->

                                    </tr>
                                    @php
                                    } 
                                 @endphp 
                                   
                                  
                                </tbody>

                          
                              </table>
                              <div></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                                  @php
  $fornext=$allcursors[count($allcursors) - 1];
  $forprev=$allcursors['0'];
  $hasNextPageProd = $hasNextPageCust;
  $hasPreviousPageProd=$hasPreviousPageCust;

@endphp
<div class="button-for-pagination">
  @if($hasPreviousPageProd)
 <form class="productbackform" method="post" action="/paginatecustomer">  
    <input  name="hasforback" value="{{$forprev}}" type="hidden">
    <input  name="hasbackprod" value="{{$hasPreviousPageProd}}" type="hidden">
    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Back</span></span></button></div>
</form>
@else
    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button" disabled="disabled"><span class="Polaris-Button__Content"><span>Back</span></span></button></div>

@endif
@if($hasNextPageProd)
 <form class="productnextform" method="post" action="/paginatecustomer">  
    <input  name="hasfornext" value="{{$fornext}}" type="hidden">
    <input  name="hasnextprod" value="{{$hasNextPageProd}}" type="hidden">
    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Next</span></span></button></div>
    </form>
@else
  <div class="Polaris-Connected__Item Polaris-Connected__Item--connection"><button type="submit" class="productsearchbtn Polaris-Button" disabled="disabled"><span class="Polaris-Button__Content"><span>Next</span></span></button></div>
@endif

</div>
                </div>




                <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden step-panel step-panel-2" id="selectproduct" role="tabpanel" aria-labelledby="accepts-marketing" tabindex="-1" style="display: none;">
                 <div class="order-creation-wrapper">
                  <div class="Polaris-Card__Section">
                   <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;" class="">
                    <div class="Polaris-Layout">

                      <div class="Polaris-Layout__Section">
                        <div class="Polaris-Card">
                          <section class="ui-card__section customer-top-wrap">
                            <div class="Polaris-Card__Header">
                              <h2 class="Polaris-Heading">Customer</h2>
                            </div>
                            
                            <div class="ui-card__section">
                              <div class="ui-type-container ui-type-container--spacing-tight">

                                <div data-protected-personal-information="">
                                  <a href="">
                                   <span id="first_name"></span>
                                   <span id="last_name"></span>
                                 </a>          
                               </div>

                               <div class="ui-stack ui-stack--alignment-baseline ui-stack--spacing-none">
                                <div class="ui-stack-item ui-stack-item--fill type--truncated">
                                  <a  href="#" id="email"></a>
                                </div>
                              </div>                                       
                            </div>
                          </div>
                        </section>

                        <section class="ui-card__section customer-top-wrap">
                          <div class="ui-type-container">
                            <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                              <div class="ui-card__section-header"><h3 class="ui-subheading">Shipping address</h3></div>
                            </div>   
                            <div class="ui-type-container text-emphasis-subdued" data-protected-personal-information="true">
                              <span id="sfirstname"></span>
                              <span id="slastname"></span>
                              <br><span id="company"></span>
                              <br><span id="address1"></span>
                              <br><span id="address2"></span><br>
                              <span id="city"></span><span id="province_code"></span> , <span id="zip"></span><br>
                              <span id="country_name"></span><br>
                              <span><a id="shipping-edit">Edit Shipping Address</a></span><br>
                            </div>
                          </div>
                        </section>

                        <section class="ui-card__section customer-top-wrap">
                          <div class="ui-type-container">
                            <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                              <div class="ui-card__section-header"><h3 class="ui-subheading">Billing address</h3></div>
                            </div>    
                            <p class="text-emphasis-subdued" data-protected-personal-information="">
                              Same as shipping address 
                            </p>
                          </div>
                        </section>
                      </div>
                    </div>

                    <div class="Polaris-Layout__Section customer-top-wrap">
                      <div class="Polaris-Card">

                        <div class="Polaris-Card__Header">
                          <h2 class="Polaris-Heading">Add products</h2>
                        </div>

                        <section class="next-card__section next-card__section--no-border hide-when-printing">
                          <div class="ui-form__section">
                            <div class="next-input-wrapper browse-products-wrap">
                              <div class="next-field__connected-wrapper">
                                <div class="next-input--stylized next-field--connected">
                                  <input type="search" name="" id="productselectsearch" class="next-input next-input--search next-input--invisible js-no-dirty">
                                </div>
                                <button class="ui-button next-field--connected--no-flex"  type="button" class="productselect" name="" id="productselect">Browse products
                                </button>
                              </div>
                            </div>
                          </div>          
                        </section>

                        <div class="next-card__section next-card__section--no-vertical-spacing result-row">
                          <table class="next-table--line-items">
                            <tbody id="line_item_rows" context="draftBuilder">                    
                              <!-- append the order list -->
                            </tbody>
                          </table>
                        </div>

                        <!-- div ends here -->

                        <div class="next-card__section calculation-div">
                          <div class="wrappable" context="draftBuilder">
                            <div class="wrappable__item">
                              <div class="date-picker-wrapper">
                                <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Need by Date</label></div>
                                  <div class="Polaris-TextField">
                                    <input id="PolarisTextField3" class="pickup-date Polaris-TextField__Input" value="">
                                    <div class="Polaris-TextField__Backdrop"></div>
                                  </div>
                             </div>
                             <div class="po-number-wrapper">
                              <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Reference</label></div>
                              <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="">
                                <div class="Polaris-TextField__Backdrop"></div>
                              </div>
                            </div>
                            <div class="next-input-wrapper order-note-wrapper"><label class="next-label" for="note">Notes</label>
                              <textarea expanding="true" placeholder="" class="order_note" name="order_note"></textarea>
                              <div class="only-when-printing"></div>
                            </div>
                            <div class="fileinput-wrapper" filepath="false">
                              <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Attach File</label></div>
                              <div class="Polaris-TextField">
                                <form method="POST" action="/rborderfileupload" class="rborderfileupload" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="rb_customer_id" class="rb_customer_id" value="">
                                <input type="file" id="PolarisTextField3" class="Polaris-TextField__Input uploadorderfile" value="" name="orderfile" >
                                <div class="rbuploadorderfile-wrapper">
                                 
                                  <span class="response"></span>
                                </div>
                              </form>
                              </div>
                            </div>
                          </div>
                          <div class="wrappable__item">
                            <div>
                              <table class="table--no-border">
                                <tbody>
                                  <tr>
                                    <td class="type--right">        
                                      <div class="ui-popover__container discount-popover-wrapper" style="position: relative;">
                                        <!-- <button type="button" class="btn btn--link" bind-disabled="calculating">Add discount</button> -->
                                        <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                                         <div class="discount-label-wrap">
                                           <div>
                                            <button type="button" class="add-discount-cta Polaris-Button btn btn--link" data-discount-percent="" data-discount-reason="" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper">Add discount</span></span></button>
                                           <p class="type--subdued sel-discount-name"></p>
                                         </div>
                                         </div>
                                       </div>
                                       <div class="discount-popover ui-popover ui-popover--align-edge ui-popover--is-positioned-beneath ui-popover--is-active" context="discountPopover" data-popover-horizontally-relative-to-closest=".next-card" data-popover-css-vertical-margin="8" data-popover-css-horizontal-margin="16" data-popover-css-max-height="300" data-popover-css-max-width="550" id="ui-popover--3" aria-labelledby="ui-popover-activator--3" aria-expanded="true" role="dialog" style="max-width: none;margin-right: 0px;margin-left: 0px;left: -203px;top: 23px;z-index: 9999;position: absolute;"><div class="ui-popover__content-wrapper"><div class="ui-popover__content" style="max-height: 300px; width: 285px;">
                                        <div class="ui-popover__pane" style="min-width: 245px;">
                                          <div class="ui-popover__section">
                                            <div style="max-width: 200px;" class="next-input-wrapper"><label class="next-label" for="discount">Discount this order by</label><div class="next-field__connected-wrapper"><!--
                                              <button disabled="disabled" class="ui-button next-field--connected--no-flex active" bind-event-click="value_type='fixed_amount'" bind-class="{active: value_type == 'fixed_amount'}" type="button" name="button">$</button>
                                              <button class="ui-button next-field--connected--no-flex" bind-event-click="value_type='percentage'" bind-class="{active: value_type == 'percentage'}" type="button" name="button">%</button> -->
                                              <input bind="value_type" class="js-no-dirty" size="30" type="hidden" name="draft_order[applied_discount][value_type]" id="draft_order_applied_discount_value_type" value="fixed_amount">
                                              <input bind="title" class="js-no-dirty" size="30" type="hidden" name="draft_order[applied_discount][title]" id="draft_order_applied_discount_title" value="">
                                              <label class="next-label helper--visually-hidden" for="draft_order_applied_discount_value">Value</label><div class="next-input--stylized next-field--connected">

                                                <span bind-show="value_type == &quot;fixed_amount&quot;" class="next-input__add-on next-input__add-on--before hide">$</span>

                                                <input bind="value" class="order-discount-input next-input next-input--invisible js-money-field js-no-dirty" size="30" type="text" name="draft_order[applied_discount][value]" id="draft_order_applied_discount_value">
                                                <span bind-show="value_type == &quot;percentage&quot;" class="next-input__add-on next-input__add-on--after">%</span></div>
                                            </div></div>    <div class="next-input-wrapper"><label class="next-label" for="draft_order_applied_discount_description">Reason</label><input class="discount-reason next-input js-no-dirty" bind="description" placeholder="Damaged item, loyalty discount" size="30" type="text" name="draft_order[applied_discount][description]" id="draft_order_applied_discount_description"></div>
                                          </div></div><div class="ui-popover__pane ui-popover__pane--fixed">
                                            <div class="next-grid next-grid--no-padding">
                                              <div class="next-grid__cell">
                                                <button class="ui-button close-discount-popover" bind-event-click="Shopify.UIPopover.deactivate()" type="button" name="button">Close</button>
                                              </div>
                                              <div class="next-grid__cell next-grid__cell--no-flex">
                                                <button class="apply-discount-btn ui-button btn-primary" type="button" name="button">Apply</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div></div></div>
                                      </div>
                                    </td>
                                    <td class="type--right discount-value">
                                      <span class="type--subdued" id="discount-amt">—</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="type--right type--subdued">Subtotal</td>
                                    <td class="type--right sub-total">-</td>
                                  </tr>
                                  <tr>
                                    <td class="type--right" style="max-width: 100px;position: relative;">
                                    <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                                         <div class="shipping-label-wrap">
                                           <div><button type="button" class="add-shipping-cta Polaris-Button btn btn--link" data-shipping-type="null" data-custom-shipping-name="" data-custom-shipping-amt="" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper" >Add Shipping</span></span></button>
                                           <p class="type--subdued sel-shipping-name"></p>
                                         </div>
                                         </div>
                                       </div>
                                    <div class="shipping-popover ui-popover ui-popover--is-positioned-beneath ui-popover--is-active" define="{enterKeyHandler: new Shopify.EnterKeyHandler(this, setShippingLine)}" data-popover-horizontally-relative-to-closest=".next-card" data-popover-css-vertical-margin="8" data-popover-css-horizontal-margin="16" data-popover-css-max-height="300" data-popover-css-max-width="550" id="ui-popover--4" aria-labelledby="ui-popover-activator--4" aria-expanded="true" role="dialog" style="max-width: none;margin-right: 0px;margin-left: 0px;left: -207px;transform-origin: 0px center;top: -90px;position: absolute;z-index: 999;"><div class="ui-popover__content-wrapper"><div class="ui-popover__content" style="max-height: 285.5px; width: 280px;">
                                        <div class="ui-popover__pane">
                                          <div class="ui-popover__section" define="{ selectedRateHandle: &quot;custom&quot; }">

                                            <!-- shipping options -->

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_ground_shipping" data-shipping-cost="5">Ground</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_ground_shipping" value="Ground shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_threeday_shipping" data-shipping-cost="5">Three Day</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_threeday_shipping" value="Three Day shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>
                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_twoday_shipping" data-shipping-cost="5">Two Day</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_twoday_shipping" value="Two Day shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_nextdayair_shipping" data-shipping-cost="5">Next Day Air</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_nextdayair_shipping" value="Next Day Air shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>
                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_nextdayairam_shipping" data-shipping-cost="5">Next Day Air AM</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_nextdayairam_shipping" value="Next Day Air AM shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>
                                              

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_thirdpartyground_shipping" data-shipping-cost="5">Third party Ground</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_thirdpartyground_shipping" value="Third party Ground shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_thirdpartythreeday_shipping" data-shipping-cost="5">Third party Three Day</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_thirdpartythreeday_shipping" value="Third party Three Day shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_thirdpartytwoday_shipping" data-shipping-cost="5">Third party two day</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_thirdpartytwoday_shipping" value="Third party two day shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_thirdpartynextday_shipping" data-shipping-cost="5">Third party next day</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_thirdpartynextday_shipping" value="Third party next day shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>

                                              <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_free_shipping_custom_thirdpartythreedayAM_shipping" data-shipping-cost="5">Third party next day AM</label>

                                              <input type="radio" name="custom_free_shipping" id="custom_free_shipping_custom_thirdpartythreedayAM_shipping" value="Third party next day AM shipping" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>


                                            <div class="custom-shipping-method" style="display: none;">
                                            <div class="next-input-wrapper"><label class="next-label next-label--switch" for="custom_custom">Custom</label>
                                              <input type="radio" name="custom_free_shipping" id="custom_custom" value="custom" bind="selectedRateHandle" class="next-radio js-no-dirty"><span class="next-radio--styled"></span></div>

                                            <div class="next-card__section--half-spacing">
                                              <div class="next-grid next-grid--compact next-grid--no-outside-padding">
                                                <div class="next-grid__cell">
                                                  <div class="next-input-wrapper"><label class="next-label helper--visually-hidden" for="shippingTitle">Shippingtitle</label><input type="text" name="shippingTitle" id="shippingTitle" placeholder="Custom rate name" class="next-input js-no-dirty" bind="validShippingRates.custom.title" bind-event-focus="selectedRateHandle = &quot;custom&quot;"></div>
                                                </div>
                                                <div class="next-grid__cell next-grid__cell--third">
                                                  <div class="next-input-wrapper"><label class="next-label helper--visually-hidden" for="shippingRate">Shippingrate</label><div class="next-input--stylized"><span class="next-input__add-on next-input__add-on--before">$</span><input type="text" name="shippingRate" id="shippingRatecustom" bind="validShippingRates.custom.price" class="next-input next-input--invisible js-money-field js-no-dirty" bind-event-focus="selectedRateHandle = &quot;custom&quot;"></div></div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          </div></div>
                                          <div class="ui-popover__pane ui-popover__pane--fixed">
                                            <div class="custom-shipping-method" >
                                              <div class="next-card__section--half-spacing">
                                                <div class="next-grid next-grid--compact next-grid--no-outside-padding">

                                                <div class="next-grid__cell">
                                                <label>Shipping Rate</label>
                                                <div class="next-input-wrapper "><label class="next-label helper--visually-hidden" for="shippingRate">Shippingrate</label><div class="next-input--stylized shippingratewrapper"><span class="next-input__add-on next-input__add-on--before">$</span><input type="number" name="shippingRate" id="addshippingRate" bind="validShippingRates.custom.price" class="next-input next-input--invisible js-money-field js-no-dirty" bind-event-focus="selectedRateHandle = &quot;custom&quot;"></div></div>
                                                </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="next-grid next-grid--no-padding">
                                              <div class="next-grid__cell">
                                                <button class="ui-button close-shipping-popover" bind-event-click="Shopify.UIPopover.for(this).deactivate()" type="button" name="button">Close</button>
                                              </div>
                                              <div class="next-grid__cell next-grid__cell--no-flex">
                                               <button class="apply-shipping-btn ui-button btn-primary" type="button" name="button">Apply</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div></div></div>   
                                      </td>
                                      <td class="type--right">
                                        <span class="type--subdued" id="shipping-amt">—</span>
                                      </td>
                                    </tr>
                                    <tr>
                                    <td class="type--right type--subdued"><label class="Polaris-Choice" for="PolarisCheckbox2"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="PolarisCheckbox2" type="checkbox" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" value="true" name="markaspaid"><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></svg></span></span></span></span><span class="Polaris-Choice__Label">Open Account</span></label></td>
                                   
                                  </tr>
                                  </tbody>

                                  <tbody class="next-table--row-group-no-spacing">
                                    <tr class="tax_applied">
                                      <td class="type--right">          
                                        <button type="button" class="btn btn--link ">Taxes</button>                
                                      </td>
                                      <td class="type--right taxes-applied">
                                        0.00
                                      </td>
                                    </tr>
                                  </tbody>
                                  <tbody>
                                    <tr>
                                      <td class="type--right" style="width: 50%;">
                                        <strong>Total</strong>
                                      </td>
                                      <td class="type--right grand-total" style="width: 50%;">
                                        <strong>-</strong>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div style="display:none; --top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">

                        <div class=" Polaris-Card next-card__section next-card__section--bordered">
                          <div class="wrappable wrappable--half-spacing wrappable--vertically-centered wrappable--right-aligned">
                            <div class="wrappable__item wrappable__item--no-flex">
                              <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20 next-icon--no-nudge" aria-hidden="true" focusable="false"> <use xlink:href="#next-document"></use> </svg>
                            </div>
                            <div class="wrappable__item">
                              <h2 class="next-heading next-heading--small next-heading--no-margin">
                                Email invoice
                              </h2>
                            </div>
                            <div class="wrappable__item wrappable__item--no-flex">
                              <button class="ui-button ui-button--primary btn-disabled" id="" data-bind-event-click=")" data-bind-class="" type="button" name="button">Email invoice</button>
                            </div>
                          </div>
                        </div>

                        <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                          <div class="Polaris-Card next-card__section ">
                            <div class="wrappable wrappable--half-spacing wrappable--vertically-centered wrappable--right-aligned">
                              <div class="wrappable__item wrappable__item--no-flex">
                                <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20 next-icon--no-nudge" aria-hidden="true" focusable="false"> <use xlink:href="#next-credit-card"></use> </svg>
                              </div>
                              <div class="wrappable__item">
                                <h2 class="next-heading next-heading--small next-heading--no-margin">
                                  Accept payment
                                </h2>
                              </div>
                              <div class="wrappable__item wrappable__item--no-flex">
                                <div class="button-group button-group--right-aligned">
                                  <button class="ui-button btn-disabled" id="" data-bind-event-click="" data-bind-class="" data-bind-disabled="" type="button" name="button">Mark as paid</button>

                                  <button class="ui-button btn-disabled" id="" data-bind-event-click="" data-bind-class="" data-bind-disabled="" type="button" name="button">Mark as pending</button>

                                  <a class="tooltip tooltip-bottom btn btn-separate btn-disabled" href="#">
                                    <div class="tooltip-container">
                                      <span class="tooltip-label">
                                        Credit card gateway required
                                      </span>
                                    </div>
                                    Pay with credit card
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="ui-page-actions">
                    <div class="ui-page-actions__container">
                      <div class="order-creation-status"></div>
                      <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                        <div class="ui-page-actions__button-group">
                          <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading create-final-order" type="submit" name="commit" value="Create order">Create order</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden step-panel step-panel-3" id="repeat-customers-content" role="tabpanel" aria-labelledby="repeat-customers" tabindex="-1" style="display: none;">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <script src="{{ asset('public/js/_order.js') }}"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css"/>
  <script>
    $(document).ready(function() {

      $('.pickup-date').datepicker({
         dateFormat: "dd/mm/yy",
         minDate:3
      });
      $('.days li').click(function(){
        $('.datepicker-container').hide();
      });
      $('.pickup-date').click(function(){
        $('.datepicker-container').show();
      });
      
      //  $('#datepicker,.pickup-date').datepicker({
      //   dateFormat: "dd/mm/yy",
      //   minDate:3
      // });

      $('.variant-height-input,.variant-size-input').select2();

      //discount and shipping
  $('.apply-discount-btn').click(function(){
        var discount_input = $('.order-discount-input').val();
        var discount_reason = $('.discount-reason').val();
        if(discount_input && !isNaN(discount_input)){
          $('.add-discount-cta').attr('data-discount-percent',discount_input);
          if(discount_reason){
            $('.add-discount-cta').attr('data-discount-reason',discount_reason);
            $('.sel-discount-name').text(discount_input+'% off('+discount_reason+')');
          }else{
            $('.add-discount-cta').attr('data-discount-reason','');
            $('.sel-discount-name').text(discount_input+'% off');
          }
          $('.discount-popover').css('display','none');
        }
        updatesubtotalandtotal();
      });
      $('.apply-shipping-btn').click(function(){
        var selected_shipping = $('input[name="custom_free_shipping"]:checked').val(); 
        var shippingTitle = selected_shipping;     
        var data_shipping_cost = 0;
        var custom_shipping_name = $('#shippingTitle').val();
        var custom_shipping_amt = parseFloat($('#addshippingRate').val());
        if(selected_shipping=='custom'){
          //look if name and amout is given
          if(custom_shipping_name && custom_shipping_amt && !isNaN(custom_shipping_amt)){
            //is valid custom shipping
            $('.add-shipping-cta').attr('data-shipping-type',selected_shipping).attr('data-custom-shipping-name',custom_shipping_name).attr('data-custom-shipping-amt',custom_shipping_amt);
            $('.sel-shipping-name').text(selected_shipping);
            $('.shipping-popover').css('display','none');
          }
        }else{
          if (isNaN(custom_shipping_amt)) {
            console.log("here");
            $('.shippingratewrapper').addClass("isempty");
          }
          else{
          var data_shipping_cost =custom_shipping_amt;
          $('.shippingratewrapper').removeClass("isempty");
          $(".create-final-order").removeClass("btn-disabled");  
          $('.order-creation-status').html(" ");  
          $('.add-shipping-cta').attr('data-shipping-type',selected_shipping).attr('data-custom-shipping-name',selected_shipping).attr('data-custom-shipping-amt',data_shipping_cost);
          $('.sel-shipping-name').text(selected_shipping);
          $('#shipping-amt').text(data_shipping_cost);
          $('.shipping-popover').css('display','none');    
            
          }
        }
        updatesubtotalandtotal();
      });

      $('.add-discount-cta,.close-discount-popover').click(function(){
        if(!$('.discount-popover').is(':visible')){
            $('.discount-popover').css('display','block');
        }else{
            $('.discount-popover').css('display','none');
        }
      });
      $('.add-shipping-cta,.close-shipping-popover').click(function(){
        if(!$('.shipping-popover').is(':visible')){
            $('.shipping-popover').css('display','block');
        }else{
            $('.shipping-popover').css('display','none');
        }
      });

      // $('form.rborderfileupload').submit(function(event) {
      // uploadorderfile.onchange = function(e) {
      $('input[type=file]').change(function() { 
        event.preventDefault();
        var formData = new FormData($('.rborderfileupload')[0]);
        // var form_data = new FormData('.rborderfileupload');
        // console.log(form_data);
       $.ajax({
           url:"/rborderfileupload",
           method:"POST",
           data:formData,
           dataType:'JSON',
           contentType: false,
           cache: false,
           processData: false,
            beforeSend: function() {
                // setting a timeout
                $('.response').html("Uploading ......");
            },
            success: function(result)
            {
                // location.reload();
                console.log(result);
                $('.fileinput-wrapper').attr('filepath',result);
                $('.response').html("Attachment uploaded !!");
            },
            error: function(data)
            {
                console.log(data);
                $('.response').html("Failed !!");
            }
        });

    });

      $('.Polaris-TextField').click(function(){
        $(this).removeClass("isempty");
        $('.order-creation-status').html("");
        $(".create-final-order").removeClass("btn-disabled");
      });
      

    });
  </script>


@endsection