@extends('shopify-app::layouts.default')

@section('content')


 <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.26.1/styles.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
  <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet">
  <!-- links for rb portal -->
  <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/ordercreation.css') }}">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="//cdn.shopify.com/s/files/1/0115/7490/2850/t/3/assets/jquery.csv.min.js?23815" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <!-- <script src="{{ asset('public/js/spectrum.js') }}"></script> -->
   
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
  ShopifyApp.init({
      apiKey: '427ad31ed9c45b59b4227a4187f91e7e',
      shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}'
  });
</script>
<div class="customer-tab-first first-wrapper">    
      <div class="Polaris-Page">        
        <div class="Polaris-Page-Header heading-margin  Polaris-Page-Header--mobileView">
             <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                  <div class="Polaris-Header-Title header-option-style">
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge ">Select an Invoice Email to Pay</h1>                  
                  </div>                
              </div>           
        </div>


          <div class="Polaris-Page__Content">
            <div class="Polaris-Card">
              <div>
                <ul role="tablist" class="Polaris-Tabs">
                  <li class="Polaris-Tabs__TabContainer step-tab step-tab-1"><button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers"><span class="Polaris-Tabs__Title">1. Invoice Lists</span></button></li>
                  <li class="Polaris-Tabs__TabContainer step-tab step-tab-2"><button id="accepts-marketing" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="accepts-marketing-content"><span class="Polaris-Tabs__Title">2. Invoice Detail</span></button></li>
                </ul>
              <div class="Polaris-Tabs__Panel step-panel step-panel-1" id="all-customers-content" role="tabpanel" aria-labelledby="all-customers" tabindex="-1">
              
              <div class="sales-by-product-wrapper rb-invoices-content-wrap">
                <div class="Polaris-DataTable__Navigation"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly" disabled="" aria-label="Scroll table left one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Scroll table right one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button></div>
                <div class="Polaris-DataTable">
                  <div class="Polaris-DataTable__ScrollContainer">
                    <table class="Polaris-DataTable__Table orderlisting">
                      <thead>
                        <tr>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Date Sent</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col"> Comments</th>
                          
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Status</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Action</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col"></th>
                        </tr>

                      </thead>
                      <tbody>
                        
                       @php

                        foreach ($valuesnode as $key => $order) { 
                          $deletestatus = false;
                          $processpayment = true;
                          $invoicedate = date("m/d/Y",strtotime($order->invdate));

                          $comment = $order->comment;
                          $status = $order->status;

                          if(!$status){
                            $status = "Awaiting Payment";
                            $deletestatus = true;
                          }
                          if($status == "Paid" || $status == "Overriden" ){
                          $deletestatus = true;
                          $processpayment = false;
                         }
                          
                          $customerid = $order->customerid;
                          $invoice_group_id = $order->invoice_group_id;
                      @endphp
                            <tr class="Polaris-DataTable__TableRow  individual_order " data-toggle="modal"  id="inv_{{ $invoice_group_id }}" data-groupid="{{ $invoice_group_id }}" data-target="#orderModal" >
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">{{ $invoicedate }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $comment }}</td>
                            
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $status }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                                @php
                                if($processpayment) { 
                                @endphp 
                                <a class="btn-invoiceemails-action processpayment_invoice" >Process Payments</a>
                                @php
                                }
                                @endphp 
                              </td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                                @php
                                if($deletestatus) { 
                                @endphp 
                                <a class="btn-invoiceemails-action delete_invoice" >Delete</a>
                                @php
                                } 
                                @endphp 
                              </td>
                            </tr>                              

                            @php
                                } 
                             @endphp 
                              </tbody>
                            </table>

                            
                          </div>
                        </div>
                      <div class="button-for-pagination">
                         {{ $valuesnode->appends(request()->query())->links() }}
                      </div>
                  </div>
              </div>
              <!-- secondtab -->
              <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden step-panel step-panel-2" id="invoicedetailwrapper" role="tabpanel" aria-labelledby="accepts-marketing" tabindex="-1" style="display: none;">
                  <div class="order-detail-wrapper">
                    <div class="Polaris-Card__Section">
                       <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;" class="">
                        <div class="Polaris-Layout">
                          <div class="Polaris-Layout__Section">
                            
                            <div class="Polaris-Card">
                              
                             <div class="Polaris-Layout">

                                  <div class="Polaris-Layout__Section">
                                  <div class="Polaris-Card payments--description-wrapper">
                                    <div class="next-card__section calculation-div">
                                      <div class="wrappable" context="draftBuilder">
                                        <div class="wrappable__item">                                                                              
                                        <div class="next-input-wrapper order-note-wrapper"><label class="next-label" for="note">Comments</label>
                                          <textarea expanding="true" placeholder="" class="order_note" name="order_note"></textarea>
                                          <div class="only-when-printing"></div>
                                        </div>
                                        
                                        </div>
                                        <div class="wrappable__item">
                                        <div>
                                          <div class="paymentwrapper">
                                            <div class="custom-option custom-option-Size">                        
                                              <p><label><span class="label_index">Payment Method</span></label></p>                        
                                              <div class="variant-height-input-wrap">                                   
                                                <select id="paymentmethod" class=" variant-closure-input selectvalue" aria-invalid="false" name="Size"> 
                                                <option value="Check">Check</option>
                                                <option value="banktransfer">Bank Transfer Payment</option>
                                                <option value="authorizenet">Credit Card Authorize.net</option>
                                                <option value="Cash">Cash</option>                    
                                                <option value="paypal_express">PayPal Express Checkout</option>
                                                <option value="free">No Payment Information Required</option>                        
                                                </select>                        
                                              </div>                      
                                            </div>
                                        </div>
                                          
                                        </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                  <div class="Polaris-Layout__Section invoicewrapper">
                                    <div class="Polaris-DataTable">
                                      <div class="Polaris-DataTable__ScrollContainer">
                                        <table class="Polaris-DataTable__Table orderlisting">
                                          <thead>
                                            <tr>
                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Invoice Number</th>
                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Order Number</th>
                                            
                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Status</th>
                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Amount to be Paid</th>
                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Amount</th>
                                            
                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">
                                              
                                            </th>
                                            </tr>

                                          </thead>
                                          <tbody class="individualinvoice-wrapper">
                                            
                                          </tbody>                                                
                                          </table>
                                        </div> 
                                      </div>
                                  </div>

                                </div>
                                <!-- end here -->
                            </div>
                            <div class="ui-page-actions">
                                <div class="ui-page-actions__container">
                                  <div class="payment-status"></div>
                                  <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                                    <div class="ui-page-actions__button-group">
                                      <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading processpayment" type="submit" name="commit" value="Process Payment">Process Payment</button>
                                    </div>
                                  </div>
                                </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
           
            </div>
          </div>


          <style type="text/css">
            a#navigation_tab_5 span {
                border-bottom: .3rem solid #5c6ac4 !important;
            }
            .fieldwrap{
                width: 190px !important;
                margin-bottom: 15px!important;
                margin-left: 10px;
              }
            .order_filter form{
              /*display: inline-flex;*/
              margin-top: 15px;
            }
            .fieldswrapper{
              display: inline-flex;
            }
            .buttonwrapper {
                display: inline-flex;
                margin-bottom: 15px;
            }
            .resersearch{
              margin-top: 0px !important;
            }
            ul.pagination {
                display: flex;
            }
            #customer_id{
              display: none;
            }
            .Polaris-DataTable__ScrollContainer
            {margin-left:0px;}
            table.Polaris-DataTable__Table th, table.Polaris-DataTable__Table td {
                text-align: left;
            }
            .first-wrapper
            {
              margin: 0px 20px 20px 20px;
            }
            .heading-margin
            {
              margin-top: 0px;
              padding-top: 2.1rem;
            }
            .header-option-style
            {
              padding: 7px 0px 0px 0px;
            }
            form.customer-csv-upload {
                text-align: left;
                margin-left: 20px;
                padding-top: 15px;
            }
            .customerSel{
              background-color: #e1e5f2;
            }
            .btn-invoiceemails-action{
              display: inline-block;
              text-align: center;
              line-height: normal;
              padding: 0.5625rem 0.9375rem;
              background: none;
              background-color: #444;
              border-radius: 0.125rem;
              color: #fff;
            }
            .btn-invoiceemails-action:hover{
              color: #fff;
            }
            ._1MkCc.active{
              border-bottom: .3rem solid #5c6ac4 !important;
            }

          </style>
    <script type="text/javascript">
      $(document).ready(function(){         
          //Step 1 : order selection
          $('.processpayment_invoice').on('click',function(){
            var groupid = $(this).parent().parent().attr('data-groupid');
            $('.processpayment').attr("groupid",groupid);
            var paymentdata = $(this).parent().parent().attr('data-paymentdata');
            var groupid = {'groupid':groupid};
            $.ajax({
                url: '/getinvoicefromgroup',
                dataType: 'json',
                type: 'post',
                data: groupid,
                beforeSend: function() {
                    // setting a timeout
                    // $('.response').html("Uploading ......");
                },
                success: function(orderdetail)
                {
                  console.log(orderdetail);
                  $(orderdetail).each(function(i, invoice){
                      var percentage ;
                      var amttobepaid= invoice.amttobepaid;
                      
                      if (amttobepaid == "Full Payment") {
                        var percentage = 100;
                      }
                      else{
                         var percentage = amttobepaid.replace("% Deposit","");
                         var percentage = parseFloat(percentage);
                      }
                      var invoice_number= invoice.invoice_number; 
                      var orderid = invoice.orderid; 
                      var amount= invoice.amount; 
                      var tobepaid = (parseFloat(amount)*parseFloat(percentage))/100;                     
                      var status = invoice.status;
                      var depositepaid= invoice.Deposit;
                      var checkbox ='<label class="Polaris-Choice" for="PolarisCheckbox'+orderid+'">\
                                                  <span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="PolarisCheckbox'+orderid+'" type="checkbox" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" value="true" name="selectforinvoice"><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></svg></span></span></span></span>\
                                                </label>';
                      if(status == "Paid"){
                        checkbox= '';
                      }
                      if(status != "Pending" && depositepaid == "Paid"){
                        checkbox= '';
                      }
                      var linehtml='<tr class="Polaris-DataTable__TableRow  toggle-addcsv individual-customer" data-toggle="modal" orderid="'+orderid+'" data-target="#orderModal"  paymentdata="'+amttobepaid+'" >\
                                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">INV'+invoice_number+'</td>\
                                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">#50000'+invoice_number+'</td>\
                                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">'+status+'</td>\
                                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">'+amttobepaid+'</td>\
                                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">$'+tobepaid.toFixed(3)+'</td>\
                                                <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">'+checkbox+'\
                                                </td>\
                                              </tr>';
                      console.log(orderid);
                      $('.individualinvoice-wrapper').append(linehtml);

                    });
                   // hide show tab
                  $('li.step-tab').each(function(){
                    $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
                  });
                  $('.step-tab-2').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
                  //2. respective content area
                  $('.step-panel').each(function(){
                    $(this).hide();
                  });
                  $('.step-panel-2').show();
                },
                error: function(data)
                {
                    console.log(data);

                }
            });


        });

          // selecting order show

          $('.step-tab-1').on('click',function(){             
            $('li.step-tab').each(function(){
              $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
            });
            $('.step-tab-1').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
            $('.step-panel-2').hide();
            $('.step-panel-1').show();
            $('.individualinvoice-wrapper').empty();
          });


          $('.processpayment').on('click',function(){
            var orderarray = [];
            var groupid = $('.processpayment').attr("groupid");
            $('.individualinvoice-wrapper').find('tr').each(function(){
              var selectforinvoice = $(this).find("input[name=selectforinvoice]").prop('checked');              
              if(selectforinvoice){
                  var orderid = $(this).attr("orderid");
                  var paymentdata = $(this).attr('paymentdata');
                  var orderdata = {'orderid':orderid,'paymentdata':paymentdata};
                  orderarray.push(orderdata);
              }
            });
            var orderarray = JSON.stringify(orderarray);
            var comment = $('.order_note').val();
            if (($('.order_note').val().trim().length)<1) {
              // $('.payment-status').html("Comment Section Should be filled.");
            }
            var paymentmethod = $('#paymentmethod').val();

            var data ={"orderarray":orderarray,"comment":comment,"paymentmethod":paymentmethod,"groupid":groupid};
            console.log(data);
            $.ajax({
                url: '/updatepaymentstatus',
                dataType: 'json',
                type: 'post',
                data: data,
                beforeSend: function() {
                    // setting a timeout
                    // $('.response').html("Uploading ......");
                },
                success: function(res)
                {
                  console.log(res);
                  $('.payment-status').html(res);
                  setTimeout(function() {                 
                    window.location.href = "/invoicepaymentcustomerlist";
                  }, 2000);

                },
                error: function(data)
                {
                    console.log(data);
                }
            });

          });

        $('.delete_invoice').on('click',function(){
          var groupid = $(this).parent().parent().attr("data-groupid");
          var data ={"groupid":groupid};
          var ele = "#inv_"+groupid;
          console.log(data);
          $.ajax({
              url: '/deletegroupinvoice',
              dataType: 'json',
              type: 'post',
              data: data,
              beforeSend: function() {
                  // setting a timeout
                  $(ele).find(".delete_invoice").html("Deleting");
              },
              success: function(res)
              {
                console.log(res);
                $(ele).find(".delete_invoice").html("Deleted");
                setTimeout(function() {                 
                  window.location.href = "/invoicepaymentcustomerlist";
                  location.reload();
                }, 2000);

              },
              error: function(data)
              {
                  console.log(data);
              }
          });
        });


        let url = window.location.href;
        if(url.includes('invoiceemailslist')){
          console.log("invoiceemailslist");
          $('#navigation_tab_5').find("._1MkCc").addClass("active");
        }
      });
    </script>

@endsection