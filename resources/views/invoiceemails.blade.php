@extends('shopify-app::layouts.default')

@section('content')


 <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.26.1/styles.min.css"/>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{ asset('public/css/custom.css') }}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
  <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet">
  <!-- links for rb portal -->
  <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/css/ordercreation.css') }}">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="//cdn.shopify.com/s/files/1/0115/7490/2850/t/3/assets/jquery.csv.min.js?23815" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <!-- <script src="{{ asset('public/js/spectrum.js') }}"></script> -->
   
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
  ShopifyApp.init({
      apiKey: '427ad31ed9c45b59b4227a4187f91e7e',
      shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}'
  });
</script>
<div class="customer-tab-first first-wrapper">    
      <div class="Polaris-Page">        
        <div class="Polaris-Page-Header heading-margin  Polaris-Page-Header--mobileView">
             <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                  <div class="Polaris-Header-Title header-option-style">
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge ">Invoices</h1>                  
                  </div>                
              </div>           
        </div>


          <div class="Polaris-Page__Content">
            <div class="Polaris-Card">
              <div>
                <ul role="tablist" class="Polaris-Tabs">
                  <li class="Polaris-Tabs__TabContainer step-tab step-tab-1"><button id="all-customers" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true" aria-controls="all-customers-content" aria-label="All customers"><span class="Polaris-Tabs__Title">1. Invoice Lists</span></button></li>
                  <li class="Polaris-Tabs__TabContainer step-tab step-tab-2"><button id="accepts-marketing" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="accepts-marketing-content"><span class="Polaris-Tabs__Title">2. Invoice Detail</span></button></li>
                </ul>
              <div class="Polaris-Tabs__Panel step-panel step-panel-1" id="all-customers-content" role="tabpanel" aria-labelledby="all-customers" tabindex="-1">
              <div class="order_filter" class="wrappable__item" style="display: none;">
                <form action="/ordersearch" method="post">
                  <div class="fieldswrapper">
                  <div class="ordernumber-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Order Number</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="" name="ordernumber">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                  <div class="firstname-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Firstname</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="" name="firstname">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                  <div class="lastname-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Lastname</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="" name="lastname">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                  
                  <div class="fulfillment-wrapper fieldwrap">
                    <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">Delivery Status:</label></div>
                    <!-- <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="" name="deliverystatus">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div> -->

                    <div class="custom-option custom-option-Size">       
                      <!-- <p> -->
                       <!--  <label>
                          <span class="label_index">Fulfillment status</span><span class="required">*</span>
                        </label> -->
                      <!-- </p> -->
                      <div class="ariant-height-input-wrap">
                        <select class=" variant-closure-input selectvalue" aria-invalid="false" name="deliverystatus">
                           <option value="Unfulfilled">Unfulfilled</option>
                           <option value="Fulfilled">Fulfilled</option>    
                        </select>                       
                       </div>  
                     </div>

                    <!-- <div class="">
                      <select  class="" name="deliverystatus">
                        <option value="Unfulfilled">Unfulfilled</option>
                        <option value="Fulfilled">Fulfilled</option>
                      </select>
                      <div class="Polaris-TextField__Backdrop"></div> -->
                    <!-- </div>  -->
                  </div>
                  <div class="school-wrapper fieldwrap">
                   <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text">School:</label></div>
                    <div class="Polaris-TextField"><input id="PolarisTextField3" class="Polaris-TextField__Input" value="" name="schoolname">
                      <div class="Polaris-TextField__Backdrop"></div>
                    </div>
                  </div>
                </div>

                <div class="buttonwrapper">
                  <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                    <div class="ui-page-actions__button-group">
                      <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading filterorder" type="submit" name="commit" >Filter</button>
                    </div>
                  </div>
                
                <div >
                
                <form method="post" action="/getorder" class="resersearch">

                <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                  <div class="ui-page-actions__button-group">
                    <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading resetfilter" type="submit" name="commit" value="search" >Reset</button>
                  </div>
                </div>
                </form>
                </div>
                </div>
                </form>
                
              </div>
              <div class="sales-by-product-wrapper">
                <div class="Polaris-DataTable__Navigation"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly" disabled="" aria-label="Scroll table left one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Scroll table right one column"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path>
                </svg></span></span></span></button></div>
                <div class="Polaris-DataTable">
                  <div class="Polaris-DataTable__ScrollContainer">
                    <table class="Polaris-DataTable__Table orderlisting">
                      <thead>
                        <tr>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Invoice Number</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Comment</th>
                          
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Status</th>
                          <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col"> Action</th>
                        </tr>

                      </thead>
                      <tbody>
                        
                       @php
                        foreach ($valuesnode as $key => $order) {                       

                          $orderid=$order->orderid;
                          $invoice_number = $order->invoice_number;
                          $customer_first_name = $order->customer_first_name;
                          $customer_last_name = $order->customer_last_name;
                          $customername = $customer_first_name." ".$customer_last_name;
                          $customerid = $order->customerid;
                          $amttobepaid = $order->amttobepaid;
                          $status = $order->status;                        
                          $school = $order->school;                                                 
                          $total = $order->amount;
                          $comments = $order->comment;
                      @endphp
                            <tr class="Polaris-DataTable__TableRow  individual_order " data-toggle="modal" data-orderid="{{ $orderid }}" data-paymentdata= "{{ $amttobepaid }}" data-school="{{ $school }}" data-target="#orderModal" >
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn" scope="row">INV{{ $invoice_number }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $comments }}</td>
                            
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{{ $status }}</td>
                              <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric"><a class="btn-invoiceemails-action" >Process Payments</a></td>
                              

                            </tr>                              

                            @php
                                } 
                             @endphp 
                              </tbody>
                            </table>

                            
                          </div>
                        </div>
                      <div class="button-for-pagination">
                         {{ $valuesnode->appends(request()->query())->links() }}
                      </div>
                  </div>
              </div>

                    <!-- second tab for order detail -->
              <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden step-panel step-panel-2" id="orderdetailwrapper" role="tabpanel" aria-labelledby="accepts-marketing" tabindex="-1" style="display: none;">
                  <div class="order-detail-wrapper">
                    <div class="Polaris-Card__Section">
                       <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;" class="">
                        <div class="Polaris-Layout">
                          <div class="Polaris-Layout__Section">
                            
                            <div class="Polaris-Card">
                              <div class="Polaris-Card__Header" style="padding: 15px;">
                                <h2 class=" ordername" style="padding: 10px;">Order No : <span class="order_name" style="padding-top: 77px !important;">#500001066</span></h2>
                            </div>
                             <div class="Polaris-Layout">

                                    <div class="Polaris-Layout__Section">
                                      <div class="Polaris-Card" style="display: none;">
                                        <section class="ui-card__section customer-top-wrap">
                                          <div class="Polaris-Card__Header">
                                            <h2 class="Polaris-Heading">Customer</h2>
                                          </div>
                                          
                                          <div class="ui-card__section">
                                            <div class="ui-type-container ui-type-container--spacing-tight">

                                              <div data-protected-personal-information="">
                                                <a href="">
                                                 <span id="first_name">first_name</span>
                                                 <span id="last_name">last_name</span>
                                               </a>          
                                             </div>

                                             <div class="ui-stack ui-stack--alignment-baseline ui-stack--spacing-none">
                                              <div class="ui-stack-item ui-stack-item--fill type--truncated">
                                                <a href="#" id="email">email</a>
                                              </div>
                                            </div>                                       
                                          </div>
                                        </div>
                                      </section>

                                      <section class="ui-card__section customer-top-wrap">
                                        <div class="ui-type-container">
                                          <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                                            <div class="ui-card__section-header"><h3 class="ui-subheading">Shipping address</h3></div>
                                          </div>   
                                          <div class="ui-type-container text-emphasis-subdued" data-protected-personal-information="true">
                                            <span id="sfirstname">sandeep</span>
                                            <span id="slastname">pangeni</span>
                                            <br><span id="company">Cartmade</span>
                                            <br><span id="address1">ktm</span>
                                            <br><span id="address2">fns</span><br>
                                            <span id="city">Kathmandu</span><span id="province_code"></span> , <span id="zip">44600</span><br>
                                            <span id="country_name"></span><br>
                                          </div>
                                        </div>
                                      </section>

                                      <section class="ui-card__section customer-top-wrap">
                                        <div class="ui-type-container">
                                          <div class="ui-stack ui-stack--wrap ui-stack--distribution-equal-spacing">
                                            <div class="ui-card__section-header"><h3 class="ui-subheading">Billing address</h3></div>
                                          </div>    
                                          <p class="text-emphasis-subdued" data-protected-personal-information="">
                                            Same as shipping address 
                                          </p>
                                        </div>
                                      </section>
                                    </div>
                                  </div>

                                <div class="Polaris-Layout__Section">
                                  <div class="Polaris-Card">
                                    <div class="next-card__section calculation-div">
                                      <div class="wrappable" context="draftBuilder">
                                        <div class="wrappable__item">                                                                              
                                        <div class="next-input-wrapper order-note-wrapper"><label class="next-label" for="note">Comments</label>
                                          <textarea expanding="true" placeholder="" class="order_note" name="order_note"></textarea>
                                          <div class="only-when-printing"></div>
                                        </div>
                                        
                                        </div>
                                        <div class="wrappable__item">
                                        <div>
                                          <div class="paymentwrapper">
                                            <div class="custom-option custom-option-Size">                        
                                              <p><label><span class="label_index">Payment Method</span></label></p>                        
                                              <div class="variant-height-input-wrap">                                   
                                                <select id="paymentmethod" class=" variant-closure-input selectvalue" aria-invalid="false" name="Size"> 
                                                <option value="Check">Check</option>
                                                <option value="banktransfer">Bank Transfer Payment</option>
                                                <option value="authorizenet">Credit Card Authorize.net</option>
                                                <option value="Cash">Cash</option>                    
                                                <option value="paypal_express">PayPal Express Checkout</option>
                                                <option value="free">No Payment Information Required</option>                        
                                                </select>                        
                                              </div>                      
                                            </div>
                                        </div>
                                          
                                        </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                  <div class="Polaris-Layout__Section customer-top-wrap">
                                    <div class="Polaris-Card">                       

                                      <div class="next-card__section next-card__section--no-vertical-spacing result-row">
                                        <table class="next-table--line-items">
                                          <tbody id="line_item_rows" context="draftBuilder">                    
                                          <!-- append the order list -->
                                        

                                        </tbody>
                                        </table>
                                      </div>

                                      <!-- div ends here -->

                                      <div class="next-card__section calculation-div">
                                        <div class="wrappable" context="draftBuilder">
                                          <div class="wrappable__item">
                                            <div class="date-picker-wrapper">
                                              <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text"><b>Need by Date :</b></label> <span class="pickup-date "></span></div>
                                                
                                           </div>
                                           <div class="po-number-wrapper">
                                            <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text"><b>Reference :</b></label><span class="reference"></span></div>
                                            
                                          </div>
                                          <div class="next-input-wrapper order-note-wrapper"><label class="next-label" for="note"><b>Notes :</b></label>
                                            <span class="order_note"></span>
                                            <div class="only-when-printing"></div>
                                          </div>
                                          <div class="fileinput-wrapper" filepath="false">
                                            <div class="Polaris-Label"><label id="PolarisTextField3Label" for="PolarisTextField3" class="Polaris-Label__Text"><b>Attach File</b></label></div>
                                            <div class="Polaris-TextField">
                                              <a href="#filelink" id="attachment_link" download>Attachment</a>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="wrappable__item">
                                          <div>
                                            <table class="table--no-border">
                                              <tbody>
                                                <tr>
                                                  <td class="type--right">        
                                                    <div class="ui-popover__container discount-popover-wrapper" style="position: relative;">
                                                      <!-- <button type="button" class="btn btn--link" bind-disabled="calculating">Add discount</button> -->
                                                      <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                                                       <div class="discount-label-wrap">
                                                         <div>
                                                          <button type="button" class="add-discount-cta Polaris-Button btn btn--link" data-discount-percent="" data-discount-reason="" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper">Discount Amount</span></span></button>
                                                         <p class="type--subdued sel-discount-name"></p>
                                                       </div>
                                                       </div>
                                                     </div>
                                                    
                                                    </div>
                                                  </td>
                                                  <td class="type--right discount-value">
                                                    <span class="type--subdued" id="discount-amt">0.00</span>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td class="type--right type--subdued">Subtotal</td>
                                                  <td class="type--right sub-total">25.68</td>
                                                </tr>
                                                <tr>
                                                  <td class="type--right" style="max-width: 100px;position: relative;">
                                                  <div style="--top-bar-background:#00848e; --top-bar-background-lighter:#1d9ba4; --top-bar-color:#f9fafb;">
                                                    <div class="shipping-label-wrap">
                                                     <div><button type="button" class="add-shipping-cta Polaris-Button btn btn--link" data-shipping-type="Two Day shipping" data-custom-shipping-name="Two Day shipping" data-custom-shipping-amt="1" tabindex="0" aria-controls="Polarispopover8" aria-owns="Polarispopover8" aria-expanded="true" aria-haspopup="false"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text discount-text-wrapper"> Shipping Method</span></span></button>
                                                     <p class="type--subdued sel-shipping-name">Two Day shipping</p>
                                                   </div>
                                                   </div>
                                                    </div>
                                                    
                                                    </td>
                                                    <td class="type--right">
                                                      <span class="type--subdued" id="shipping-amt">—</span>
                                                    </td>
                                                  </tr>
                                                </tbody>

                                                <tbody class="next-table--row-group-no-spacing">
                                                  <tr class="tax_applied">
                                                    <td class="type--right">          
                                                      <button type="button" class="btn btn--link ">Taxes</button>                
                                                    </td>
                                                    <td class="type--right taxes-applied">
                                                      0.00
                                                    </td>
                                                  </tr>
                                                </tbody>
                                                <tbody>
                                                  <tr>
                                                    <td class="type--right" style="width: 50%;">
                                                      <strong>Total</strong>
                                                    </td>
                                                    <td class="type--right grand-total" style="width: 50%;">25.68</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>                  
                                  </div>
                                </div>
                                <!-- end here -->
                            </div>
                          </div>
                        </div>
                        <div class="ui-page-actions">
                        <div class="ui-page-actions__container">
                          <div class="payment-status"></div>
                          <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                            <div class="ui-page-actions__button-group">
                              <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary btn-primary has-loading processpayment" type="submit" name="commit" value="Process Payment">Process Payment</button>
                            </div>
                          </div>
                        </div>
                  </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
           
            </div>
          </div>


          <style type="text/css">
            .btn-invoiceemails-action{
              display: inline-block;
              text-align: center;
              line-height: normal;
              padding: 0.5625rem 0.9375rem;
              background: none;
              background-color: #444;
              border-radius: 0.125rem;
              color: #fff;
            }
            .btn-invoiceemails-action:hover{
              color: #fff;
            }
            .fieldwrap{
                width: 190px !important;
                margin-bottom: 15px!important;
                margin-left: 10px;
              }
            .order_filter form{
              /*display: inline-flex;*/
              margin-top: 15px;
            }
            .fieldswrapper{
              display: inline-flex;
            }
            .buttonwrapper {
                display: inline-flex;
                margin-bottom: 15px;
            }
            .resersearch{
              margin-top: 0px !important;
            }
            ul.pagination {
                display: flex;
            }
            #customer_id{
              display: none;
            }
            .Polaris-DataTable__ScrollContainer
            {margin-left:0px;}
            table.Polaris-DataTable__Table th, table.Polaris-DataTable__Table td {
                text-align: left;
            }
            .first-wrapper
            {
              margin: 0px 20px 20px 20px;
            }
            .heading-margin
            {
              margin-top: 0px;
              padding-top: 2.1rem;
            }
            .header-option-style
            {
              padding: 7px 0px 0px 0px;
            }
            form.customer-csv-upload {
                text-align: left;
                margin-left: 20px;
                padding-top: 15px;
            }
            .customerSel{
              background-color: #e1e5f2;
            }

          </style>
    <script type="text/javascript">
      
        $(document).ready(function(){         
          //Step 1 : order selection
          $('.btn-invoiceemails-action').on('click',function(){
            var orderid = $(this).parent().parent().attr('data-orderid');
            var paymentdata = $(this).parent().parent().attr('data-paymentdata');
            var orderid = {'orderid':orderid};
            $.ajax({
                url: '/getorderdetail',
                dataType: 'json',
                type: 'post',
                data: orderid,
                beforeSend: function() {
                    // setting a timeout
                    // $('.response').html("Uploading ......");
                },
                success: function(orderdetail)
                {
                    
                  console.log(orderdetail);
                  var orderid = orderdetail.id;
                  $('.processpayment').attr('orderid',orderid);
                  $('.processpayment').attr('paymentdata',paymentdata);                  
                  var email = orderdetail.customer.email;
                  var first_name = orderdetail.customer.first_name;
                  var last_name = orderdetail.customer.last_name;
                  var addresses = orderdetail.customer.default_address;
                  var company = addresses.company;
                  var address1 = addresses.address1;
                  var address2 = addresses.address2;
                  var phone = orderdetail.customer.phone;
                  var city = addresses.city;
                  var province_code = addresses.provinceCode;
                  var zip = addresses.zip;
                  var country_name = addresses.country;
                  $("#first_name , #sfirstname").text(first_name);
                  $("#last_name , #slastname").text(last_name);
                  $("#email").html(email);
                  $("#company").html(company);
                  $("#address1").html(address1);
                  $("#address2").html(address2);
                  $("#city").html(city);
                  $("#province_code").html(province_code);
                  $("#zip").html(zip);
                  $("#country_name").html(country_name); 
                  var line_items = orderdetail.line_items;
                  console.log(line_items);
                  var notes = orderdetail.note;
                  if (notes) {
                    $('.order_note').html(notes);
                  }
                  $('.order_name').html(orderdetail.name);
                  var grand_total =orderdetail.total_price;
                  $('.grand-total').html(grand_total);
                  var total_discounts =orderdetail.total_discounts;
                  $('#discount-amt').html(total_discounts);
                  var subtotal_price =orderdetail.subtotal_price;
                  $('.sub-total').html(subtotal_price);

                  var shipping_lines = orderdetail.shipping_lines;
                  $(shipping_lines).each(function(i, shipval){
                    $('.sel-shipping-name').html(shipval.title);
                    $('#shipping-amt').html(shipval.price);
                    
                  });
                  var attribute_field = [];
                  var note_attributes = orderdetail.note_attributes;
                    console.log(note_attributes);
                    $(note_attributes).each(function(i, attr){
                      attribute_field.push(attr.name);
                      var name= attr.name;
                      var value= attr.value;
                      if (name == "Date") {
                        $('.pickup-date').html(value);
                      }
                      if (name == "Reference") {
                        $('.reference').html(value);
                      }
                      if (name == "filepath") {
                        $('#attachment_link').attr("href",value);
                      }
                    });
                    var hasfile = jQuery.inArray("filepath", attribute_field);
                    console.log(hasfile);
                    if(jQuery.inArray("filepath", attribute_field) == -1){
                      $('.fileinput-wrapper').hide();
                    }

                  $(line_items).each(function(i, field){
                    var productid = field.id;
                    var properties = field.properties;
                    console.log(properties);
                    var properties_all= '';
                    $(properties).each(function(i, label){
                      console.log(label);
                      var properties_individual ='<span class="property"><span class="name">'+label.name+' :</span><span class="value">'+label.value+'</span></span>'
                      properties_all += properties_individual;
                    });
                    var title = field.name;
                    var image = field.title;
                    var price =field.price;
                    var discount_amt = field.total_discount;
                    var original_price = field.price;
                    var qty= field.quantity;
                    var itemtotal = parseFloat(original_price)*parseFloat(qty);
                    var sku = field.sku;                   

                    var productrow = '<tr class="indiv-row-tr" data-attr-id="'+productid+'"><td class="next-table__image-cell hide-when-printing" width="1"><div class="aspect-ratio aspect-ratio--square aspect-ratio--square--50">\
                      <img title="" class="productimage block aspect-ratio__content" src="'+image+'"></div>\
                  </td>\
                  <td class="next-table__cell--item-name" width="45%">\
                    <a href="" class="handle">'+title+'</a>\
                    <p class="type--subdued">SKU: <span class="sku price">'+sku+'</span></p>\
                    <p class="properties">'+properties_all+'</p>\
                  </td>\
                  <td class="type--left next-table__cell--grow-when-condensed" \
                  width="1">               \
                    <div class="ui-popover__container">\
                        <span class="price unit-price" data-custom-price="" data-unit-price="'+price+'" data-unit-discount="'+discount_amt+'" data-attr-org-price="'+original_price+'">'+original_price+'</span>\
                        \
                    </div>\
                    <div class="item-custom-pricing">\
                    </div>\
                  </td>\
                  <td class="type--subdued">✖</td>\
                  <td style="min-width: 75px; width: 75px;">\
                    <div class="next-input-wrapper" data-unit-price="'+price+'" data-custom-price=""><input min="1" max="1000000" class="next-input next-input--number qty-input next_input_wrapper" size="30" type="number" value="'+qty+'" name="" disabled>\
                    </div>\
                  </td>\
                  <td class="type--right individual-total" class="total_sub_right">'+itemtotal+'</td>\
                  </tr> ';
                  $('#line_item_rows').append(productrow);
                  });

                  
                  console.log(email);
                  $('li.step-tab').each(function(){
                    $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
                  });
                  $('.step-tab-2').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
                  //2. respective content area
                  $('.step-panel').each(function(){
                    $(this).hide();
                  });
                  $('.step-panel-2').show();
                    
                },
                error: function(data)
                {
                    console.log(data);
                }
            });
                   
          });


          // selecting order show

          $('.step-tab-1').on('click',function(){             
            $('li.step-tab').each(function(){
              $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
            });
            $('.step-tab-1').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
            $('.step-panel-2').hide();
            $('.step-panel-1').show();
            $('#line_item_rows').empty();
          });


          $('.processpayment').on('click',function(){
            var orderid =$('.processpayment').attr("orderid");
            var paymentdata = $('.processpayment').attr('paymentdata');  
            var comment = $('.order_note').val();
            var paymentmethod = $('#paymentmethod').val();
            var data ={"orderid":orderid,"paymentdata":paymentdata,"comment":comment,"paymentmethod":paymentmethod};
            console.log(data);
            $.ajax({
                url: '/updatepayment',
                dataType: 'json',
                type: 'post',
                data: data,
                beforeSend: function() {
                    // setting a timeout
                    // $('.response').html("Uploading ......");
                },
                success: function(res)
                {
                  console.log(res);
                  $('.payment-status').html(res);

                },
                error: function(data)
                {
                    console.log(data);
                }
            });

          });

        });

    </script>

@endsection