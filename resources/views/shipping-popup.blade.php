<div class="custom-modal-wrap custom-shipping-wrap-popup">
  <div class="size-variant-product-row"></div>
<div>
  <div class="Polaris-Modal-Dialog__Container undefined" data-polaris-layer="true" data-polaris-overlay="true">
    <div>
      <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header28" tabindex="-1">
        <div class="Polaris-Modal-Header">
          <div id="modal-header28" class="Polaris-Modal-Header__Title">
            <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Edit shipping address</h2>
          </div><button class="Polaris-Modal-CloseButton" aria-label="Close"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
              </svg></span></button>
        </div>
        <div class="Polaris-Modal__BodyWrapper">
          <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
            <section class="Polaris-Modal-Section">
              <div class="Polaris-Stack Polaris-Stack--vertical">
                <div class="Polaris-Stack__Item">
                  <div class="Polaris-TextContainer">
                    <div class="shipping-popup-content active">
                      <form id="shipping_address_form" >
                      <div class="shipping_field_row">
                        <div class="shipping_edit_field">
                        <label>First name</label>
                            <input name="customer[firstName]" id="PolarisTextField4" class="glbr9" aria-labelledby="PolarisTextField4Label" aria-invalid="false" aria-multiline="false" value="">
                          </div>
                          <div class="shipping_edit_field">
                            <label>Last name</label>
                            <input name="customer[lastName]" id="PolarisTextField5" class="glbr9" aria-labelledby="PolarisTextField5Label" aria-invalid="false" aria-multiline="false" value="">
                          </div>
                        </div>
                        <div class="shipping_field_row">
                          <div class="shipping_edit_field">
                            <label>Company</label>
                            <input name="customer[company]" id="PolarisTextField6" class="glbr9" aria-labelledby="PolarisTextField6Label" aria-invalid="false" aria-multiline="false" value="">
                          </div>
                          <div class="shipping_edit_field">
                            <label>Address</label>
                            <input name="customer[address1]" id="PolarisTextField7" class="glbr9" aria-labelledby="PolarisTextField7Label" aria-invalid="false" aria-multiline="false" value="" tabindex="0" aria-controls="Polarispopover20" aria-owns="Polarispopover20" aria-expanded="false">
                          </div>
                        </div>
                        <div class="shipping_field_row">
                          <div class="shipping_edit_field">
                            <label>Apartment, suite, etc.</label>
                            <input name="customer[address2]" id="PolarisTextField8" class="glbr9" aria-labelledby="PolarisTextField8Label" aria-invalid="false" aria-multiline="false" value="">
                          </div>
                          <div class="shipping_edit_field">
                            <label>City</label>
                            <input name="customer[city]" id="PolarisTextField9" class="glbr9" aria-labelledby="PolarisTextField9Label" aria-invalid="false" aria-multiline="false" value="">
                          </div>
                        </div>
                        <div class="shipping_field_row">
                          <div class="shipping_edit_field one-third">
                            <label>Country/Region</label>
                            <select id="PolarisSelect1" class= "country_name" name="customer[country]" class="_3uXkj" aria-invalid="false">
                              <option value="" disabled="">Country/Region</option>
                              <option value="US">United States</option>
                            </select>
                          </div>
                          <div class="shipping_edit_field one-third">
                              <label>State</label>
                            <select id="PolarisSelect2" name="province" class="state_choice" aria-invalid="false">
                              <option value="" disabled="">State</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AS">American Samoa</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">Washington DC</option><option value="FM">Micronesia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="GU">Guam</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MH">Marshall Islands</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="MP">Northern Mariana Islands</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PW">Palau</option><option value="PA">Pennsylvania</option><option value="PR">Puerto Rico</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VI">U.S. Virgin Islands</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option><option value="AA">Armed Forces Americas</option><option value="AE">Armed Forces Europe</option><option value="AP">Armed Forces Pacific</option>
                            </select>
                            </div>
                        </div>
                        <div class="shipping_field_row">
                          <div class="shipping_edit_field one-third">
                            <label>Zip code</label>
                            <input name="customer[zip]" id="PolarisTextField10" class="glbr9" aria-labelledby="PolarisTextField10Label" aria-invalid="false" aria-multiline="false" value="">
                          </div>
                          <div class="shipping_edit_field">
                            <label>Phone</label>
                            <input name="customer[phone]" id="PolarisTextField11" placeholder="" class="glbr9" aria-labelledby="PolarisTextField11Label" aria-invalid="false" aria-multiline="false" value="">
                          </div>
                        </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
        <div class="Polaris-Modal-Footer">
          <div class="Polaris-Modal-Footer__FooterContent">
            <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
              <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
              <div class="Polaris-Stack__Item">
                <div class="Polaris-ButtonGroup">
                  
                  <div class="Polaris-ButtonGroup__Item">
                    <button type="button" class="Polaris-Button Polaris-Button--seconday cancel-on-config"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Cancel</span></span></button>
                  </div>
                
                  <div class="Polaris-ButtonGroup__Item">
                    <button type="button" class="Polaris-Button Polaris-Button--primary save-shipping"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Save</span></span></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<div class="Polaris-Backdrop"></div>
</div>
<style>
  .custom-shipping-wrap-popup{
    display:none;
  }
</style>