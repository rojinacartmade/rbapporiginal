<div class="custom-modal-wrap custom-modal-wrap-popup">
  <div class="size-variant-product-row"></div>
<div>
  <div class="Polaris-Modal-Dialog__Container undefined" data-polaris-layer="true" data-polaris-overlay="true">
    <div>
      <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header28" tabindex="-1">
        <div class="Polaris-Modal-Header">
          <div id="modal-header28" class="Polaris-Modal-Header__Title">
            <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Configure Product</h2>
            <p><span class="qty_variant">No</span></p>
          </div><button class="Polaris-Modal-CloseButton" aria-label="Close"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
              </svg></span></button>
        </div>
        <div class="Polaris-Modal__BodyWrapper">
          <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
            <section class="Polaris-Modal-Section">
              <div class="Polaris-Stack Polaris-Stack--vertical">
                <div class="Polaris-Stack__Item">
                  <div class="Polaris-TextContainer">
                    <h3>Custom Options</h3>
                      <form class="size-selection">
                      <div class="heightsizewrapper"></div>
                      </form>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
        <div class="Polaris-Modal-Footer">
          <div class="Polaris-Modal-Footer__FooterContent">
            <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
              <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
              <div class="Polaris-Stack__Item">
                <div class="Polaris-ButtonGroup">
                  <div class="Polaris-ButtonGroup__Item">
                    <button type="button" class="Polaris-Button Polaris-Button--seconday cancel-on-config"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Cancel</span></span></button>
                  </div>
                  <div class="Polaris-ButtonGroup__Item">
                    <button type="button" class="Polaris-Button Polaris-Button--seconday custom-popup-save-add-more"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Save & add another</span></span></button>
                  </div>
                  <div class="Polaris-ButtonGroup__Item">
                    <button type="button" class="Polaris-Button Polaris-Button--seconday custom-popup-save-more-size"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Save and Add new variant</span></span></button>
                  </div>           
                  
                  <div class="Polaris-ButtonGroup__Item">
                    <button type="button" class="Polaris-Button Polaris-Button--primary custom-popup-add-product-to-order"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add to order</span></span></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="Polaris-Backdrop"></div>
</div>
<style>
  .custom-modal-wrap-popup, .qty_variant{
    display:none;
  }

</style>
<script type="text/javascript">

</script>