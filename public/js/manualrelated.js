$(document).ready(function(){

  // var productid = $(this).parent().attr('data-id');
  //     var ullength =  $(this).find('ul#checkli li').length;
  //     console.log(ullength);
  //     if ( ullength > 1 ) {       
  //       $(this).parent().parent().find('.count-of-related').text("("+ullength+")");       
  //     }


    var drag = '<span class="Polaris-Icon drag"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path></svg></span>';
     var cross = '<span class="Polaris-Icon cross"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span>';
    $( ".sortable" ).sortable({connectWith: ".con"}).disableSelection();
    $('button.addrelatedinside').on('click',function(){
        console.log("clicked");
        var clickeditem = $(this);
        var productid = $(this).parent().parent().attr('data-id');
        ShopifyApp.flashNotice('Loading products');
        var productPickerOptions = {
                  'selectMultiple': true,
                };
        ShopifyApp.Modal.productPicker(productPickerOptions, function(success, data) {
          // Callback is triggered any time a button
          // is clicked or the window is closed.
          if(!success) {
            // Success is true when a resource is picked successfully
            return;
          }
          if (data.products.length > 0) {
            clickeditem.parent().parent().parent().find('.related-count-text span').text(data.products.length);
            var selectedProducts = data.products;
            var selectedresult = '';
           var currentproductid = clickeditem.parent().parent().attr('data-id');
           var currentsel = [currentproductid];
           //get already saved values and check for duplicate
           clickeditem.parent().parent().find('.selected-related > ul.con > li').each(function(){
                 currentsel.push($(this).attr('data-product-id'));
           });
            selectedProducts.forEach(function(item){
                if(Object.values(currentsel).indexOf(item.id.toString()) == -1){//not in object
                    selectedresult += '<li class="ui-state-default" data-product-id="'+item.id+'" data-product-handle="'+item.handle+'" data-product-name="'+item.title+'">'+drag+' '+item.title +' '+ cross+'</li>';
                }
            });
            clickeditem.parent().parent().find('div.selected-related ul.con').append(selectedresult);
          }
          if (data.errors) {
            console.error(data.errors);
          }
        });
    });

      $('button.addrelatedinsidematching').on('click',function(){
        console.log("clicked");
        var clickeditem = $(this);
        var productid = $(this).parent().parent().attr('data-id');
        ShopifyApp.flashNotice('Loading products');
        var productPickerOptions = {
                  'selectMultiple': true,
                };
        ShopifyApp.Modal.productPicker(productPickerOptions, function(success, data) {
          // Callback is triggered any time a button
          // is clicked or the window is closed.
          if(!success) {
            // Success is true when a resource is picked successfully
            return;
          }
          if (data.products.length > 0) {
            clickeditem.parent().parent().parent().find('.related-count-text span').text(data.products.length);
            var selectedProducts = data.products;
            var selectedresult = '';
           var currentproductid = clickeditem.parent().parent().attr('data-id');
           var currentsel = [currentproductid];
           //get already saved values and check for duplicate
           clickeditem.parent().parent().find('.selected-matching > ul.con > li').each(function(){
                 currentsel.push($(this).attr('data-product-id'));
           });
            selectedProducts.forEach(function(item){
                if(Object.values(currentsel).indexOf(item.id.toString()) == -1){//not in object
                    selectedresult += '<li class="ui-state-default" data-product-id="'+item.id+'" data-product-handle="'+item.handle+'" data-product-name="'+item.title+'">'+drag+' '+item.title +' '+ cross+'</li>';
                }
            });
            clickeditem.parent().parent().find('div.selected-matching ul.con').append(selectedresult);
          }
          if (data.errors) {
            console.error(data.errors);
          }
        });
    });

//     $('.selected-related').click(function(){
//       var productid = $(this).parent().attr('data-id');
//       var ullength =  $(this).find('ul#checkli li').length;
//       console.log(ullength);
//       if ( ullength > 1 ) {       
//         $(this).parent().parent().find('.count-of-related span').text("("+ullength+")");       
//       }
// });
});
$(document).on('click','.selected-related .cross',function(){

	$(this).parent().remove();
});
$( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();