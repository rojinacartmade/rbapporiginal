$(document).ready(function(){
    // enable distable btn

    if (enable_disable_status=='enabled') {
        $('#enable_disable_btn').text('Disable');
        $('#add-order-status').text('enabled');
    }else if(enable_disable_status=='disabled'){
        $('#enable_disable_btn').addClass("Polaris-Button--primary"); 
        $('#enable_disable_btn').text('Enable');
        $('#add-order-status').text('disabled');

    }



// offer order backgroud color
    $('#offer_order_bgcolor').on('move.spectrum',function(){
            var newbgcolor =  $('#offer_order_bgcolor').val();
            $('.preview-box').css("background-color", newbgcolor);
        }).mouseout(function(){
        $('.preview-box').css("background-color", newbgcolor);
 });


$("#offer_order_bgcolor").spectrum({
        preferredFormat: "rgb",
        color: bgcolor,
        showAlpha: true,showInput: true,        
        move: function(color){
                // jQuery('#s_text_border_onhover').css('background-color',color.toRgbString());  
                $('#offer_order_bgcolor').val(color.toRgbString());
                //instant apply on preview
            }
        });


// offer order button background color
var newbtnbgcolor;
    $('#offer_order_btnbgcolor').on('move.spectrum',function(){
             var newbtnbgcolor =  $('#offer_order_btnbgcolor').val();
            $('#previewbutton').css("background", newbtnbgcolor);
        }).mouseout(function(){
        $('#previewbutton').css("background", newbtnbgcolor);
 });
$("#offer_order_btnbgcolor").spectrum({
        preferredFormat: "rgb",
        color: btnbgcolor,
        showAlpha: true,showInput: true,        
        move: function(color){
                // jQuery('#s_text_border_onhover').css('background-color',color.toRgbString());  
                $('#offer_order_btnbgcolor').val(color.toRgbString());
                //instant apply on preview
            }
        });

//offer order button text color
    $('#offer_order_btncolor').on('move.spectrum',function(){
            var newbtncolor =  $('#offer_order_btncolor').val();
            $('#previewbutton').css("color", newbtncolor);
        }).mouseout(function(){
        $('#previewbutton').css("color", newbtncolor);
 });
$("#offer_order_btncolor").spectrum({
        preferredFormat: "rgb",
        color: btncolor,
        showAlpha: true,showInput: true,        
        move: function(color){
                // jQuery('#s_text_border_onhover').css('background-color',color.toRgbString());  
                $('#offer_order_btncolor').val(color.toRgbString());
                //instant apply on preview
            }
        });



// offer order text color
    $('#offer_order_txtcolor').on('move.spectrum',function(){
            var newtxtcolor =  $('#offer_order_txtcolor').val();
            $('.preview-box').css("color", newtxtcolor);
            // $('#previewbutton').css("color", newtxtcolor);
        }).mouseout(function(){
        $('.preview-box').css("color", newtxtcolor);
         // $('#previewbutton').css("color", newtxtcolor);
 });
$("#offer_order_txtcolor").spectrum({
        preferredFormat: "rgb",
        color: txtcolor,
        showAlpha: true,showInput: true,        
        move: function(color){
                // jQuery('#s_text_border_onhover').css('background-color',color.toRgbString());  
                $('#offer_order_txtcolor').val(color.toRgbString());
                //instant apply on preview
            }
        });


// Choose Button bg color on hover
         $('#offer_order_btnbgcolor').on('move.spectrum',function(){
             var newbtnbgcolor =  $('#offer_order_btnbgcolor').val();
                $('#previewbutton').hover(function(){
                $('#previewbutton').css("background",btnhovercolor);
             },function(){                
                $('#previewbutton').css("background", newbtnbgcolor);
  }); 
         });
    if(newbtnbgcolor == undefined || newbtnbgcolor== null ){
             $('#previewbutton').hover(function(){
                $('#previewbutton').css("background",btnhovercolor);
             },function(){                
                $('#previewbutton').css("background", btnbgcolor);
  }); 
         } 
            
    

    
  //               $('#previewbutton').hover(function(){
  //                 var newbtnbgcolor=$('#offer_order_btnbgcolor').val();
  //               $('#previewbutton').css("background",btnhovercolor);
  //            },function(){                
  //               $('#previewbutton').css("background", newbtnbgcolor);
  // });
  // }
$('#offer_order_hover_btn_bg_color').on('move.spectrum',function(){
            var newbtnhoverbgcolor =  $('#offer_order_hover_btn_bg_color').val();
            var newbtnbgcolor=$('#offer_order_btnbgcolor').val();
             $('#previewbutton').hover(function(){
                $('#previewbutton').css("background",newbtnhoverbgcolor);
             },function(){                
                $('#previewbutton').css("background", newbtnbgcolor);
                
  });
 });
 
$("#offer_order_hover_btn_bg_color").spectrum({
        preferredFormat: "rgb",
        color: btnhovercolor,
        showAlpha: true,showInput: true,        
        move: function(color){
                // jQuery('#s_text_border_onhover').css('background-color',color.toRgbString());  
                $('#offer_order_hover_btn_bg_color').val(color.toRgbString());
                //instant apply on preview
            }
        });
 
        

 // $('#offer_order_hover_btn_bg_color').on('move.spectrum',function(){
 //            var newbghoverbtncolor =  $('#offer_order_hover_btn_bg_color').val();
            
 //            $('#previewbutton').hover(function(){$(this).css("background", newbghoverbtncolor);
 //        }).mouseout(function(){
       
 //         $('#previewbutton').hover(function(){
 //            $(this).css("background", btncolor);
 //         });
 //        });
 //    });



// choose btn text color on hover
// $("#offer_order_hov_btn_txt_color").spectrum({
//         preferredFormat: "rgb",
//         color: "#401",
//         showAlpha: true,showInput: true,        
//         move: function(color){
//                 // jQuery('#s_text_border_onhover').css('background-color',color.toRgbString());  
//                 $('#offer_order_hov_btn_txt_color').val(color.toRgbString());
//                 //instant apply on preview
//             }
//         });



// choose heading color

//choose the heading color
$('#offer_order_head_color').on('move.spectrum',function(){
   var orderheadingcolor =  $('#offer_order_head_color').val();
    console.log(orderheadingcolor);
    $('.orderstatusheading').css("color",orderheadingcolor);
}).mouseout(function(){
    var orderheadingcolor =  $('#offer_order_head_color').val();
        $('.orderstatusheading').css("color",orderheadingcolor);
    });

$("#offer_order_head_color").spectrum({
        preferredFormat: "rgb",
        color:heading_color ,
        showAlpha: true,showInput: true,        
        move: function(color){
                // jQuery('#s_text_border_onhover').css('background-color',color.toRgbString());  
                $('#offer_order_head_color').val(color.toRgbString());
                //instant apply on preview
            }
        });

// config and style setting tabs
$('li.style-settings,li.config-settings').click(function(){
    $('.settings-tab').find('button').removeClass('Polaris-Tabs__Tab--selected');
    $(this).find('button').addClass('Polaris-Tabs__Tab--selected');
    //content toggle
    if($(this).hasClass('style-settings')){
        $('.tab-content').hide();
           $('.style-settings-content').show();
    }
    if($(this).hasClass('config-settings')){
        $('.tab-content').hide();
           $('.config-settings-content').show();
    }
});

    // sell after checkout shape instant view
        $('#offer_content_align').on('change', function() {
            var offer_coneten_align = $('#offer_content_align').val();
            console.log(offer_coneten_align);
            $('.preview-box').css("text-align",offer_coneten_align); 
        });
         $(".tab-content input[type='radio']").click(function(){
            var offer_coneten_align = $("input[name='offer_shape']:checked").val();
            console.log(offer_coneten_align);
            if(offer_coneten_align=="preview_shape_rounded"){
                $('.preview-outer-box').addClass("preview_shape_rounded");
                $('.preview-outer-box').removeClass("preview_shape_rectangle");
                return;
            }
            if(offer_coneten_align=="preview_shape_rectangle"){
                $('.preview-outer-box').addClass("preview_shape_rectangle");
                $('.preview-outer-box').removeClass("preview_shape_rounded");
                return;
            } 
        });
         $('.Polaris-Banner__Dismiss').click(function(){
        $('.installation-info-top').hide();
        });
         $('#enable_disable_btn').removeClass("Polaris-Button--primary");
});
