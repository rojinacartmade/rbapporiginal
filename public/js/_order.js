$(".create-final-order").addClass("btn-disabled");
function updatesubtotalandtotal(){
  var lineitemstotal = 0;
  $('.individual-total').each(function(){
    lineitemstotal += parseFloat($(this).text());
  });
  //check discount
  var total_line_discount_percent = $('.add-discount-cta').attr('data-discount-percent');
  var total_line_discount = 0;
  if(total_line_discount_percent){
    total_line_discount = lineitemstotal*(parseFloat(total_line_discount_percent)/100);
    total_line_discount = total_line_discount.toFixed(2);
    $('#discount-amt').text('-'+total_line_discount);
    $('#discount-amt').attr('data-discount-amt',total_line_discount);
  }
    var subtotal_after_discount = lineitemstotal - total_line_discount;
    //update subtotal
    $('.sub-total').text(subtotal_after_discount.toFixed(2));

  //check shipping
  var custom_shipping_cost = parseFloat($('.add-shipping-cta').attr('data-custom-shipping-amt'));
  if(isNaN(custom_shipping_cost)){
    custom_shipping_cost = 0;
  }
  if(custom_shipping_cost){
    $('#shipping-amt').text(custom_shipping_cost.toFixed(2));
  }
  //add shipping cost
  var total = subtotal_after_discount + custom_shipping_cost;

  console.log('subtotal_after_discount '+ subtotal_after_discount);
  console.log('custom_shipping_cost '+custom_shipping_cost);

  $('.grand-total').text(total.toFixed(2));
}

function getproductrowhtml(id,image,title,sku,price,original_price,discount_amt,qty=1,properties=''){
  var itemtotal = (price*qty).toFixed(2);
  var productrow = '<tr class="indiv-row-tr" data-attr-id="'+id+'"><td class="next-table__image-cell hide-when-printing" width="1"><div class="aspect-ratio aspect-ratio--square aspect-ratio--square--50">\
        <img title="" class="productimage block aspect-ratio__content" src="'+image+'"></div>\
    </td>\
    <td class="next-table__cell--item-name" width="45%">\
      <a href="" class="handle">'+title+'</a>\
      <p class="type--subdued">SKU: <span class="sku price">'+sku+'</span></p>\
      <p class="properties">'+properties+'</p>\
    </td>\
    <td class="type--left next-table__cell--grow-when-condensed" \
    width="1">               \
      <div class="ui-popover__container">\
          <span class="price unit-price" data-custom-price="" data-unit-price="'+price+'" data-unit-discount="'+discount_amt+'" data-attr-org-price="'+original_price+'">'+price+'</span>\
          \
      </div>\
      <div class="item-custom-pricing">\
        <span><input type="checkbox" class="item-to-custom-price" name="item-custom-price"><label>Custom price*</label></span>\
        <span class="input-custom-price"><input type="text" name="input-custom-price" class="input-custom-price-box"></span>\
      </div>\
    </td>\
    <td class="type--subdued">✖</td>\
    <td style="min-width: 75px; width: 75px;">\
      <div class="next-input-wrapper" data-unit-price="'+price+'" data-custom-price=""><input min="1" max="1000000" class="next-input next-input--number qty-input next_input_wrapper" size="30" type="number" value="'+qty+'" name="">\
      </div>\
    </td>\
    <td class="type--right individual-total" class="total_sub_right">'+itemtotal+'</td>\
    <td class="next-table__cell--top-right-when-condensed type--right line-item-remove" title="remove item">&#10006;</td></tr> ';
  return productrow;
}

//delete product item from order
$(document).on('click','.remove-line-item',function(){
  $(this).parent().remove();
  updatesubtotalandtotal();
});

//increase quantity by manually typing
$(document).on('keyup','.qty-input',function(){
  custompriceupdated();
  // var new_qty = $(this).val();
  // var unit_price = $(this).parent().attr('data-custom-price')?$(this).parent().attr('data-custom-price'):$(this).parent().attr('data-custom-price');
  // var updated_price = (parseFloat(new_qty)*parseFloat(unit_price)).toFixed(2);
  // // update_price = update_price.toFixed(2);
  // $(this).parent().parent().parent().find('.individual-total').html(updated_price);
  // //update the cart subtotal
  // updatesubtotalandtotal();
});       

$(document).ready(function(){

  

  //Step 1 : Customer selection
  $('table.sel-customer-table tr').on('click',function(){ 
    var customer_detail = $(this).attr('data-sel-customerid');
    var customerdetail = JSON.parse(customer_detail);
    var sel_customerid = customerdetail.id;
    $('.create-final-order').attr('data-customer-id',sel_customerid);
    var email = customerdetail.email;
    var first_name = customerdetail.firstName;
    var last_name = customerdetail.lastName;
   var addresses = customerdetail.addresses[0];
    var company = addresses.company;
    var address1 = addresses.address1;
    var address2 = addresses.address2;
    var phone = customerdetail.phone;
    var city = addresses.city;
    var province_code = addresses.provinceCode;
    var zip = addresses.zip;
    var country_name = addresses.country;
    $('.order-creation-wrapper').attr('data-sel-id',customer_detail);
    $('.rb_customer_id').val(sel_customerid);
    //go to second step
    //1. tab selection
    $('li.step-tab').each(function(){
      $(this).find('.Polaris-Tabs__Tab').removeClass('Polaris-Tabs__Tab--selected');
    });
    $('.step-tab-2').find('.Polaris-Tabs__Tab').addClass('Polaris-Tabs__Tab--selected');
    //2. respective content area
    $('.step-panel').each(function(){
      $(this).hide();
    });
    $('.step-panel-2').show();
    $("#first_name , #sfirstname").text(first_name);
    $("#last_name , #slastname").text(last_name);
    $("#email").html(email);
    $("#company").html(company);
    $("#address1").html(address1);
    $("#address2").html(address2);
    $("#city").html(city);
    $("#province_code").html(province_code);
    $("#zip").html(zip);
    $("#country_name").html(country_name);  

    // set value in shipping form
    $('input[name="customer[firstName]"]').val(first_name);
    $('input[name="customer[lastName]"]').val(last_name);
    $('input[name="customer[company]"]').val(company);
    $('input[name="customer[address1]"]').val(address1);
    $('input[name="customer[address2]"]').val(address2);
    $('input[name="customer[city]"]').val(city);
    $('.state_choice').val(province_code);
    $('input[name="customer[zip]"]').val(zip);
    $('input[name="customer[phone]"]').val(phone);


  });
});

function getvariantsselection(individual_product){
  console.log('getvariantsselection');
}


function checkaddedalready(){


}

// select height vs size function

function heightvssize(){
  var selectvalue =$('select[name="Height"]').val();
  switch(selectvalue) {
    case (selectvalue == "5'0\" - 5'2"):
      $('select[name="Size"]').val("3-42 ");
      break;
  
  }
}
// $('select[name="Height"]').on('change', function(){  
$(document).on('change','select[name="Height"]',function(){
  var selectvalue =$('select[name="Height"]').val(); 
  switch(selectvalue) {
    case "5'0\"- 5'2" :
      $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
          case "3-45" :
          $('select[name="Size"]').val("3-45");        
          break;
          case "9-45" :
          $('select[name="Size"]').val("9-45");        
          break;
          case "6-45" :
          $('select[name="Size"]').val("6-45");        
          break;
        }
      });    
    break;
    case "3'6\"- 3'8" :
     $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "27" :
        $('select[name="Size"]').val("27");      
        break;
        }
      });
      break;
    case "3'9\"- 3'11" :
     $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "30" :
        $('select[name="Size"]').val("30");     
        break;
        }
      });
      break;
   case "4'0\"- 4'2" :
    $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "33" :
        $('select[name="Size"]').val("33");    
        break;
        }
      });
      break;
    case "4'3\"- 4'5" :
     $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "36" :
        $('select[name="Size"]').val("36");   
        break;
        }
      });
      break;
    case "4'6\"- 4'8" :
     $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "39" :
        $('select[name="Size"]').val("39"); 
        break;
        }
      });
      break;
   case "4'9\"- 4'11" :
        $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "3-42" :
        $('select[name="Size"]').val("3-42");
        break;
        }
      });
      break;
    case "5'3\"- 5'5" :
        $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "3-48" :    
        $('select[name="Size"]').val("3-48");
        break;
        case "6-48" :    
        $('select[name="Size"]').val("6-48");
        break;
        case "9-48" :    
        $('select[name="Size"]').val("9-48");
        break;
        case "12-48" :    
        $('select[name="Size"]').val("12-48");
        break;
        }
      });
      break;
    case "5'6\"- 5'8":
        $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "3-51" :    
        $('select[name="Size"]').val("3-51");
        break;
        case "6-51" :    
        $('select[name="Size"]').val("6-51");
        break;
        case "9-51" :    
        $('select[name="Size"]').val("9-51");
        break;
        case "12-51" :    
        $('select[name="Size"]').val("12-51");
        break;
        }
      });
      break;
   case "5'9\"- 5'11" :
        $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "5-54" :    
        $('select[name="Size"]').val("5-54");
        break;
        case "6-54" :    
        $('select[name="Size"]').val("6-54");
        break;
        case "9-54" :    
        $('select[name="Size"]').val("9-54");
        break;
        case "12-54" :    
        $('select[name="Size"]').val("12-54");
        break;
        }
      });
      break;

    case "6'0\"- 6'2" :
        $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "5-57" :    
        $('select[name="Size"]').val("5-57");
        break;
        case "9-57" :    
        $('select[name="Size"]').val("9-57");
        break;
        case "12-57" :    
        $('select[name="Size"]').val("12-57");
        break;
        }
      });
      break;
    case "6'3\"- 6'5" :
        $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "5-60" :
        $('select[name="Size"]').val("5-60");
        break;
        case "9-60" :    
        $('select[name="Size"]').val("9-60");
        break;
        case "12-60" :    
        $('select[name="Size"]').val("12-60");
        break;
        }
      });
      break;
   case "6'6\"- 6'8" :
        $('select[name="Size"] option').each(function(){
        var sizeoption = this.value;
        switch(sizeoption) {
        case "5-63" :
        $('select[name="Size"]').val("5-63");
        break;
        case "9-63" :    
        $('select[name="Size"]').val("9-63");
        break;
        case "12-63" :    
        $('select[name="Size"]').val("12-63");
        break;
        }
      });
      break;
    case "6'9\"- 6'11" :
      $('select[name="Size"]').val("6-66");
      break;
  
  }
  
});


// select size and change height value
$(document).on('change','select[name="Size"]',function(){
  var selectvalue =$('select[name="Size"]').val(); 
  switch(selectvalue) {
    case "27" :
      $('select[name="Height"]').val("3'6\"- 3'8");
      break;
      case "30" :
      $('select[name="Height"]').val("3'9\"- 3'11");
      break;
    case "33" :
      $('select[name="Height"]').val("4'0\"- 4'2");
      break;
      case "36" :
      $('select[name="Height"]').val("4'3\"- 4'5");
      break;
    case "39" :
      $('select[name="Height"]').val("4'6\"- 4'8");
      break;
    case "3-42" :
      $('select[name="Height"]').val("4'9\"- 4'11");
      break;
    case "3-45" :
      $('select[name="Height"]').val("5'0\"- 5'2");
      break;
      case "3-48" :
      $('select[name="Height"]').val("5'3\"- 5'5");
      break;
    case "3-51" :
      $('select[name="Height"]').val("5'6\"- 5'8");
      break;
      case "5-54" :
      $('select[name="Height"]').val("5'9\"- 5'11");
      break;
    case "5-57" :
      $('select[name="Height"]').val("6'0\"- 6'2");
      break;
    case "5-60" :
      $('select[name="Height"]').val("6'3\"- 6'5");
      break;
      case "5-63" :
      $('select[name="Height"]').val("6'6\"- 6'8");
      break;
    case "6-66" :
      $('select[name="Height"]').val("6'9\"- 6'11");
      break;
      case "6-45" :
      $('select[name="Height"]').val("5'0\"- 5'2");
      break;
    case "6-48" :
      $('select[name="Height"]').val("5'3\"- 5'5");
      break;
    case "6-51" :
      $('select[name="Height"]').val("5'6\"- 5'8");
      break;
    case "6-54" :
      $('select[name="Height"]').val("5'9\"- 5'11");
      break;
      case "9-45" :
      $('select[name="Height"]').val("5'0\"- 5'2");
      break;
    case "9-48" :
      $('select[name="Height"]').val("5'3\"- 5'5");
      break;
      case "9-51" :
      $('select[name="Height"]').val("5'6\"- 5'8");
      break;
    case "9-54" :
      $('select[name="Height"]').val("5'9\"- 5'11");
      break;
    case "9-57" :
      $('select[name="Height"]').val("6'0\"- 6'2");
      break;
    case "9-60" :
      $('select[name="Height"]').val("6'3\"- 6'5");
      break;
    case "9-63" :
      $('select[name="Height"]').val("6'6\"- 6'8");
      break;
    case "12-45" :
      $('select[name="Height"]').val("5'0\"- 5'2");
      break;
    case "12-48" :
      $('select[name="Height"]').val("5'3\"- 5'5");
      break;
    case "12-51" :
      $('select[name="Height"]').val("5'6\"- 5'8");
      break;
    case "12-54" :
      $('select[name="Height"]').val("5'9\"- 5'11");
      break;
    case "12-57" :
      $('select[name="Height"]').val("6'0\"- 6'2");
      break;
    case "12-60" :
      $('select[name="Height"]').val("6'3\"- 6'5");
      break;
    case "12-63" :
      $('select[name="Height"]').val("6'6\"- 6'8");
      break;                  
  }
  
});

$(document).on('click','button.create-final-order',function(){
  //collect line items
  var lineitems = {};
  var lineitemsarr = new Array();
  var i=0;
  $('.indiv-row-tr').each(function(){
    lineitemsarr[i] = {};
    lineitems['variantid'] = $(this).attr('data-attr-id');
    lineitemsarr[i]['variantid'] = $(this).attr('data-attr-id');
    lineitems['qty'] = $(this).find('.qty-input').val();
    lineitemsarr[i]['qty'] = $(this).find('.qty-input').val();
    var unitpriceele = $(this).find('.unit-price');
    var unitprice = unitpriceele.attr('data-custom-price')?unitpriceele.attr('data-custom-price'):unitpriceele.attr('data-unit-price');
    var original_unitprice = unitpriceele.attr('data-attr-org-price');
    var per_item_discount = original_unitprice-unitprice;
    lineitems['individual_discount_amt'] = parseFloat(per_item_discount); //unitpriceele.attr('data-unit-discount');
    lineitemsarr[i]['price_for_customer'] = parseFloat(unitprice).toFixed(2);
    lineitemsarr[i]['individual_discount_amt'] = parseFloat(per_item_discount).toFixed(2);
    //collect line item properties
    var lineitemspropertiesarr = new Array();
    var k=0;
    var properties = $(this).find('.property');
    if(properties.length){
      properties.each(function(property){
        lineitemspropertiesarr[k] = {};
        var property_name = $(this).find('span.name').text();
        var property_value = $(this).find('span.value').text();
        lineitemspropertiesarr[k]['name'] = property_name.replace(':', '');
        lineitemspropertiesarr[k]['value'] = property_value;
        //console.log($(this).attr('class')+'='+$(this).text());
        k++;
      });
    }
    lineitemsarr[i]['properties']=lineitemspropertiesarr;
    i++;
  });
  var enablebtn = false;
  var lineitemsarr = JSON.stringify(lineitemsarr);

  var additionalinfoarr = {};
  additionalinfoarr['order_note'] = $('.order_note').val();
  additionalinfoarr['date'] = $('.pickup-date').val();

  additionalinfoarr['po'] = $('.po-number-wrapper input').val();
  var hasdate = true;
  var hasref = true;
  if (additionalinfoarr['date'] == "") {
    $('.date-picker-wrapper').find('.Polaris-TextField').addClass("isempty");
    $('.order-creation-status').html("Need By Date must be selected.");
    var enablebtn =true;
    hasdate = false;
    console.log(enablebtn);
  }
  if (additionalinfoarr['po'] == "") {
    $('.po-number-wrapper').find('.Polaris-TextField').addClass("isempty");
    $('.order-creation-status').html("Reference must be filled.");
    var enablebtn = true;
    hasref = false;
    console.log(enablebtn);
  }
  var order_discount = {};
  order_discount['percent'] = $('.add-discount-cta').attr('data-discount-percent');
  order_discount['reason'] = $('.add-discount-cta').attr('data-discount-reason');
  order_discount['amt'] = $('#discount-amt').attr('data-discount-amt');
  var order_shipping = {};
  order_shipping['type'] = $('.add-shipping-cta').attr('data-shipping-type');
  order_shipping['name'] = $('.add-shipping-cta').attr('data-custom-shipping-name');
  order_shipping['amt'] = $('.add-shipping-cta').attr('data-custom-shipping-amt'); 

  // console.log(lineitems);
  //console.log(lineitemsarr);
  //find customerid
  var customer_id = $('.create-final-order').attr('data-customer-id');
  var order_note = $('.order_note').val();
  var additional_info = JSON.stringify(additionalinfoarr);
  var discount_info = JSON.stringify(order_discount);
  var shipping_info = JSON.stringify(order_shipping);
  var shipping_data  = $('.order-creation-wrapper').attr('shipping_data');
  var markaspaid =$("input[name=markaspaid]").prop('checked');
  if (markaspaid) {
    markaspaid= "true";
  }
  console.log(markaspaid);
  var filepath = $('.fileinput-wrapper').attr("filepath");
  //request to create order
  var cartdata = {'cartdata':lineitemsarr,'customer_id':customer_id,'discount_info':discount_info,'shipping_info':shipping_info,'order_note':order_note,'additional_info':additional_info,'shipping_data':shipping_data, 'markaspaid':markaspaid, 'filepath':filepath};
  console.log(cartdata);
  if(order_shipping['type'] == "null"){
    $(".create-final-order").addClass("btn-disabled");
    $('.order-creation-status').html("Shipping Method must be selected."); 
    var enablebtn =true; 
  }
  console.log(enablebtn);
  if(enablebtn){
    $(".create-final-order").addClass("btn-disabled");
    if(order_shipping['type'] == "null"){
        $(".create-final-order").addClass("btn-disabled");
        $('.order-creation-status').html("Shipping Method must be selected.");          
      }
    else if(hasref == false || hasdate == false) {
      $('.order-creation-status').html("Need By Date and Reference must be filled.");
    }else{
      $('.order-creation-status').html("Something went wrong.");
    }
  }
  else{
  jQuery.ajax({
    url: 'createorder',
    dataType: 'json',
    type: 'post',
    data: cartdata,
     beforeSend: function() {
                // setting a timeout
      $('.order-creation-status').html("Creating order....");
    },
    success: function(res){
      console.log(res);
      if(res){
        var message = 'Order <strong>'+res+'</strong> has been created successfully!<br/>Customer has been notified via email.';
        $('.order-creation-status').html(message);
        $('.create-final-order').attr('disabled',true);
        setTimeout(function() { 
           location.reload();
        }, 3000);
      }
    },
    error: function(xhr, status, error){
      //var err = JSON.parse(xhr.responseText);
      console.log("Draft Order Failed");
      console.log(xhr);
      console.log(status);
      console.log(error);
    }
  });
}
});

var totalvariantadded = 1;

function addvariantproduct(){
  var dataArray = $('.custom-modal-wrap-popup').find('form.size-selection').serializeArray();  
  console.log(dataArray);
  var dataObj = {};
  $(dataArray).each(function(i, field){
    dataObj[field.name] = field.value;
  });
  var properties_start ='<span class="properties">';
  var properties_end ='</span>';
  var properties_all = ''; 
  var input_qty = $('.custom-modal-wrap-popup').find('.qty-wrapper input').val()!=''?$('.custom-modal-wrap-popup').find('.qty-wrapper input').val():1;
  var hasrequiredvalue = true;
  $('form.size-selection').find('.custom-option').each(function(){
    var label_index = $(this).find(".label_index").text();
    var label_value = dataObj[''+label_index+''];
    console.log(label_value);
    var has_required = $(this).find('.required').length;
    if (has_required > 0) {
      console.log(has_required);
      if (label_value == "") {
        console.log(label_value);
        $(this).find('.inputvalue').css('border-color','red');
        $(this).find('.selectvalue').css('border-color','red');
        hasrequiredvalue = false;
      }
    }
    if(typeof(label_value) != "undefined"){
    var properties_individual ='<span class="property"><span class="name">'+label_index+' :</span><span class="value">'+label_value+'</span></span>'
    properties_all += properties_individual;
  }
  });
  var properties = properties_start + properties_all+ properties_end;
  var individual_product = $.parseJSON($('.size-variant-product-row').attr('data-attr-product'));
  console.log(properties);
  console.log(jQuery.type(hasrequiredvalue));
  if (hasrequiredvalue) {
    var productrow = getproductrowhtml(individual_product['variant_id'],individual_product['product_image'],individual_product['title'],individual_product['sku'],individual_product['price'],individual_product['original_price'],individual_product['discount_amt'],input_qty,properties);
    $('#line_item_rows').append(productrow);
    closemodal();
  }
  updatesubtotalandtotal();
}
function addnewvariantproduct(){
  var dataArray = $('.custom-modal-wrap-popup').find('form.size-selection').serializeArray();  
  console.log(dataArray);
  var dataObj = {};
  $(dataArray).each(function(i, field){
    dataObj[field.name] = field.value;
  });
  var properties_start ='<span class="properties">';
  var properties_end ='</span>';
  var properties_all = ''; 
  var input_qty = $('.custom-modal-wrap-popup').find('.qty-wrapper input').val()!=''?$('.custom-modal-wrap-popup').find('.qty-wrapper input').val():1;
  var hasrequiredvalue = true;
  $('form.size-selection').find('.custom-option').each(function(){
    var label_index = $(this).find(".label_index").text();
    // var label_value = dataObj[''+label_index+''].replace('*', '"');
    var label_value = dataObj[''+label_index+''];
    // console.log(label_value);
    // var label_value = labelvalue.replace('*', '"');
    console.log(label_value);
    var has_required = $(this).find('.required').length;
    if (has_required > 0) {
      console.log(has_required);
      if (label_value == "") {
        console.log(label_value);
        $(this).find('.inputvalue').css('border-color','red');
        $(this).find('.selectvalue').css('border-color','red');
        hasrequiredvalue = false;
        hasrequiredvalue = false;
      }
    }
    if(typeof(label_value) != "undefined"){
    var properties_individual ='<span class="property"><span class="name">'+label_index+' :</span><span class="value">'+label_value+'</span></span>'
    properties_all += properties_individual;
  }
  });
  var properties = properties_start + properties_all+ properties_end;
  var individual_product = $.parseJSON($('.size-variant-product-row').attr('data-attr-product'));
  console.log(properties);
  console.log(jQuery.type(hasrequiredvalue));
  if (hasrequiredvalue) {
    var productrow = getproductrowhtml(individual_product['variant_id'],individual_product['product_image'],individual_product['title'],individual_product['sku'],individual_product['price'],individual_product['original_price'],individual_product['discount_amt'],input_qty,properties);
    $('#line_item_rows').append(productrow);
    // closemodal();
    // $('.custom-popup .size-selection')[0].reset();
  }
  updatesubtotalandtotal();
  
  setTimeout(function() { 
    $('button.custom-popup-save-more-size').find('.Polaris-Button__Text').html("Add new variant");
  }, 1500);
  
}


$('button.custom-popup-save-add-more').click(function(){
  $.when(addvariantproduct()).then(addproductviashopifymodal());
  $('.custom-popup .size-selection')[0].reset();
});

$(document).on('click','button.custom-popup-save-more-size',function(){
// $('button.custom-popup-save-more-size').click(function(){
  console.log("here");
  $('.qty_variant').addClass("addedvariant");
  setTimeout(function() { 
    $(this).find('.Polaris-Button__Text').html("Adding");     
  }, 1000);
  addnewvariantproduct();
  $('.size-selection').trigger("reset");
  // $('.qty_variant').html(totalvariantadded++);

});


function selectvarainsorqty(individual_product){
  //if the selected product has variant then ask to choose variants
  var selproducttags = individual_product['tags'];
  var selproduct_type = individual_product['product_type'];
  var productid = individual_product['id'];
  console.log(selproducttags);
   //popular,hasize
   // if (selproducttags != null) {
   //  console.log("here null");
   // }
  var tags = selproducttags.split(',');
  var hassize = false;
  var hascustom = false;
  tags.forEach(function(tag){
    if(tag=='hassize'){
      hassize = true;
    }
    if(tag=='custom'){
      hascustom = true;
    }
  });

  if(hascustom){    
    var toupdatedata = JSON.stringify(individual_product);    
    $('.size-variant-product-row').attr('data-attr-product',toupdatedata);
    var productdata = {'productid':productid};
    console.log();
    jQuery.ajax({
      url: '/getmetafield',
      dataType: 'json',
      type: 'post',
      data: productdata,
      success: function(res){    
        console.log(res);
        var selecthtml = '';
        var heightsizewrap = '<div class="heightsizewrap">';
        // $('.custom-modal-wrap-popup').find('.size-selection').append(heightsizewrap);
        $.each( res, function( index, value ){
          var metavalue = value;
          console.log(metavalue);
          console.log($.parseJSON(metavalue));
          metavalue = $.parseJSON(metavalue);
          var label = metavalue.label;
          var type = metavalue.type;
          var isRequired = metavalue.required;
          console.log(jQuery.type(isRequired));
          if (isRequired == true) {
            requiredhtml = '<span class="required">*</span>';
          }else{
            requiredhtml = '';
          }
          

          if (type == 'dropdown') {
          var options = metavalue.options;
          var options_val = options.split(',');
          var optionvalues ='';
          $.each( options_val, function( index, value ){
            var optionlabelvalue = value.replace('"', "&quot;");
            var optionlabelvalue = optionlabelvalue.replace(' ', '');
            option_values = '<option value="'+optionlabelvalue +'" price="0" >'+value +'</option>'
            optionvalues += option_values;
          });
          
          selecthtml = '<div class="custom-option custom-option-'+label +'">\
                        <p><label><span class="label_index" >'+label +'</span>'+requiredhtml+'</label></p>\
                        <div class="variant-height-input-wrap">         \
                          <select id="" class=" variant-closure-input selectvalue" aria-invalid="false" name="'+label +'">\
                            <option value="">-- Please Select --</option>'+optionvalues+'\
                          </select>\
                        </div>\
                      </div>';
          // console.log(selecthtml);
          if(label == "Height" || label=="Size"){
            $('.custom-modal-wrap-popup').find('.heightsizewrapper').append(selecthtml);
            console.log("inside jsbbj");
          }else{
            $('.custom-modal-wrap-popup').find('.size-selection').append(selecthtml);
          }
          
        }
        if (type=="text") {
          var texthtml = '<div class="custom-option custom-option-'+label +'">\
                        <p><label><span class="label_index" >'+label +'</span>'+requiredhtml+'</label></p>\
                        <div class="variant-height-input-wrap">         \
                          <input type="text" class="input-text required-entry inputvalue" value="" name="'+label +'" >\
                        </div>\
                      </div>';
           $('.custom-modal-wrap-popup').find('.size-selection').append(texthtml);            
        }
        if (type=="textarea") {
          var textareahtml = '<div class="custom-option custom-option-'+label +'">\
                        <p><label><span class="label_index" >'+label +'</span>'+requiredhtml+'</label></p>\
                        <div class="variant-height-input-wrap">         \
                          <textarea name="'+label +'" class="inputvalue"></textarea>\
                        </div>\
                      </div>';
           $('.custom-modal-wrap-popup').find('.size-selection').append(textareahtml);            
        }
        });
        var qtyhtml ='<div class=" custom-option custom-option-qty">\
                        <p class="custom-option"><label><span class="" >Quantity</span></label></p>\
                        <p class="qty-wrapper">\
                          <input type="number"  min="1" class="quantity-input" name="Quantity">\
                        </p>\
                      </div>';
        $('.custom-modal-wrap-popup').find('.size-selection').append(qtyhtml);
        $('.custom-modal-wrap-popup').show();

      },
      error: function(xhr, status, error){
        //var err = JSON.parse(xhr.responseText);
        console.log("Failed");
        console.log(xhr);
        console.log(status);
        console.log(error);
      }
    });
    console.log(selproduct_type);
    
  }else{
    //if the product is already added in the cart
    console.log("not has tag custom");
    var foundinalreadyadded = false;
    $('.indiv-row-tr').each(function(){
      var addeditemid = $(this).attr('data-attr-id');
      if(addeditemid==individual_product['id']){
        //only update the product row
        foundinalreadyadded = true;
        var foundele = $(this);
        //find the quantity of already added
        var newqty = 1+parseInt(foundele.find('.qty-input').val());
        var unitprice = individual_product['price'];
        var newindividualsubtotal = newqty*unitprice;
        foundele.find('.qty-input').val(newqty);
        foundele.find('.individual-total').text(newindividualsubtotal.toFixed(2));
      }
    });

    if(!foundinalreadyadded){
      //append the product selected
      var productrow = getproductrowhtml(individual_product['variant_id'],individual_product['product_image'],individual_product['title'],individual_product['sku'],individual_product['price'],individual_product['original_price'],individual_product['discount_amt']);
      $('#line_item_rows').append(productrow);
    }
  }
}

function addproductviashopifymodal(){
  ShopifyApp.flashNotice('Loading products');
  var productPickerOptions = {
    'selectMultiple': false,
  };
  ShopifyApp.Modal.productPicker(productPickerOptions, function(success, data) {
    // Callback is triggered any time a button
    // is clicked or the window is closed.
    if(!success) {
      // Success is true when a resource is picked successfully
      return;
    }
    if(success) {
      //if the selected product has variant then ask to choose variants
      var selproducttags = data.products[0].tags; //popular,hasize
      var tags = selproducttags.split(',');
      var hassize = false;
      tags.forEach(function(tag){
        if(tag=='hassize'){
          hassize = true;
        }
      });
    }           

    if (data.products.length > 0) {
      $('.qty_variant').html("No");
      var selectedProducts = data.products;
      var productdata = [];
      selectedProducts.forEach(function(selectedProduct){ //only one product is selected
        //read csv file
        var customer_detail = $('.order-creation-wrapper').attr('data-sel-id');
        var customerdetail = JSON.parse(customer_detail);
        var sel_customerid = customerdetail.id;
        var cust_id = sel_customerid.replace("gid://shopify/Customer/", "");
        var filepath = 'https://rbmembersportal.cartbrain.net/public/csvfiles/'+cust_id+'.csv';
        var csvcontents = '';
        // var url_path = '/rbmembersportal.cartbrain.net/public/csvfiles/'+sel_customerid+'.csv';
        console.log(filepath);
        $.ajax({
          url : filepath,
          dataType: "text",
          async: false,
          success : function (data) {
           csvcontents = $.csv.toObjects(data);
           console.log(csvcontents);
         }
        });
        //product sku vs price array
        var csv_product = [];      
        if (csvcontents != '') {       
        $.each(csvcontents, function(key,val){
          var csv_sku = val.sku;
          var csv_price = val.Price;
          csv_product[csv_sku]=csv_price;
        });
        }
        var individual_product = {};
        console.log(selectedProduct.variants);
        individual_product['id'] = selectedProduct.id;
        individual_product['variant_id'] = selectedProduct.variants[0].id;
        individual_product['handle'] = selectedProduct.handle;
        individual_product['title'] = selectedProduct.title;
        individual_product['product_type'] = selectedProduct.product_type;
        var variantsitems = selectedProduct.variants;
        variantsitems.forEach(function(variantsitem){
          var inventory_quantity =variantsitem.inventory_quantity;
          var sku = variantsitem.sku;
          console.log(sku);
          individual_product['sku'] = sku;
          var  price = variantsitem.price;
          console.log(price);
          individual_product['original_price'] = price;
          individual_product['price'] = price;
          individual_product['discount_amt'] = 0;
          console.log(jQuery.type(csv_product[sku]));
          if (csvcontents != '') {
            if(typeof csv_product[sku] != "undefined"){
              console.log("inside csv");
              var csv_price = csv_product[sku];
              console.log(csv_price);
              var discount_amt = parseFloat(price) - parseFloat(csv_price); 
              individual_product['price'] = csv_price;
              individual_product['discount_amt'] = discount_amt;
              console.log(csv_product[sku]);
            }
          }
        });
        console.log(selectedProduct);
        console.log(selectedProduct.images.length);
        var hasimage = selectedProduct.images.length;
        
        if (hasimage == 0) {          
          var product_image = selectedProduct.handle;
          individual_product['product_image'] = product_image;
        }
        else{
          var product_image = selectedProduct.images[0].src;
          individual_product['product_image'] = product_image; 
        }
        individual_product['tags'] = selectedProduct.tags;
        selectvarainsorqty(individual_product);
        productdata.push(individual_product);
        console.log(individual_product);

      });
      //After item added in order update subtotal/grandtotal
      //update the cart subtotal
      updatesubtotalandtotal();  
      $(".create-final-order").removeClass("btn-disabled");         
    }
    if (data.errors) {
      console.error(data.errors);
    }
    
  });
}

$('.Polaris-Modal-CloseButton').click(function(){
  $('.custom-modal-wrap').show();
});

$('#productselect , #productselectsearch').on('click',function(){
  addproductviashopifymodal();
});

function closemodal(){
  $('.custom-modal-wrap').hide();
  //reset form
  $('form.size-selection')[0].reset();
  $('form.size-selection').empty();
  $('form.size-selection').append('<div class="heightsizewrapper"></div>');
  var totalvariantadded = 1;
  $('.qty_variant').html("No");
  $('.qty_variant').removeClass("addedvariant");

}

$(document).on('click','.Polaris-Modal-CloseButton',function(){
  closemodal();
});
$(document).on('click','.cancel-on-config',function(){
  closemodal();
});
$(document).on('click','.item-to-custom-price',function(){
  var targetele = $(this).parent().parent().find('.input-custom-price');
  if($(this).is(':checked')){
    targetele.slideDown();
  }else{
    targetele.slideUp();
    //if has price set set to null
    targetele.val("");
  }
});

function custompriceupdated(){
  var lineitem_length = $('.indiv-row-tr').length;
  if(lineitem_length > 0){
  $('.indiv-row-tr').each(function(){
      var new_qty = $(this).find('.qty-input').val();
      var unit_price = $(this).find('.price.unit-price').attr('data-custom-price')?$(this).find('.price.unit-price').attr('data-custom-price'):$(this).find('.price.unit-price').attr('data-unit-price');
      var updated_price = (parseFloat(new_qty)*parseFloat(unit_price)).toFixed(2);
      // update_price = update_price.toFixed(2);
      $(this).find('.individual-total').html(updated_price);
      //update the cart subtotal  
  });
  updatesubtotalandtotal();
  $(".create-final-order").removeClass("btn-disabled");
}
else{
  $('#discount-amt').html('—');
  $('#shipping-amt').html('—');
  $('.sub-total').html('—');
  $('.taxes-applied').html('0.00');
  $('.grand-total').html('0.00');
  $(".order_note").val("");
  $("#PolarisTextField3").val("");
  $(".create-final-order").addClass("btn-disabled");
}
}

// $('.line-item-remove').click(function(){
$(document).on('click','.line-item-remove',function(){
  console.log("clicked");
  $(this).parent().remove();
  custompriceupdated();
});

$(document).on('keyup','.input-custom-price-box',function(){
  var new_custom_price = parseFloat($(this).val());
  //if new custom price is less than original price
  var unitpriceele = $(this).parent().parent().parent().find('.price.unit-price');
  var nextinputwrap = $(this).parent().parent().parent().parent().find('.next-input-wrapper');
  var originalprice = parseFloat(unitpriceele.attr('data-attr-org-price'));
  var unitpriceval = parseFloat(unitpriceele.attr('data-unit-price'));
  if(new_custom_price<originalprice){
    //strikethrough the unitprice
    unitpriceele.addClass('line-through');
    nextinputwrap.attr('data-custom-price',new_custom_price);
    unitpriceele.attr('data-custom-price',new_custom_price);
    //calculate new discount price
    var newdiscount_price =(originalprice-new_custom_price).toFixed(2);
    unitpriceele.attr('data-unit-discount',newdiscount_price);
  }else{
    unitpriceele.removeClass('line-through');
    nextinputwrap.attr('data-custom-price','');
    unitpriceele.attr('data-custom-price','');
    //calculate new discount price
    var newdiscount_price =(originalprice-unitpriceval).toFixed(2);
    unitpriceele.attr('data-unit-discount',newdiscount_price);
  }
  custompriceupdated();
});

//new code
$(document).on('click','button.custom-popup-add-product-to-order',function(){
  var hasaddededvariant = $('.qty_variant').hasClass("addedvariant");
  // if (hasaddededvariant) {
  //  // closemodal();    
  // }
  // else {
  var dataArray = $('.custom-modal-wrap-popup').find('form.size-selection').serializeArray();  
  console.log(dataArray);
  var dataObj = {};
  $(dataArray).each(function(i, field){
    dataObj[field.name] = field.value;
  });
  var properties_start ='<span class="properties">';
  var properties_end ='</span>';
  var properties_all = ''; 
  var input_qty = $('.custom-modal-wrap-popup').find('.qty-wrapper input').val()!=''?$('.custom-modal-wrap-popup').find('.qty-wrapper input').val():1;
  var hasrequiredvalue = true;
  $('form.size-selection').find('.custom-option').each(function(){
    var label_index = $(this).find(".label_index").text();
    var label_value = dataObj[''+label_index+''];
    // console.log(label_value);
    // var label_value = label_value.replace("*", '"');
    console.log(label_value);
    var has_required = $(this).find('.required').length;
    if (has_required > 0) {
      console.log(has_required);
      if (label_value == "") {
        console.log(label_value);
        $(this).find('.inputvalue').css('border-color','red');
        $(this).find('.selectvalue').css('border-color','red');
        hasrequiredvalue = false;
        hasrequiredvalue = false;
      }
    }
    if(typeof(label_value) != "undefined"){
      var properties_individual ='<span class="property"><span class="name">'+label_index+' :</span><span class="value">'+label_value+'</span></span>'
      properties_all += properties_individual;
    }
  });
  var properties = properties_start + properties_all+ properties_end;
  var individual_product = $.parseJSON($('.size-variant-product-row').attr('data-attr-product'));
  console.log(properties);
  console.log(jQuery.type(hasrequiredvalue));
  if (hasrequiredvalue) {
    var productrow = getproductrowhtml(individual_product['variant_id'],individual_product['product_image'],individual_product['title'],individual_product['sku'],individual_product['price'],individual_product['original_price'],individual_product['discount_amt'],input_qty,properties);
    $('#line_item_rows').append(productrow);
    closemodal();
  }
  updatesubtotalandtotal();
// }
});

$('.selectvalue').on('change', function() {
   $(this).css('border-color','#ccc');
});
$('.inputvalue').on('keyup', function() {
   $(this).css('border-color','#ccc');
});


// edit shipping adddress
$(document).on('click','#shipping-edit',function(){
  $('.custom-shipping-wrap-popup').show();
});
$(document).on('click','.save-shipping',function(){
  var first_name= $('input[name="customer[firstName]"]').val();
  var last_name= $('input[name="customer[lastName]"]').val();
  var company= $('input[name="customer[company]"]').val();
  var address1= $('input[name="customer[address1]"]').val();
  var address2= $('input[name="customer[address2]"]').val();
  var city= $('input[name="customer[city]"]').val();
  var province_code= $('.state_choice').find(":selected").text();
  var province = $('.state_choice').val();
  var zip_code= $('input[name="customer[zip]"]').val();
  var phone= $('input[name="customer[phone]"]').val();
  var country_name= $('.country_name').find(":selected").text();
  var countrycode= $('.country_name').val();
  $("#first_name , #sfirstname").text(first_name);
  $("#last_name , #slastname").text(last_name);
  // $("#email").text(email);
  $("#company").text(company);
  $("#address1").text(address1);
  $("#address2").text(address2);
  $("#city").text(city);
  $("#province_code").text(province_code);
  $("#zip").text(zip_code);
  $("#country_name").text(country_name);
  $('.custom-shipping-wrap-popup').hide();
  var shipping_data = {"first_name":first_name,"last_name":last_name,"company":company,"address1":address1,"address2":address2,
  "city":city,"province_code":province_code,"zip_code":zip_code,"phone":phone,"country_name":country_name,"province":province,"countrycode":countrycode};
  $('.order-creation-wrapper').attr('shipping_data',JSON.stringify(shipping_data));
});

$('.pickup-date').change(function() { 
    $('.datepicker-dropdown').hide();
});
