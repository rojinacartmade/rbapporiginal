<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use OhMyBrew\ShopifyApp\Models\Shop;
use Log;
use OhMyBrew\BasicShopifyAPI;
use OhMyBrew\ShopifyApp\Facades\ShopifyApp;
use Mail;
use View;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
        public function handle()
        {
            Log::info('inside handle of job');                
            $shop_email_for_email = '';
            $shop = ShopifyApp::shop();
            $shopdomain = $shop->shopify_domain;
            $shop_data = $shop->api()->rest('GET', '/admin/shop.json');
            $shops=$shop_data->body;
            foreach ($shops as $key => $value) {
               $shop_name_for_email = $value->name;
               $shop_url = $value->myshopify_domain; 
               $shop_email_for_email = $value->email; 
            }
             View::share('shop_name_for_email', $shop_name_for_email);
                    $data = array('name'=>$shop_name_for_email, 'email'=>$shop_email_for_email);
                    // Path or name to the blade template to be rendered
                    $template_path = 'email_template';

            $results = DB::select( DB::raw("SELECT * FROM mail_status WHERE store_name = '$shop_email_for_email'") );
            $countresult = count($results);
            if (!$countresult) {                   
                Mail::send($template_path, $data, function($message) {
                    $shop = ShopifyApp::shop();
                    $shopdomain = $shop->shopify_domain;
                    $shop_data = $shop->api()->rest('GET', '/admin/shop.json');
                    $shops=$shop_data->body;
                    foreach ($shops as $key => $value) {
                       $shop_name_for_email = $value->name;
                       $shop_email_for_email = $value->email;
                    }       
                    // Set the receiver and subject of the mail.
                    $message->to($shop_email_for_email, $shop_name_for_email)->subject('Welcome to Cartmade Handpick Related');
                    // Set the sender
                    $message->from('sandesh.paudyal@cartmade.com','Cartmade Handpick Related');
                    $message->subject('Welcome to Cartmade Handpick Related');
                });

                    $resultsend = DB::INSERT( DB::raw("INSERT INTO mail_status (store_name, email_status) VALUES ('$shopdomain','1')") );               
            }

    }
}