<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OhMyBrew\BasicShopifyAPI;
use OhMyBrew\ShopifyApp\Facades\ShopifyApp;
use App\User;
use OhMyBrew\ShopifyApp\Models\Shop;
use View;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Support\Collection;
class orderController extends Controller
{
	  // for order filter
	 // save order to database
	public function orderfilter(Request $res){
		$responsedata = $res->all();
		$shop_domain = ShopifyApp::shop();
		$shopdomain = $shop_domain->shopify_domain;
		$shopify_token = $shop_domain->shopify_token;
		$ordernumber = "";
		$firstname="";
		$lastname="";
		$deliverystatus="";
		$schoolname="";
		$no_of_order_list="20";
		// dd($responsedata);
		if (array_key_exists('ordernumber', $responsedata)) {
		    $ordernumber = $responsedata['ordernumber'];
		}
		if (array_key_exists('firstname', $responsedata)) {
		    $firstname = $responsedata['firstname'];
		}
		if (array_key_exists('lastname', $responsedata)) {
		    $lastname = $responsedata['lastname'];
		}
		if (array_key_exists('deliverystatus', $responsedata)) {
		    $deliverystatus = $responsedata['deliverystatus'];
		}
		if (array_key_exists('schoolname', $responsedata)) {
		    $schoolname = $responsedata['schoolname'];
		}
		if (array_key_exists('no_of_order_list', $responsedata)) {
		    $no_of_order_list = $responsedata['no_of_order_list'];
		}
		
		$searchdata = json_encode($responsedata);
		// print_r(json_encode($responsedata));
		
	
		try{

			// $first_name_filter = true;
			$valuesnode = DB::table('order_table')
                ->where([
                		['order_name', 'like', '%'.$ordernumber.'%'],
                	    ['customer_first_name', 'like', '%'.$firstname.'%'],
            	        ['customer_last_name', 'like', '%'.$lastname.'%'],
            	        ['fulfillment_status','like', $deliverystatus.'%'],
            	        ['reference', 'like', '%'.$schoolname.'%']
            	    ])
                ->orderBy('id', 'DESC')
                ->paginate($no_of_order_list);
                $valuesnode->appends(['firstname' => $firstname, 'lastname' =>$lastname, 'deliverystatus'=>$deliverystatus, 'schoolname'=>$schoolname, 'no_of_order_list'=>$no_of_order_list, 'ordernumber'=>$ordernumber ]);
		
			return view('orderlisting')->with(compact('valuesnode','searchdata'));
        }
        catch (shopify\ApiException $e)
        {
            # HTTP status code was >= 400 or response contained the key 'errors'
            echo $e;
            print_r($e->getRequest());
            print_r($e->getResponse());
        }
    }

    // get invoice list
		public function getinvoicefromdatabase(Request $res){
			// $orderid  = 2696706752671;
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			 $responsedata = $res->all();
			 $no_of_order_list="";
			 $invoice_number="";
			 $customer_name="";
			 $customer_last_name="";

			 $no_of_order_list="20";
				// dd($responsedata);
				if (array_key_exists('no_of_order_list', $responsedata)) {
				    $no_of_order_list = $responsedata['no_of_order_list'];
				}
				if (array_key_exists('invoice_number', $responsedata)) {
				    $invoice_number = $responsedata['invoice_number'];
				    $invoice_number =strtoupper($invoice_number) ;
				    $invoice_number = str_replace("INV","",$invoice_number);
				    $invoice_number = str_replace("I","",$invoice_number);
				    $invoice_number = str_replace("N","",$invoice_number);
				    $invoice_number = str_replace("V","",$invoice_number);
				}
				if (array_key_exists('customer_name', $responsedata)) {
				    $customer_name = $responsedata['customer_name'];
				}
				if (array_key_exists('customer_last_name', $responsedata)) {
				    $customer_last_name = $responsedata['customer_last_name'];
				}

				$searchdata = json_encode($responsedata);
				// print_r($searchdata);
			try{
                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);

                // $first_name_filter = true;
			$valuesnode = DB::table('invoice_table')
                ->where([
                		['invoice_number', 'like', '%'.$invoice_number.'%'],
                	    ['customer_first_name', 'like', '%'.$customer_name.'%'],
            	        ['customer_last_name', 'like', '%'.$customer_last_name.'%']
            	    ])
                ->orderBy('id', 'DESC')
                ->paginate($no_of_order_list);
                $valuesnode->appends(['invoice_number' => $invoice_number, 'customer_name' =>$customer_name, 'customer_last_name'=>$customer_last_name ]);

                // $valuesnode = DB::table('invoice_table')->orderBy('id', 'desc')->paginate(20);
                // ;
				return view('invoicelisting')->with(compact('valuesnode','searchdata'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
        }


}