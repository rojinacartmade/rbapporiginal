<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OhMyBrew\BasicShopifyAPI;
use OhMyBrew\ShopifyApp\Facades\ShopifyApp;
use App\User;
use OhMyBrew\ShopifyApp\Models\Shop;
use View;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Support\Collection;
class invoiceController extends Controller
{
		public function invoicepaymentcustomer(){
			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false;
			$allcursors=array(); 
			$valuesnode=array();

			$dataEdit='query	{
				customers(first:20)	{
				pageInfo{
				hasNextPage
				hasPreviousPage
				}
				edges{
				cursor
				node{
				id
				tags 
				firstName
				lastName
				email
				phone
				addresses {
				id
				country
				province                  
				provinceCode
				address1
				address2
				company
				city
				zip

				}
				createdAt
				}
				}
				}
			}';

			$request_edit=$api->graph($dataEdit);
			// dd($request_edit);
			$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
			$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

			$edges = $request_edit->body->customers->edges;

			try{
				$customerdata = array();
				$customerfinaldata = array();
				$customeraddresses = array();
				$customeraddressdata=array();
				foreach ($edges as $key=>$value) {
					$values = $value->node;
					array_push($valuesnode, $value->node);

					// $values = (array) $values;
					$value=(array) $value;

					array_push($allcursors, $value['cursor']);
				}
			} catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }
			

			return view('invoicepayment_customer')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
		}

		public function paginateinvoicepaymentcustomertabnext(Request $res){
			$responsedata=$res->all(); 
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false; 
			$allcursors=array(); 
			$valuesnode=array();    


			// dd($responsedata);
			if(!empty($responsedata)){
				if (array_key_exists('hasfornext', $responsedata)) {
					$hasfornextcur=$responsedata['hasfornext'];
					$hasnextprodstat = $responsedata['hasnextprod'];
					if($hasnextprodstat){
						$dataEdit='query	{
						customers(first: 20 after:"'.$hasfornextcur.'")	{
						pageInfo{
						hasNextPage
						hasPreviousPage
						}
						edges{
						cursor
						node{
						id
						tags 
						firstName
						lastName
						email
						phone
						addresses {
						id
						country
						province                  
						provinceCode
						address1
						address2
						company
						city
						zip

						}
						createdAt
						}
						}
						}
						}';

						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					} 
				}



				if (array_key_exists('hasforback', $responsedata)) {
					$hasforbackcur=$responsedata['hasforback'];
					$hasbackprodstat = $responsedata['hasbackprod'];
					if($hasbackprodstat){
						$dataEdit='query	{
							customers(last: 20 before:"'.$hasforbackcur.'")	{
							pageInfo{
							hasNextPage
							hasPreviousPage
							}
							edges{
							cursor
							node{
							id
							tags 
							firstName
							lastName
							email
							phone
							addresses {
							id
							country
							province                  
							provinceCode
							address1
							address2
							company
							city
							zip

							}
							createdAt
							}
							}
							}
						}';
						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					}    

				}  

				if (isset($edges)){  
				try
				{
					$customerdata = array();
					$customerfinaldata = array();
					$customeraddresses = array();
					$customeraddressdata=array();
					foreach ($edges as $key=>$value) {
						$values = $value->node;
						array_push($valuesnode, $value->node);
						$value=(array) $value;
						array_push($allcursors, $value['cursor']);
					}
				}catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }

					return view('invoicepayment_customer')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
				}else{
				return \Redirect::to('/invoicepaymentcustomerlist');
				}

			}else{
				return \Redirect::to('/invoicepaymentcustomerlist');             
			}

		}

		public function invoicecustomersearch(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all(); 
        	if (array_key_exists('firstname', $responsedata)) {
			    $firstname = $responsedata['firstname'];
			}
			else{
				$firstname = '';
			}
			if (array_key_exists('lastname', $responsedata)) {
			    $lastname = $responsedata['lastname'];
			}
			else{
				$lastname = '';
			}        	        	
        	
        	$query = $firstname." ".$lastname;
			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false;
			$allcursors=array(); 
			$valuesnode=array();
			// $query = 'Nic ';
			// print_r(json_encode($query));
			// dd();
			$dataEdit='query	{
				customers(first:20  query:"'.$query.'")	{
				pageInfo{
				hasNextPage
				hasPreviousPage
				}
				edges{
				cursor
				node{
				id
				tags 
				firstName
				lastName
				email
				phone
				addresses {
				id
				country
				province                  
				provinceCode
				address1
				address2
				company
				city
				zip

				}
				createdAt
				}
				}
				}
			}';

			$request_edit=$api->graph($dataEdit);
			// dd($request_edit);
			$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
			$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

			$edges = $request_edit->body->customers->edges;

			try{
				$customerdata = array();
				$customerfinaldata = array();
				$customeraddresses = array();
				$customeraddressdata=array();
				foreach ($edges as $key=>$value) {
					$values = $value->node;
					array_push($valuesnode, $value->node);

					// $values = (array) $values;
					$value=(array) $value;

					array_push($allcursors, $value['cursor']);
				}
			} catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }
			

			return view('invoicepayment_customer')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
		}

		
		public function invoicecustomeremails(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	$customer_id =$responsedata['customerid'];
        	// dd($customer_id);			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			try{
                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);
                $valuesnode = DB::table('invoice_table')->where('customerid', '=', $customer_id)->paginate(20);
                // $valuesnode = DB::table('order_table')->orderBy('id', 'desc')->paginate(5);
              	// dd($valuesnode);
				return view('invoiceemails')->with(compact('valuesnode'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}

		
		public function updateinvoicepaymentstatus(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	// print_r(json_encode($responsedata));
        	// dd();
        	$orderid =$responsedata['orderid'];
        	$paymentdata =$responsedata['paymentdata'];
        	$comment =$responsedata['comment'];
        	$paymentmethod =$responsedata['paymentmethod'];
        	// dd($customer_id);			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			try{
				if ($paymentdata == "Full Payment") {
					$status = "Paid";
					$fulfillment_status = "Fulfilled";
				}
				else{
					$status = "Partially Paid";
					$fulfillment_status = "Unfulfilled";
				}
				$updateDetails = [
				    'payment_methods' => $paymentmethod,
				    'comment' => $comment,
				    'status' =>$status
				];

				$updateorderDetails =[
				    'fulfillment_status' => $fulfillment_status
				];

				$valuesnode = DB::table('invoice_table')
				    ->where('orderid', $orderid)
				    ->update($updateDetails);

				$orderupdate = DB::table('order_table')
				    ->where('order_id', $orderid)
				    ->update($updateorderDetails);

				// print_r(json_encode($orderupdate));
				// dd(); 

				 if ($valuesnode || $orderupdate) {
				 	print_r(json_encode("Payment Process Success"));
				 	dd();
				 }else{
				 	print_r(json_encode("Payment Process Failed"));
				 	dd();
				 }

                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);
                // $valuesnode = DB::table('invoice_table')->where('customerid', '=', $customer_id)->paginate(10);
                // $valuesnode = DB::table('order_table')->orderBy('id', 'desc')->paginate(5);
              	// dd($valuesnode);
				// return view('invoiceemails')->with(compact('valuesnode'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}
		
		public function invoicecustomeremailslist(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	$customer_id =$responsedata['customerid'];
        	// dd($customer_id);			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$searchdata = json_encode($responsedata);
			try{
                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);
                $valuesnode = DB::table('invoice_group_tbl')->where('customerid', '=', $customer_id)->orderBy('invoice_group_id', 'DESC')->paginate(20);
                // $valuesnode = DB::table('order_table')->orderBy('id', 'desc')->paginate(5);
              	// dd($valuesnode);
				return view('customerinvoiceemaillist')->with(compact('valuesnode','searchdata'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}

		
		public function getinvoicefromgroup(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	$groupid =$responsedata['groupid'];
        	// dd($customer_id);
        	$invoicelists = array();			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			try{                
                $valuesnode = DB::table('invoice_vs_group')->where('invoice_group_id', '=', $groupid)->get();
                foreach ($valuesnode as $key => $invoiceorder) {
                	$orderid = $invoiceorder->orderid;
                	$ordernumber = $invoiceorder->ordernumber;
                	$invoice = DB::table('invoice_table')->where('orderid', '=', $orderid)->get();
                	array_push($invoicelists, $invoice[0]);
                	// print_r(json_encode($invoice));
                	// die();
                }
                print_r(json_encode($invoicelists));
                die();
				return view('customerinvoiceemaillist')->with(compact('valuesnode'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}

		public function updateorderinvoicepaymentstatus(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	$comment =$responsedata['comment'];
        	$paymentmethod =$responsedata['paymentmethod']; 
        	$groupid = $responsedata['groupid']; 
        	$Success = false;
        	$groupstatus = false;
        	$statusarray = array();
			try{
				$orderarray =json_decode($responsedata['orderarray']);
	        	foreach ($orderarray as $key => $individualorder) {
	        		$data = $individualorder;
	        		$orderid = $data->orderid;
	        		$paymentdata = $data->paymentdata; 
	        		$valuesnode = DB::table('invoice_table')->where('orderid', '=', $orderid)->orderBy('id', 'DESC')->get();
	        		// print_r(json_encode($valuesnode[0]->Deposit));
	        		// dd();
	        		$invstatus = $valuesnode[0]->status;
	        		$deposie_status = $valuesnode[0]->Deposit;
	        		$Deposit = "Paid";
					if ($paymentdata == "Full Payment") {
						$status = "Paid";
						// $fulfillment_status = "Fulfilled";
						$orderstatus = "Settled";
						$deposite_paid = "Paid";
					}
					else{
						$status = "Deposit paid";
						$fulfillment_status = "Unfulfilled";
						$orderstatus = "Deposit paid";
						$deposite_paid = $paymentdata;
					}
					// print_r(json_encode($invstatus));
	    //     		dd();
					if ($invstatus=="Pending"  && $deposie_status == "Paid") {
						$status = "Paid";
						// $fulfillment_status = "Fulfilled";
						$orderstatus = "Settled";
						$deposite_paid = "Paid";
						// print_r(json_encode($invstatus));
	     //    			dd();
					}
					if ($invstatus=="Pending" && $deposie_status == "Not Paid"  && $paymentdata != "Full Payment") {
						$status = "Deposit paid";
						$fulfillment_status = "Unfulfilled";
						$orderstatus = "Deposit paid";
						$deposite_paid = $paymentdata;
						// print_r(json_encode($invstatus));
	     //    			dd();
					}
					$payment_date = date("d/m/Y");
					$updateDetails = [
					    'payment_methods' => $paymentmethod,
					    'comment' => $comment,
					    'status' =>$status,
					    'Deposit' =>$Deposit,
					    'payment_date' => $payment_date
					];
					// print_r(json_encode($updateDetails));
	    //     		dd();

					// $updateorderDetails =[
					//     'fulfillment_status' => $fulfillment_status
					// ];

					$paid = "Paid";

					$updateorderDetail = [
					    'comment' => $comment,
					    'status' =>$orderstatus,
					    'deposite' =>$paid,
					    'deposite_paid'=>$deposite_paid
					];

					$valuesnode = DB::table('invoice_table')
					    ->where('orderid', $orderid)
					    ->update($updateDetails);

					// $orderupdate = DB::table('order_table')
					//     ->where('order_id', $orderid)
					//     ->update($updateorderDetails);					

					$updateorder = DB::table('order_table')
					    ->where('order_id', $orderid)
					    ->update($updateorderDetail);

					 if ($valuesnode || $updateorder) {
					 	$Success = true;
					 }else{
					 	$Success = false;
					 }
					$valuesnode = DB::table('invoice_table')->where('orderid', '=', $orderid)->orderBy('id', 'DESC')->get();
	        		$invstatus = $valuesnode[0]->status;
	        		array_push($statusarray, $invstatus);
				}
				if ($valuesnode || $updateorder) {
					
					$updategroupstatus = array();
					$valuesnode = DB::table('invoice_vs_group')->where('invoice_group_id', '=', $groupid)->orderBy('id', 'DESC')->get();
					foreach ($valuesnode as $key => $invoices) {
						$invoiceid = $invoices->invoice_id;
						$valuesnode = DB::table('invoice_table')->where('id', '=', $invoiceid)->orderBy('id', 'DESC')->get();
							foreach ($valuesnode as $key => $invoices) {
							if ( !is_null($valuesnode[0]->Deposit)) {
								$invoice_status = $valuesnode[0]->Deposit;
								array_push($updategroupstatus, $invoice_status);							
							}
							else{
								$invoice_status = "Not Paid";
								array_push($updategroupstatus, $invoice_status);
							}
						}
					}
					if (in_array("Not Paid", $updategroupstatus)){
						$invgrpstatus = 'Partially paid';
					}
					else{
						$invgrpstatus = 'Paid';
					}

					$updatestatusDetails = [
					    'payment_method' => $paymentmethod,
					    'comment' => $comment,
					    'status' =>$invgrpstatus
					];

					$updategroupinvoice = DB::table('invoice_group_tbl')
				    ->where('invoice_group_id', $groupid)
				    ->update($updatestatusDetails);

				  //   print_r(json_encode($updategroupstatus));
				 	// dd();

				 	print_r(json_encode("Payment Process Success"));
				 	dd();
				}else{
				 	print_r(json_encode("Payment Process Failed"));
				 	dd();
				 }

            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}


		public function orderlistingforinvoice(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	$customer_id =$responsedata['customerid'];
        	// dd($customer_id);			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			try{
                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);
                $valuesnode = DB::table('order_table')->where('customer_id', '=', $customer_id)->orderBy('id', 'DESC')->paginate(20);
                // $valuesnode = DB::table('order_table')->orderBy('id', 'desc')->paginate(5);
              	// dd($valuesnode);
				return view('invoice_order')->with(compact('valuesnode'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}

		
		public function getorderinvoicestatusdetail(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	$orderid =$responsedata['orderid'];
        	// dd($customer_id);			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			try{
                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);
                $valuesnode = DB::table('invoice_table')->where('orderid', '=', $orderid)->orderBy('id', 'DESC')->get();
                // $valuesnode = DB::table('order_table')->orderBy('id', 'desc')->paginate(5);
                print_r(json_encode($valuesnode));
              	dd();
				return view('invoice_order')->with(compact('valuesnode'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}

		public function deletegroupinvoice(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all();
        	$groupid =$responsedata['groupid'];
        	// print_r(json_encode($groupid));
         //    dd();
			try{
                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);
                $valuesnode = DB::table('invoice_vs_group')->where('invoice_group_id', '=', $groupid)->orderBy('id', 'DESC')->get();
                foreach ($valuesnode as $key => $value) {
                	$invoice_id =$value->invoice_id;
                	$valuesinvoicenode = DB::table('invoice_table')->where('id', '=', $invoice_id)->orderBy('id', 'DESC')->get();
                	foreach ($valuesinvoicenode as $key => $value) {
                		$payment_methods = $value->payment_methods;
                		$orderid = $value->orderid;
                		$ordernode = DB::table('order_table')->where('order_id', '=', $orderid)->orderBy('id', 'DESC')->get();
                		$order_deposite  = $ordernode[0]->deposite;
                		$order_deposite_paid  = $ordernode[0]->deposite_paid;
                		$order_status  = $ordernode[0]->status;
                		if ($order_deposite == "Paid" && $order_deposite_paid != "Paid") {
                			$updateorderDetails = [
							    'status' =>"Deposit paid"
							];

							$updateinvoiceDetails = [
							    'status' =>"Deposit paid",
							    'amttobepaid' => $order_deposite_paid
							];

                		}
                		elseif ($order_deposite == "Paid" && $order_deposite_paid == "Paid") {
                			$updateorderDetails = [
							    'status' =>"Settled"
							];

							$updateinvoiceDetails = [
							    'status' =>"Paid"

							];
                		}
                		else{
                			$updateorderDetails = [
							    'status' =>"Unsettled"
							];

							$updateinvoiceDetails = [
							    'status' =>"Unsettled",
							    'amttobepaid' => "No Deposite"
							];
                		}
                		// print_r(json_encode($updateinvoiceDetails));
                		// dd();		

						$orderupdate = DB::table('order_table')
						    ->where('order_id', $orderid)
						    ->update($updateorderDetails); 				

						$orderupdate = DB::table('invoice_table')
						    ->where('orderid', $orderid)
						    ->update($updateinvoiceDetails);               		

                	}
                }
                $valuesnode = DB::table('invoice_group_tbl')->where('invoice_group_id', $groupid)->delete();
                $valuesnode = DB::table('invoice_vs_group')->where('invoice_group_id', $groupid)->delete();
                // $valuesnode = DB::table('order_table')->orderBy('id', 'desc')->paginate(5);

                print_r(json_encode("Invoice Email Deleted."));
              	dd();
				return view('invoice_order')->with(compact('valuesnode'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}

	public function invoicesearch(Request $res){
		$responsedata = $res->all();
		$shop_domain = ShopifyApp::shop();
		$shopdomain = $shop_domain->shopify_domain;
		$shopify_token = $shop_domain->shopify_token;
		$ordernumber = "";
		$deliverystatus="";
		$no_of_order_list="20";
		$customerid = '';
		$delivery_status = "";
		// dd($responsedata);
		if (array_key_exists('ordernumber', $responsedata)) {
		    $ordernumber = $responsedata['ordernumber'];
		}
		if (array_key_exists('customerid', $responsedata)) {
		    $customerid = $responsedata['customerid'];
		}
		if (array_key_exists('deliverystatus', $responsedata)) {
		    $deliverystatus = $responsedata['deliverystatus'];
		    if ($deliverystatus == "Unsettled") {
		    	$delivery_status = "Pending";
		    }
		    else{
		    	$delivery_status = $deliverystatus;
		    }
		}
		if (array_key_exists('no_of_order_list', $responsedata)) {
		    $no_of_order_list = $responsedata['no_of_order_list'];
		}
		
		$searchdata = json_encode($responsedata);
		// print_r(json_encode($responsedata));
		
	
		try{

			// $first_name_filter = true;
			$valuesnode = DB::table('order_table')
                ->where([
                		['order_name', 'like', '%'.$ordernumber.'%'],
                		['customer_id', '=', $customerid],
            	        ['status','like', $deliverystatus.'%']
            	    ])
                ->orWhere([
                		['order_name', 'like', '%'.$ordernumber.'%'],
                		['customer_id', '=', $customerid],
            	        ['status','like', $delivery_status.'%']
            	    ])
                ->orderBy('id', 'DESC')
                ->paginate($no_of_order_list);
                $valuesnode->appends(['deliverystatus'=>$deliverystatus,'no_of_order_list'=>$no_of_order_list, 'ordernumber'=>$ordernumber ,'$customerid'=>$customerid]);
		
			return view('invoice_order')->with(compact('valuesnode','searchdata'));
        }
        catch (shopify\ApiException $e)
        {
            # HTTP status code was >= 400 or response contained the key 'errors'
            echo $e;
            print_r($e->getRequest());
            print_r($e->getResponse());
        }
    }

    
    // get product image
	public function getproductimage(Request $request){
		$shop_domain = ShopifyApp::shop();
		$shopdomain = $shop_domain->shopify_domain;
		$shopify_token = $shop_domain->shopify_token;
		$orderdata  = $request->all();
		$productid = json_decode($orderdata['product_id']);		
		try{
            $shop = ShopifyApp::shop();
            $product = $shop->api()->rest('GET', '/admin/products/'.$productid.'.json');
            print_r(json_encode($product->body->product->images));
    		dd();	               

        }
        catch (shopify\ApiException $e)
        {
            # HTTP status code was >= 400 or response contained the key 'errors'
            echo $e;
            print_r($e->getRequest());
            print_r($e->getResponse());
        }
        print_r(json_encode($productid));
        dd();
       return view('invoicecreation')->with(compact('orderdetails'));
	}

}