<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OhMyBrew\BasicShopifyAPI;
use OhMyBrew\ShopifyApp\Facades\ShopifyApp;
use App\User;
use OhMyBrew\ShopifyApp\Models\Shop;
use View;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
	class ShopController extends Controller
	{
		public function indexredirect(){
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false;
			$allcursors=array(); 
			$valuesnode=array();

			$dataEdit='query	{
				customers(first:20)	{
				pageInfo{
				hasNextPage
				hasPreviousPage
				}
				edges{
				cursor
				node{
				id
				tags 
				firstName
				lastName
				email
				phone
				addresses {
				id
				country
				province                  
				provinceCode
				address1
				address2
				company
				city
				zip

				}
				createdAt
				}
				}
				}
			}';

			$request_edit=$api->graph($dataEdit);
			// dd($request_edit);
			$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
			$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

			$edges = $request_edit->body->customers->edges;

			try{
				$customerdata = array();
				$customerfinaldata = array();
				$customeraddresses = array();
				$customeraddressdata=array();
				foreach ($edges as $key=>$value) {
					$values = $value->node;
					array_push($valuesnode, $value->node);

					// $values = (array) $values;
					$value=(array) $value;

					array_push($allcursors, $value['cursor']);
				}
			} catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }
			

			return view('welcome')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
			}
		

		public function paginatecustomernext(Request $res){
			$responsedata=$res->all(); 
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false; 
			$allcursors=array(); 
			$valuesnode=array();    


			// dd($responsedata);
			if(!empty($responsedata)){
				if (array_key_exists('hasfornext', $responsedata)) {
					$hasfornextcur=$responsedata['hasfornext'];
					$hasnextprodstat = $responsedata['hasnextprod'];
					if($hasnextprodstat){
						$dataEdit='query	{
						customers(first: 20 after:"'.$hasfornextcur.'")	{
						pageInfo{
						hasNextPage
						hasPreviousPage
						}
						edges{
						cursor
						node{
						id
						tags 
						firstName
						lastName
						email
						phone
						addresses {
						id
						country
						province                  
						provinceCode
						address1
						address2
						company
						city
						zip

						}
						createdAt
						}
						}
						}
						}';

						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					} 
				}



				if (array_key_exists('hasforback', $responsedata)) {
					$hasforbackcur=$responsedata['hasforback'];
					$hasbackprodstat = $responsedata['hasbackprod'];
					if($hasbackprodstat){
						$dataEdit='query	{
							customers(last: 20 before:"'.$hasforbackcur.'")	{
							pageInfo{
							hasNextPage
							hasPreviousPage
							}
							edges{
							cursor
							node{
							id
							tags 
							firstName
							lastName
							email
							phone
							addresses {
							id
							country
							province                  
							provinceCode
							address1
							address2
							company
							city
							zip

							}
							createdAt
							}
							}
							}
						}';
						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					}    

				}  

				if (isset($edges)){  
				try
				{
					$customerdata = array();
					$customerfinaldata = array();
					$customeraddresses = array();
					$customeraddressdata=array();
					foreach ($edges as $key=>$value) {
						$values = $value->node;
						array_push($valuesnode, $value->node);
						$value=(array) $value;
						array_push($allcursors, $value['cursor']);
					}
				}catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }

					return view('welcome')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
				}else{
				return \Redirect::to('/');
				}

			}else{
				return \Redirect::to('/');             
			}

		}

		// function for customer tab 
		public function rbcustomers(){
			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false;
			$allcursors=array(); 
			$valuesnode=array();

			$dataEdit='query	{
				customers(first:20)	{
				pageInfo{
				hasNextPage
				hasPreviousPage
				}
				edges{
				cursor
				node{
				id
				tags 
				firstName
				lastName
				email
				phone
				addresses {
				id
				country
				province                  
				provinceCode
				address1
				address2
				company
				city
				zip

				}
				createdAt
				}
				}
				}
			}';

			$request_edit=$api->graph($dataEdit);
			// dd($request_edit);
			$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
			$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

			$edges = $request_edit->body->customers->edges;

			try{
				$customerdata = array();
				$customerfinaldata = array();
				$customeraddresses = array();
				$customeraddressdata=array();
				foreach ($edges as $key=>$value) {
					$values = $value->node;
					array_push($valuesnode, $value->node);

					// $values = (array) $values;
					$value=(array) $value;

					array_push($allcursors, $value['cursor']);
				}
			} catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }
			

			return view('customerlisting')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
		}

		// paginate for customer tab for next and back

		public function paginatecustomertabnext(Request $res){
			$responsedata=$res->all(); 
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false; 
			$allcursors=array(); 
			$valuesnode=array();    


			// dd($responsedata);
			if(!empty($responsedata)){
				if (array_key_exists('hasfornext', $responsedata)) {
					$hasfornextcur=$responsedata['hasfornext'];
					$hasnextprodstat = $responsedata['hasnextprod'];
					if($hasnextprodstat){
						$dataEdit='query	{
						customers(first: 20 after:"'.$hasfornextcur.'")	{
						pageInfo{
						hasNextPage
						hasPreviousPage
						}
						edges{
						cursor
						node{
						id
						tags 
						firstName
						lastName
						email
						phone
						addresses {
						id
						country
						province                  
						provinceCode
						address1
						address2
						company
						city
						zip

						}
						createdAt
						}
						}
						}
						}';

						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					} 
				}



				if (array_key_exists('hasforback', $responsedata)) {
					$hasforbackcur=$responsedata['hasforback'];
					$hasbackprodstat = $responsedata['hasbackprod'];
					if($hasbackprodstat){
						$dataEdit='query	{
							customers(last: 20 before:"'.$hasforbackcur.'")	{
							pageInfo{
							hasNextPage
							hasPreviousPage
							}
							edges{
							cursor
							node{
							id
							tags 
							firstName
							lastName
							email
							phone
							addresses {
							id
							country
							province                  
							provinceCode
							address1
							address2
							company
							city
							zip

							}
							createdAt
							}
							}
							}
						}';
						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					}    

				}  

				if (isset($edges)){  
				try
				{
					$customerdata = array();
					$customerfinaldata = array();
					$customeraddresses = array();
					$customeraddressdata=array();
					foreach ($edges as $key=>$value) {
						$values = $value->node;
						array_push($valuesnode, $value->node);
						$value=(array) $value;
						array_push($allcursors, $value['cursor']);
					}
				}catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }

					return view('customerlisting')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
				}else{
				return \Redirect::to('/');
				}

			}else{
				return \Redirect::to('/');             
			}

		}


		// for customer listing in invoice
		// function for customer tab 
		public function invoicecustomer(){
			
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false;
			$allcursors=array(); 
			$valuesnode=array();

			$dataEdit='query	{
				customers(first:20)	{
				pageInfo{
				hasNextPage
				hasPreviousPage
				}
				edges{
				cursor
				node{
				id
				tags 
				firstName
				lastName
				email
				phone
				addresses {
				id
				country
				province                  
				provinceCode
				address1
				address2
				company
				city
				zip

				}
				createdAt
				}
				}
				}
			}';

			$request_edit=$api->graph($dataEdit);
			// dd($request_edit);
			$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
			$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

			$edges = $request_edit->body->customers->edges;

			try{
				$customerdata = array();
				$customerfinaldata = array();
				$customeraddresses = array();
				$customeraddressdata=array();
				foreach ($edges as $key=>$value) {
					$values = $value->node;
					array_push($valuesnode, $value->node);

					// $values = (array) $values;
					$value=(array) $value;

					array_push($allcursors, $value['cursor']);
				}
			} catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }
			

			return view('invoice_customer')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
		}

		public function paginateinvoicecustomertabnext(Request $res){
			$responsedata=$res->all(); 
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false; 
			$allcursors=array(); 
			$valuesnode=array();    


			// dd($responsedata);
			if(!empty($responsedata)){
				if (array_key_exists('hasfornext', $responsedata)) {
					$hasfornextcur=$responsedata['hasfornext'];
					$hasnextprodstat = $responsedata['hasnextprod'];
					if($hasnextprodstat){
						$dataEdit='query	{
						customers(first: 20 after:"'.$hasfornextcur.'")	{
						pageInfo{
						hasNextPage
						hasPreviousPage
						}
						edges{
						cursor
						node{
						id
						tags 
						firstName
						lastName
						email
						phone
						addresses {
						id
						country
						province                  
						provinceCode
						address1
						address2
						company
						city
						zip

						}
						createdAt
						}
						}
						}
						}';

						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					} 
				}



				if (array_key_exists('hasforback', $responsedata)) {
					$hasforbackcur=$responsedata['hasforback'];
					$hasbackprodstat = $responsedata['hasbackprod'];
					if($hasbackprodstat){
						$dataEdit='query	{
							customers(last: 20 before:"'.$hasforbackcur.'")	{
							pageInfo{
							hasNextPage
							hasPreviousPage
							}
							edges{
							cursor
							node{
							id
							tags 
							firstName
							lastName
							email
							phone
							addresses {
							id
							country
							province                  
							provinceCode
							address1
							address2
							company
							city
							zip

							}
							createdAt
							}
							}
							}
						}';
						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					}    

				}  

				if (isset($edges)){  
				try
				{
					$customerdata = array();
					$customerfinaldata = array();
					$customeraddresses = array();
					$customeraddressdata=array();
					foreach ($edges as $key=>$value) {
						$values = $value->node;
						array_push($valuesnode, $value->node);
						$value=(array) $value;
						array_push($allcursors, $value['cursor']);
					}
				}catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }

					return view('invoice_customer')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
				}else{
				return \Redirect::to('/invoicecustomerlist');
				}

			}else{
				return \Redirect::to('/invoicecustomerlist');             
			}

		}
		// for file upload 

		public function rbfileupload(Request $request){
			// echo $request->file('csvfile')->store('public/csvfiles');
			// $request->csvfile
			$file = $request->file('csvfile');
			$filename = $file->getClientOriginalName();
			
		    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
		    $file_ext = substr($filename, strripos($filename, '.')); // get file name
		    // $filesize = $_FILES["csvfile"]["size"];
		    $allowed_file_types = array('.csv');  
		    $customer_id=$request->rb_customer_id;
    
		    // echo $customer_id;
		    if (in_array($file_ext,$allowed_file_types))
		    {   
		        // Rename file
		      $newfilename = $customer_id. $file_ext;
		      // dd($newfilename);

		      $path = $request->file('csvfile')->storeAs('public/csvfiles', $newfilename);
		      // print_r(json_encode($path));
		      // dd();
		     
		    }
		    elseif (empty($file_basename))
		    {   
		        // file selection error
		      echo "Please select a file to upload.";
		    }     
		    else
		    {
		        // file type error
		      echo "Only CSV file typs are allowed for upload"; 
		    }
					

		}

		public function rborderfileupload(Request $request){
			$file = $request->file('orderfile');
			$filename = $file->getClientOriginalName();			
		    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
		    $file_ext = substr($filename, strripos($filename, '.')); 
		    $allowed_file_types = array('.csv');  
		    $customer_id=$request->rb_customer_id;
		    $customer_id = str_replace("gid://shopify/Customer/","",$customer_id);
		    if ($customer_id)
		    {   
		        // Rename file
		      $newfilename = $customer_id."_".time(). $file_ext;
		      // dd($newfilename);
		      // print_r(json_encode($newfilename));
		      // dd();
		      $path = $request->file('orderfile')->storeAs('public/ordernotes', $newfilename);
		      $path = $file->move(public_path('ordernotes'), $newfilename);
		      print_r(json_encode($newfilename));
		      dd();
		     
		    }
		    elseif (empty($file_basename))
		    {   
		        // file selection error
		      echo "Please select a file to upload.";
		    }     
		    else
		    {
		        // file type error
		      echo "File Upload Failed"; 
		    }
					

		}

		public function productmetafield(Request $request){
			$productid  = $request->productid;
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			// echo "heloo";
			// $productid =5584059007135;
			try{
                $productdata = array();
                $productwithmeta = array();
                $shop = ShopifyApp::shop();
                $metadata = $shop->api()->rest('GET', '/admin/products/'.$productid.'/metafields.json');
                $metadata = $metadata->body->metafields;
                $product_metadata = array();                            
                foreach ($metadata as $relatedmetadata) {
                    $metadata_key = $relatedmetadata->namespace;
                    if ($metadata_key == "field") {
                        $metadata_value = $relatedmetadata->value;
                        array_push($product_metadata, $metadata_value);
                    }
                }          
               
				// print_r(json_encode($product_metadata));
				return json_encode($product_metadata);

            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
		}
		
		// get order detail getorder invoicedetail
		public function getorderinvoicedetail(Request $request){
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$orderdata  = $request->all();
			$orderdata = json_decode($orderdata['orderdata']);
			$orderdetails = array();
			foreach ($orderdata as $key => $value) {
				$orderid = $value;						
				try{
	                $orderdata = array();
	                $shop = ShopifyApp::shop();
	                $orderdetail = $shop->api()->rest('GET', '/admin/orders/'.$orderid.'.json');
	                array_push($orderdetails, $orderdetail->body->order);	               

	            }
	            catch (shopify\ApiException $e)
	            {
	                # HTTP status code was >= 400 or response contained the key 'errors'
	                echo $e;
	                print_r($e->getRequest());
	                print_r($e->getResponse());
	            }
	        }
	        print_r(json_encode($orderdetails));
	        dd();
	       return view('invoicecreation')->with(compact('orderdetails'));
		}
		// get order detail getorder invoicedetail
		public function getorderdetail(Request $request){
			$shop_domain = ShopifyApp::shop();			
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$orderdata  = $request->all();
			$orderdata = json_decode($orderdata['orderid']);
			$orderdetails = array();
			// foreach ($orderdata as $key => $value) {
				$orderid = $orderdata;						
				try{
	                $orderdata = array();
	                $shop = ShopifyApp::shop(); 
	                $orderdetail = $shop->api()->rest('GET', '/admin/orders/'.$orderid.'.json');
	          //       print_r(json_encode($orderdetail->body));
	        		// dd(); 
	                // array_push($orderdetails, $orderdetail->body->order);	
	                $orderdetail = $orderdetail->body->order;
	                print_r(json_encode($orderdetail));
	        		dd();              

	            }
	            catch (shopify\ApiException $e)
	            {
	                # HTTP status code was >= 400 or response contained the key 'errors'
	                echo $e;
	                print_r($e->getRequest());
	                print_r($e->getResponse());
	            }
	        // }
	       
	       return view('invoicecreation')->with(compact('orderdetails'));
		}

		public function createorder(Request $request){
			$shop = ShopifyApp::shop();
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$cartdata = json_decode($request->cartdata);	
			$customer_id = $request->customer_id;
			$customer_id = str_replace("gid://shopify/Customer/","",$customer_id);
			$filepath = $request->filepath;
			// print_r(json_encode($customer_id));
		 // 	dd();
			$markaspaid = $request->markaspaid;
			$shipping_data = (array)json_decode($request->shipping_data);
			// print_r(json_encode($cartdata));
			// dd();
			$lineItems = array();
			$total_discount_amt = 0;
			foreach ($cartdata as $cartitem){
				$cartitem = (array)$cartitem;

				$productline['variant_id'] = $cartitem['variantid'];
			    $productline['quantity'] = $cartitem['qty'];

				$discount_amt = (float)$cartitem['individual_discount_amt'];
				$lineproperties = $cartitem['properties'];
				if($discount_amt){
					//$lineproperties[] = array('name'=>"Unit price",'value'=>'$'.$cartitem['price_for_customer']);
					//$lineproperties[] = array('name'=>"Discount per item",'value'=>'$'.$discount_amt);
					$total_discount_amt = $discount_amt*$cartitem['qty'];
				    $productline['applied_discount'] = array(
				    	"title"=> "Custom",
				        "description"=> "Custom discount",
				        "value_type"=> "fixed_amount",
				        "value"=> $discount_amt,
				        "amount"=> $total_discount_amt
				    );
				}
				if(!empty($lineproperties)){
					$productline['properties'] = $lineproperties;
				}		    
			    
			    $lineItems[] = $productline;
			}

			//order attributes
			$additional_info = (array)json_decode($request->additional_info);
			$order_note = $additional_info['order_note'];
			$order_date = $additional_info['date'];
			$po_number = $additional_info['po'];
			$note_attribute = array();
			if($order_date){
				array_push($note_attribute, array('name'=>'Date','value'=>$order_date));
				$draft_order['tags'] = $order_date;
			}
			if($po_number){
				array_push($note_attribute, array('name'=>'Reference','value'=>$po_number));
				// $draft_order['tags'] = ucwords(trim($po_number));
			}

			if ($filepath != "false") {
				$filelink = 'https://rbmembersportal.cartbrain.net/public/ordernotes/'.$filepath.'';
				array_push($note_attribute, array('name'=>'filepath','value'=>$filelink));
			}


			//total order discounts
			$order_discount = (array)json_decode($request->discount_info);
			// print_r(json_encode(count($order_discount)));
			// dd();
			if(count($order_discount)>2){
			$discount_percent = $order_discount['percent'];
			$discount_amt = $order_discount['amt'];
			$default_reason = $discount_percent.'% off';
			$discount_reason = $order_discount['reason']?$discount_percent.'% off('.$order_discount['reason'].')':$default_reason;
			$applied_discount = '';
			if($discount_percent){
				$applied_discount = array(
				  "title"=> $discount_reason,
				  "description"=> $discount_reason,
				  "value"=> $discount_amt,
				  "value_type"=> "fixed_amount",
				  "amount"=> $discount_amt
				);
			}
			if($applied_discount){
				$draft_order["applied_discount"] = $applied_discount;
			}
			
			}
			//order shipping
			$order_shipping = (array)json_decode($request->shipping_info);			
			$shipping_type = $order_shipping['type'];
			$shipping_name = $order_shipping['name'];
			$shipping_amt = $order_shipping['amt'];
			$shipping_line = '';
			if($shipping_type){
				$shipping_line = array(
					  "custom"=> true,
					  "price"=> $shipping_amt,
					  "title"=> $shipping_name
				);
			}
			
			

			if($shipping_line){
				$draft_order["shipping_line"] = $shipping_line;
			}

			$draft_order["line_items"] = $lineItems;
			//$draft_order['total_discounts'] = $total_discount_amt;
			if(!empty($order_note)){
				$draft_order['note'] = $order_note;
			}

			if(count($note_attribute)){
				$draft_order['note_attributes'] = $note_attribute;
			}

			$draft_order['send_receipt'] = true;
			// print_r(count($shipping_data));
			// dd();
			if (count($shipping_data)>0) {
			$first_name = $shipping_data["first_name"];
			$address1 = $shipping_data["address1"];
			$phone = $shipping_data["phone"];
			$city = $shipping_data["city"];
			$zip = $shipping_data["zip_code"];
			$province = $shipping_data["province_code"];	
			$country = $shipping_data["country_name"];
			$last_name = $shipping_data["last_name"];
			$company = $shipping_data["company"];
			$address2 = $shipping_data["address2"];
			$countrycode = $shipping_data["countrycode"];;
			$province = $shipping_data["province"];
			$shippingaddress = array(
		      "first_name" => $first_name,
		      "address1" => $address1,
		      "phone" => $phone,
		      "city" => $city,
		      "zip" =>$zip,
		      "province" => $province,
		      "country" => $country,
		      "last_name" => $last_name,
		      "address2" => $address2,
		      "company" => $company,      
		      "country_code"=> $countrycode,
		      "province_code"=> $province
		    );
		    $billing_address = array(
		      "first_name" => $first_name,
		      "address1" => $address1,
		      "phone" => $phone,
		      "city" => $city,
		      "zip" =>$zip,
		      "province" => $province,
		      "country" => $country,
		      "last_name" => $last_name,
		      "address2" => $address2,
		      "company" => $company,      
		      "country_code"=> $countrycode,
		      "province_code"=> $province
		    );
		    $draft_order['shipping_address'] = array('shipping_address'=>$shippingaddress);
		    $draft_order['billing_address'] = array('billing_address'=>$billing_address);
			// Adding note attributes to order */
		 	// $draft_order['note_attributes'] = $noteAttributes;
		    }
		    if($customer_id){
		    	$draft_order['customer'] = array('id'=>$customer_id);
		    	$draft_order['use_customer_default_address'] = true;
		    }
		    $draftOrderArgs = array('draft_order' => $draft_order );
		    // print_r(json_encode($draftOrderArgs));
		    // dd();
			try
			{
				# Making an API request can throw an exception				
				$draftorder = $shop->api()->rest('POST', '/admin/api/2020-04/draft_orders.json', $draftOrderArgs);
				// print_r(json_encode($draftorder));
		  //   	dd();
				if($draftorder){
					$draftorder_id = $draftorder->body->draft_order->id;
					// print_r(json_encode($draftorder_id));
		 	 //        dd();
		    		if ($markaspaid == 'true') {
		    			$orderquery = 'mutation draftOrderComplete {
									  draftOrderComplete(id:"gid://shopify/DraftOrder/'.$draftorder_id.'", paymentPending:false) {
									    draftOrder {
									      id
									      order{
									      	id
									      	name
									      }
									    }
									    userErrors {
									      field
									      message
									    }
									  }
									}';
		    		}else{
					$orderquery = 'mutation draftOrderComplete {
									  draftOrderComplete(id:"gid://shopify/DraftOrder/'.$draftorder_id.'", paymentPending:true) {
									    draftOrder {
									      id
									      order{
									      	id
									      	name
									      }
									    }
									    userErrors {
									      field
									      message
									    }
									  }
									}';

					}

					$orderdata =$api->graph($orderquery);
					// print_r(json_encode($orderdata));
		 	 //        dd();
					$order_id = $orderdata->body->draftOrderComplete->draftOrder->order->id;
					// mark as paid orderid
					$orderqueryforpaid = 'mutation orderMarkAsPaid {
										  orderMarkAsPaid(input: "'.$order_id.'") {
										    order {
										      id
										    }
										    userErrors {
										      field
										      message
										    }
										  }
										}';
					$orderresponse =$api->graph($orderqueryforpaid);
					
					$order_id = str_replace("gid://shopify/Order/","",$order_id);	
					$saveordertodatabase = $this->saveordertodatabase($order_id);
					$saveinvoicetodatabase = $this->saveinvoicetodatabase($order_id);
					$order_id = $orderdata->body->draftOrderComplete->draftOrder->order->name;
					$customer = array(
				      "id" => $customer_id,
				      "tags" => $order_date
				    );
				    $customerdata = array('customer'=>$customer);
				    $customerdata = $shop->api()->rest('PUT', '/admin/api/2020-04/customers/'.$customer_id.'.json', $customerdata);
					// $saveordertodatabase = $this->saveordertodatabase($order_id);
					print_r(json_encode($order_id));
		 	        dd();
				}
			}
			catch (shopify\ApiException $e)
			{
				# HTTP status code was >= 400 or response contained the key 'errors'
				echo $e;
				print_R($e->getRequest());
				print_R($e->getResponse());
			}
			catch (shopify\CurlException $e)
			{
				# cURL error
				echo $e;
				print_R($e->getRequest());
				print_R($e->getResponse());
			}

		}

		// save order to database
		public function saveordertodatabase($order_id){
			// $orderid  = $request->orderid;
			$orderid  = $order_id;
			// print_r(json_encode($orderid));
			// dd();
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			try{
                $productdata = array();
                $productwithmeta = array();
                $shop = ShopifyApp::shop();
                $orderdata = $shop->api()->rest('GET', '/admin/orders/'.$orderid.'.json'); 
                $orderdetail = $orderdata->body->order; 
                $orderid = $orderdetail->id;   
                $email = $orderdetail->email;  
                $ordername = $orderdetail->name;  
                $fulfillment_status = $orderdetail->fulfillment_status; 
                if($fulfillment_status === NULL) {
                	$fulfillment_status = "Unfulfilled";
                } 
                $tags = $orderdetail->tags;  
                $first_name = $orderdetail->customer->first_name;  
                $last_name = $orderdetail->customer->last_name;                  
                $created_at = $orderdetail->created_at; 
                $note_attributes = $orderdetail->note_attributes; 
                $orderfor = "Null";
                $Reference =   "Null";
                $filepath =  "Null";
                if(count($note_attributes) >0){
                	foreach ($note_attributes as $key => $valuedata) {
                		$val_name = $valuedata->name;
                		if ($val_name == "Date") {
                			$orderfor = $valuedata->value;
                		}
                		if ($val_name == "Reference") {
                			$Reference =  $valuedata->value;
                		}
                		if ($val_name == "filepath") {
                			$filepath = $valuedata->value;
                		}
                	}
	                // $orderfor = $note_attributes[0]->value;
	                // $Reference =  $note_attributes[1]->value;
	                // $filepath = $note_attributes[2]->value;
	            }
                $note = $orderdetail->note; 
                $total_amt = $orderdetail->total_price; 
				$customer_id = $orderdetail->customer->id;
				$status = "Unsettled";
				$sql = DB::insert(DB::raw("INSERT INTO `order_table` (order_id, order_name, customer_first_name, customer_last_name, customer_email, order_created_at, order_delivery_date, reference, fulfillment_status, order_tags, notes, total_amount,customer_id,status) values ('$orderid','$ordername','$first_name','$last_name','$email','$created_at','$orderfor','$Reference','$fulfillment_status','$tags','$note','$total_amt','$customer_id','$status')"));


				return json_encode($sql);
   

            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
        }

        // save order to database
		public function saveinvoicetodatabase($order_id){
			// $orderid  = $request->orderid;
			$orderid  = $order_id;
			// print_r(json_encode($orderid));
			// dd();
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			try{
                $productdata = array();
                $productwithmeta = array();
                $shop = ShopifyApp::shop();
                $orderdata = $shop->api()->rest('GET', '/admin/orders/'.$orderid.'.json'); 
                $orderdetail = $orderdata->body->order; 
                $orderid = $orderdetail->id;   
                $email = $orderdetail->email;  
                $ordername = $orderdetail->order_number;  
                $fulfillment_status = $orderdetail->fulfillment_status; 
                if($fulfillment_status === NULL) {
                	$fulfillment_status = "Unfulfilled";
                } 
                $tags = $orderdetail->tags;  
                $first_name = $orderdetail->customer->first_name;  
                $last_name = $orderdetail->customer->last_name;                  
                $created_at = $orderdetail->created_at; 
                $sectime = strtotime($created_at);
				$created_at = date("m/d/Y",$sectime);
                $note_attributes = $orderdetail->note_attributes; 
                $orderfor = "Null";
                $Reference =   "Null";
                $filepath =  "Null";
                if(count($note_attributes) >0){
                	foreach ($note_attributes as $key => $valuedata) {
                		$val_name = $valuedata->name;
                		if ($val_name == "Date") {
                			$orderfor = $valuedata->value;
                		}
                		if ($val_name == "Reference") {
                			$Reference =  $valuedata->value;
                		}
                		if ($val_name == "filepath") {
                			$filepath = $valuedata->value;
                		}
                	}
	                // $orderfor = $note_attributes[0]->value;
	                // $Reference =  $note_attributes[1]->value;
	                // $filepath = $note_attributes[2]->value;
	            }
                $note = $orderdetail->note; 
                $total_amt = $orderdetail->total_price; 
				$customer_id = $orderdetail->customer->id;
				$status = "Unsettled";
			
				$sql = DB::insert(DB::raw("INSERT INTO `invoice_table` (orderid,invoice_number,invoice_date,order_date,customer_first_name,customer_last_name,amount,school,status,amttobepaid,customerid,Deposit) values ('$orderid','$ordername','$created_at','$created_at','$first_name','$last_name','$total_amt','$Reference','$status','No Deposite','$customer_id','Not Paid')"));

				return json_encode($sql);
   

            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
        }

		public function orderlisting(Request $res){
			$responsedata = $res->all();
			$customer_id =$responsedata['customerid'];
			$shop = ShopifyApp::shop();
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false; 
			$allcursors=array(); 
			$valuesnode=array(); 
			$dataEdit='query	{
		      orders(first: 20 query:"'.$customer_id.' unfulfilled")	{
		      pageInfo{
		      hasNextPage
		      hasPreviousPage
		      }
		      edges{
		      cursor
		      node{
			      id
			      name     
				  createdAt
				  updatedAt
						customer{
			        id
							displayName			
			      }
			      totalPriceSet{
			        presentmentMoney{
			          amount
			          currencyCode
			        }
			      }
			      note
			      customAttributes {
			        key
			        value
			      }
			      shippingLine{
			        title
			      }
			        fulfillments{
			          status
			          deliveredAt
			        }
		      }
		      }
		      }
			}';
			$request_edit=$api->graph($dataEdit);
			// dd($request_edit);
			$hasNextPageCust=$request_edit->body->orders->pageInfo->hasNextPage;
			$hasPreviousPageCust=$request_edit->body->orders->pageInfo->hasPreviousPage;

			$edges = $request_edit->body->orders->edges;

			try{				
				foreach ($edges as $key=>$value) {
					$values = $value->node;
					array_push($valuesnode, $value->node);
					$value=(array) $value;
					array_push($allcursors, $value['cursor']);
				}
				return view('invoice_order')->with(compact('valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
			}
			catch (shopify\ApiException $e) {
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
	        }
	        catch (shopify\CurlException $e) {
	            error_log(json_encode($e->getRequest()));
	            error_log(json_encode($e->getResponse()));
	        }

		}
		public function paginateordertabnext(Request $res){
			$responsedata=$res->all(); 
			$customer_id =$responsedata['customerid'];
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false; 
			$allcursors=array(); 
			$valuesnode=array();    


			// dd($responsedata);
			if(!empty($responsedata)){
				if (array_key_exists('hasfornext', $responsedata)) {
					$hasfornextcur=$responsedata['hasfornext'];
					$hasnextprodstat = $responsedata['hasnextprod'];
					if($hasnextprodstat){
						$dataEdit='query	{
					      orders(first: 20 after:"'.$hasfornextcur.'" query:"'.$customer_id.' unfulfilled")	{
					      pageInfo{
					      hasNextPage
					      hasPreviousPage
					      }
					      edges{
					      cursor
					      node{
						      id
						      name     
							  createdAt
							  updatedAt
									customer{
						        id
										displayName			
						      }
						      totalPriceSet{
						        presentmentMoney{
						          amount
						          currencyCode
						        }
						      }
						      note
						      customAttributes {
						        key
						        value
						      }
						      shippingLine{
						        title
						      }
						        fulfillments{
						          status
						          deliveredAt
						        }
					      }
					      }
					      }
						}';
						

					$request_edit=$api->graph($dataEdit);
					// dd($request_edit);
					$hasNextPageCust=$request_edit->body->orders->pageInfo->hasNextPage;
					$hasPreviousPageCust=$request_edit->body->orders->pageInfo->hasPreviousPage;

					$edges = $request_edit->body->orders->edges;
					} 
				}



				if (array_key_exists('hasforback', $responsedata)) {
					$hasforbackcur=$responsedata['hasforback'];
					$hasbackprodstat = $responsedata['hasbackprod'];
					if($hasbackprodstat){
						$dataEdit='query	{
					      orders(last: 20 before:"'.$hasforbackcur.'" query:"'.$customer_id.' unfulfilled")	{
					      pageInfo{
					      hasNextPage
					      hasPreviousPage
					      }
					      edges{
					      cursor
					      node{
						      id
						      name     
							  createdAt
							  updatedAt
									customer{
						        id
										displayName			
						      }
						      totalPriceSet{
						        presentmentMoney{
						          amount
						          currencyCode
						        }
						      }
						      note
						      customAttributes {
						        key
						        value
						      }
						      shippingLine{
						        title
						      }
						        fulfillments{
						          status
						          deliveredAt
						        }
					      }
					      }
					      }
						}';
						
						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->orders->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->orders->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->orders->edges;
					}    

				}  

				if (isset($edges)){  
				try
				{
					foreach ($edges as $key=>$value) {
						$values = $value->node;
						array_push($valuesnode, $value->node);
						$value=(array) $value;
						array_push($allcursors, $value['cursor']);
					}
				}catch (shopify\ApiException $e) {
		              # HTTP status code was >= 400 or response contained the key 'errors'
		                //echo $e;
		            error_log(json_encode($e->getRequest()));
		            error_log(json_encode($e->getResponse()));
		        } catch (shopify\CurlException $e) {
		              # cURL error
		                //echo $e;
		            error_log(json_encode($e->getRequest()));
		            error_log(json_encode($e->getResponse()));
		        }

					return view('invoice_order')->with(compact('valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
				}else{
				return \Redirect::to('/invoiceorder');
				}

			}else{
				return \Redirect::to('/invoiceorder');             
			}

		}

		

        // save order to database
		public function getorderfromdatabase(){
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			try{
                // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` "))->simplePaginate(1);
                $valuesnode = DB::table('order_table')->orderBy('id', 'desc')->paginate(20);
                 // $valuesnode = DB::select(DB::raw("SELECT * from `order_table` ORDER BY id DESC LIMIT 10")->simplePaginate(1));
				// print_r(json_encode($valuesnode));
				// dd();
				return view('orderlisting')->with(compact('shopdomain','valuesnode'));
            }
            catch (shopify\ApiException $e)
            {
                # HTTP status code was >= 400 or response contained the key 'errors'
                echo $e;
                print_r($e->getRequest());
                print_r($e->getResponse());
            }
        }

        public function invoicecustomersearch(Request $res){
        	// print_r(json_encode($query));
        	$responsedata =$res->all(); 
        	$firstname="";
			$lastname="";
			$no_of_order_list="20";
        	if (array_key_exists('no_of_order_list', $responsedata)) {
		    $no_of_order_list = $responsedata['no_of_order_list'];
			}
        	if (array_key_exists('firstname', $responsedata)) {
			    $firstname = $responsedata['firstname'];
			}
			else{
				$firstname = '';
			}
			if (array_key_exists('lastname', $responsedata)) {
			    $lastname = $responsedata['lastname'];
			}
			else{
				$lastname = '';
			}        	        	
        	
        	// $query = "first_name:".$firstname." last_name:".$lastname;
        	$query = $firstname." ".$lastname;
			// dd($query);
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false;
			$allcursors=array(); 
			$valuesnode=array();
			// $query = 'Nic ';
			// print_r(json_encode($query));
			// dd();
			$dataEdit='query	{
				customers(first:'.$no_of_order_list.'  query:"'.$query.'")	{
				pageInfo{
				hasNextPage
				hasPreviousPage
				}
				edges{
				cursor
				node{
				id
				tags 
				firstName
				lastName
				email
				phone
				addresses {
				id
				country
				province                  
				provinceCode
				address1
				address2
				company
				city
				zip

				}
				createdAt
				}
				}
				}
			}';

			$request_edit=$api->graph($dataEdit);
			// dd($request_edit);
			$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
			$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

			$edges = $request_edit->body->customers->edges;

			try{
				$customerdata = array();
				$customerfinaldata = array();
				$customeraddresses = array();
				$customeraddressdata=array();
				foreach ($edges as $key=>$value) {
					$values = $value->node;
					array_push($valuesnode, $value->node);

					// $values = (array) $values;
					$value=(array) $value;

					array_push($allcursors, $value['cursor']);
				}
			} catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }
			

			return view('invoice_customer')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
		}
		public function paginateinvoicecustomersearchtabnext(Request $res){
			$responsedata=$res->all(); 
			$shop_domain = ShopifyApp::shop();
			$shopdomain = $shop_domain->shopify_domain;
			$shopify_token = $shop_domain->shopify_token;
			$api = new BasicShopifyAPI();
			$api->setVersion('2020-04'); 
			$api->setShop($shopdomain);
			$api->setAccessToken($shopify_token);
			$hasNextPageProd=false;
			$hasPreviousPageProd=false; 
			$allcursors=array(); 
			$valuesnode=array();    


			// dd($responsedata);
			if(!empty($responsedata)){
				if (array_key_exists('hasfornext', $responsedata)) {
					$hasfornextcur=$responsedata['hasfornext'];
					$hasnextprodstat = $responsedata['hasnextprod'];
					if($hasnextprodstat){
						$dataEdit='query	{
						customers(first: 20 after:"'.$hasfornextcur.'")	{
						pageInfo{
						hasNextPage
						hasPreviousPage
						}
						edges{
						cursor
						node{
						id
						tags 
						firstName
						lastName
						email
						phone
						addresses {
						id
						country
						province                  
						provinceCode
						address1
						address2
						company
						city
						zip

						}
						createdAt
						}
						}
						}
						}';

						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					} 
				}



				if (array_key_exists('hasforback', $responsedata)) {
					$hasforbackcur=$responsedata['hasforback'];
					$hasbackprodstat = $responsedata['hasbackprod'];
					if($hasbackprodstat){
						$dataEdit='query	{
							customers(last: 20 before:"'.$hasforbackcur.'")	{
							pageInfo{
							hasNextPage
							hasPreviousPage
							}
							edges{
							cursor
							node{
							id
							tags 
							firstName
							lastName
							email
							phone
							addresses {
							id
							country
							province                  
							provinceCode
							address1
							address2
							company
							city
							zip

							}
							createdAt
							}
							}
							}
						}';
						$request_edit=$api->graph($dataEdit);
						// dd($request_edit);
						$hasNextPageCust=$request_edit->body->customers->pageInfo->hasNextPage;
						$hasPreviousPageCust=$request_edit->body->customers->pageInfo->hasPreviousPage;

						$edges = $request_edit->body->customers->edges;
					}    

				}  

				if (isset($edges)){  
				try
				{
					$customerdata = array();
					$customerfinaldata = array();
					$customeraddresses = array();
					$customeraddressdata=array();
					foreach ($edges as $key=>$value) {
						$values = $value->node;
						array_push($valuesnode, $value->node);
						$value=(array) $value;
						array_push($allcursors, $value['cursor']);
					}
				}catch (shopify\ApiException $e) {
              # HTTP status code was >= 400 or response contained the key 'errors'
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        } catch (shopify\CurlException $e) {
              # cURL error
                //echo $e;
            error_log(json_encode($e->getRequest()));
            error_log(json_encode($e->getResponse()));
        }

					return view('invoice_customer')->with(compact('shopdomain','valuesnode','allcursors','hasPreviousPageCust','hasNextPageCust'));
				}else{
				return \Redirect::to('/invoicecustomerlist');
				}

			}else{
				return \Redirect::to('/invoicecustomerlist');             
			}

		}


		

	}



	

	

	



