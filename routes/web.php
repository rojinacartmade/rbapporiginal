<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', function () {
//     return view('welcome');
// })->middleware(['auth.shop'])->name('home');

// if (env('APP_ENV') === 'local') {
//     URL::forceSchema('https');
// }
\URL::forceScheme('https'); 
// Route::any('/','ShopController@indexredirect')->middleware(['auth.shop'])->name('home');
Route::any('/','ShopController@getorderfromdatabase')->middleware(['auth.shop'])->name('home');
// Route::any('/','ShopController@getorderfromdatabase');
Route::any('/ordercreation','ShopController@indexredirect');
Route::any('/paginatecustomer','ShopController@paginatecustomernext');
Route::any('/rbcustomers','ShopController@rbcustomers');
Route::any('/rbfileupload','ShopController@rbfileupload');
Route::any('/customertab','ShopController@paginatecustomertabnext');
Route::any('/getmetafield','ShopController@productmetafield');
Route::any('/createorder','ShopController@createorder');

Route::any('/editorder','ShopController@editorder');
Route::any('/rborderfileupload','ShopController@rborderfileupload');
Route::any('/orderwebhook','ShopController@saveordertodatabase');
Route::any('/getorder','ShopController@getorderfromdatabase');
Route::any('/getorderdetail','ShopController@getorderdetail');

// for invoice customer listing
Route::any('/invoicecustomerlist','ShopController@invoicecustomer');
Route::any('/invoicecustomertab','ShopController@paginateinvoicecustomertabnext');

// for invoice selected customer orders
// Route::any('/invoiceorders','ShopController@orderlisting');
Route::any('/invoiceorders','invoiceController@orderlistingforinvoice');
Route::any('/invoiceordersnext','ShopController@paginateordertabnext');

// for invoice order detail
Route::any('/getorderinvoicedetail','ShopController@getorderinvoicedetail');

// filter customer on invoice

Route::any('/customersearch','ShopController@invoicecustomersearch');
Route::any('/invoicecustomertab','ShopController@paginateinvoicecustomersearchtabnext');

// filter in order listing
Route::any('/ordersearch','orderController@orderfilter');

// invoice list
Route::any('/invoices','orderController@getinvoicefromdatabase');
Route::any('/getorderinvoicestatusdetail','invoiceController@getorderinvoicestatusdetail');

// for invoice payment customer listing
Route::any('/invoicepaymentcustomerlist','invoiceController@invoicepaymentcustomer');
Route::any('/invoicepaymentcustomertab','invoiceController@paginateinvoicepaymentcustomertabnext');
Route::any('/paymentcustomersearch','invoiceController@invoicecustomersearch');
Route::any('/invoiceemails','invoiceController@invoicecustomeremails');
Route::any('/updatepayment','invoiceController@updateinvoicepaymentstatus');

// for listing of email sends
Route::any('/invoiceemailslist','invoiceController@invoicecustomeremailslist');
Route::any('/getinvoicefromgroup','invoiceController@getinvoicefromgroup');

// update invoice status
Route::any('/updatepaymentstatus','invoiceController@updateorderinvoicepaymentstatus');
Route::any('/deletegroupinvoice','invoiceController@deletegroupinvoice');
Route::any('/invoicesearch','invoiceController@invoicesearch');

Route::any('/getproductimage','invoiceController@getproductimage');



